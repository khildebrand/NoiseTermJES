bool debug=false;
bool savePNG=true;
bool savePDF=false;
void ATLASLabel3(Double_t x,Double_t y,const char* text,double labelOffset=1.,double tsize=.08,Color_t color = 1);
void ATLASLabel3(Double_t x,Double_t y,const char* text,double labelOffset,double tsize,Color_t color)
{
  TLatex l; //l.SetTextAlign(12); l.SetTextSize(tsize);
  l.SetTextSize(tsize);
  l.SetNDC();
  l.SetTextFont(72);
  l.SetTextColor(color);

  double delx = labelOffset*0.115*696*gPad->GetWh()/(472*gPad->GetWw());

  l.DrawLatex(x,y,"ATLAS");
  if (text) {
    TLatex p;
    p.SetTextSize(tsize);
    p.SetNDC();
    p.SetTextFont(42);
    p.SetTextColor(color);
    p.DrawLatex(x+delx,y,text);
  }
}
void saveSingleHisto(TH1* histoOrig,std::string xaxisTitle,std::string yaxisTitle,vector<std::string> stuffToWrite,std::string outPutDir,bool normalizedArea,Double_t xStart,Double_t yStart,Int_t yAxisStart,Int_t yAxisEnd,bool logScale=false,bool rebin=false);
void saveSingleHisto(TH1* histoOrig,std::string xaxisTitle,std::string yaxisTitle,vector<std::string> stuffToWrite,std::string outPutDir,bool normalizedArea,Double_t xStart,Double_t yStart,Int_t yAxisStart,Int_t yAxisEnd,bool logScale,bool rebin) {
	TH1D *histo = (TH1D*)histoOrig->Clone();
	TCanvas *c1 = new TCanvas("c1","c1",0,0,600,600);

	if (logScale) {
		c1->SetLogy();
	}
	TLatex latex;
	latex.SetNDC();
	latex.SetTextColor(1);
	latex.SetTextFont(42);
	latex.SetTextSize(.04);

	if (normalizedArea) {
		/*cout<<stack->GetNbinsX()<<" " <<dataHisto->GetNbinsX()<<endl;
		cout<<stack->Integral()<< " " << dataHisto->Integral()<<endl;
		stack->Scale(dataHisto->Integral()/(stack->Integral()));
		cout<<stack->Integral()<<endl;
		//dataHisto->Scale(1/(dataHisto->Integral()));*/

		cout<<histo->GetNbinsX()<<endl;
		cout<<histo->Integral(1,histo->GetNbinsX())<<endl;
		histo->Scale(1/(histo->Integral(1,histo->GetNbinsX())));
		cout<<histo->Integral(1,histo->GetNbinsX())<<endl;
		//dataHisto->Scale(1/(dataHisto->Integral()));
		//stuffToWrite.push_back("Period A4");
	}
	if (rebin) {
		histo->Rebin();
	}

	histo->Draw("HIST");
	if (logScale) {
		histo->SetMinimum(.2);
		histo->SetMaximum(histo->GetMaximum()*1000);
	}
	histo->GetXaxis()->SetTitle(xaxisTitle.c_str());
	histo->GetXaxis()->SetLabelSize(0.04);
	histo->GetXaxis()->SetNdivisions(505);
	histo->GetXaxis()->SetTitleSize(.04);
	histo->GetYaxis()->SetTitle(yaxisTitle.c_str());
	histo->GetYaxis()->SetTitleSize(.04);
	histo->GetYaxis()->SetTitleOffset(1.75);
	//dataHisto->GetYaxis()->SetLabelSize(.04);
	histo->GetYaxis()->SetLabelSize(.04);


	ATLASLabel3(xStart,yStart,"Internal",.85,.04);
	Double_t x=xStart;Double_t y=yStart-.045;
	for (int j=0;j<stuffToWrite.size();j++) {
		latex.DrawLatex(x,  y, stuffToWrite[j].c_str());
		y-=.045;
	}

	std::string temp(histo->GetName());
	if (rebin) {
		temp=temp+"rebin";
	}
	if (savePNG) {
		c1->Print((outPutDir+temp+".png").c_str(),"png");
	}
	if (savePDF) {
		c1->Print((outPutDir+temp+".pdf").c_str(),"pdf");
	}

}
void saveTwoHisto(TH1* histoOrig,TH1* histoOrig2,TLegend* legend,std::string xaxisTitle,std::string yaxisTitle,vector<std::string> stuffToWrite,std::string outPutDir,bool normalizedArea,Double_t xStart,Double_t yStart,Int_t yAxisStart,Int_t yAxisEnd,bool logScale=false,bool rebin=false);
void saveTwoHisto(TH1* histoOrig,TH1* histoOrig2,TLegend* legend,std::string xaxisTitle,std::string yaxisTitle,vector<std::string> stuffToWrite,std::string outPutDir,bool normalizedArea,Double_t xStart,Double_t yStart,Int_t yAxisStart,Int_t yAxisEnd,bool logScale,bool rebin) {
	cout<<"heyo1"<<endl;
	TH1D *histo = (TH1D*)histoOrig->Clone();
	cout<<"heyo2"<<endl;
	TH1D *histo2 = (TH1D*)histoOrig2->Clone();
	cout<<"heyo3"<<endl;
	TCanvas *c1 = new TCanvas("c1","c1",0,0,600,600);
	cout<<"heyo4"<<endl;
	if (logScale) {
		c1->SetLogy();
	}
	TLatex latex;
	latex.SetNDC();
	latex.SetTextColor(1);
	latex.SetTextFont(42);
	latex.SetTextSize(.04);

	if (normalizedArea) {
		histo->Scale(1/(histo->Integral(1,histo->GetNbinsX())));
		histo2->Scale(1/(histo2->Integral(1,histo2->GetNbinsX())));

	}
	if (rebin) {
		histo->Rebin();
		histo2->Rebin();
	}

	histo->Draw("HIST");
	//histo2->SetLineColor(2);
	histo2->Draw("HIST same");
	if (logScale) {
		histo->SetMinimum(.2);
		histo->SetMaximum(histo->GetMaximum()*1000);
	}
	histo->GetXaxis()->SetTitle(xaxisTitle.c_str());
	histo->GetXaxis()->SetLabelSize(0.04);
	histo->GetXaxis()->SetNdivisions(505);
	histo->GetXaxis()->SetTitleSize(.04);
	histo->GetYaxis()->SetTitle(yaxisTitle.c_str());
	histo->GetYaxis()->SetTitleSize(.04);
	histo->GetYaxis()->SetTitleOffset(1.75);
	//dataHisto->GetYaxis()->SetLabelSize(.04);
	histo->GetYaxis()->SetLabelSize(.04);

	legend->Draw();
	ATLASLabel3(xStart,yStart,"Internal",.85,.04);
	Double_t x=xStart;Double_t y=yStart-.045;
	for (int j=0;j<stuffToWrite.size();j++) {
		latex.DrawLatex(x,  y, stuffToWrite[j].c_str());
		y-=.045;
	}

	std::string temp(histo->GetName());
	if (rebin) {
		temp=temp+"rebin";
	}
	if (!normalizedArea) {
		temp=temp+"NotNormalized";
	}
	if (savePNG) {
		c1->Print((outPutDir+temp+".png").c_str(),"png");
	}
	if (savePDF) {
		c1->Print((outPutDir+temp+".pdf").c_str(),"pdf");
	}

}
void saveOneHistoMarker(std::string HistrogramName,TH1* histoOrig,std::string xaxisTitle,std::string yaxisTitle,vector<std::string> stuffToWrite,std::string outPutDir,bool normalizedArea,Double_t xStart,Double_t yStart,Int_t yAxisStart,Int_t yAxisEnd,bool logScaleX=false,bool logScale=false,bool rebin=false);
void saveOneHistoMarker(std::string HistrogramName,TH1* histoOrig,std::string xaxisTitle,std::string yaxisTitle,vector<std::string> stuffToWrite,std::string outPutDir,bool normalizedArea,Double_t xStart,Double_t yStart,Int_t yAxisStart,Int_t yAxisEnd,bool logScaleX,bool logScale,bool rebin) {
	TH1D *histo = (TH1D*)histoOrig->Clone();
	if (histo->GetEntries()==0) {
		cout<<"histogram was empty"<<endl;
	} else {
		TCanvas *c1 = new TCanvas("c1","c1",0,0,600,600);

		if (logScale) {
			c1->SetLogy();
		}
		if (logScaleX) {
			c1->SetLogx();
		}
		TLatex latex;
		latex.SetNDC();
		latex.SetTextColor(1);
		latex.SetTextFont(42);
		latex.SetTextSize(.04);

		if (normalizedArea) {
			histo->Scale(1/(histo->Integral(1,histo->GetNbinsX())));

		}
		if (rebin) {
			histo->Rebin();
		}

		histo->Draw();
		//histo->GetXaxis()->SetNdivisions(30);
		//histo2->SetLineColor(4);
		/*
		Double_t yMax=0;
		Double_t yMin=0;
		yMax=histo->GetMaximum()*1.33;
		yMin=histo->GetMinimum()*.95;

		histo->SetMinimum(yMin);
		histo->SetMaximum(yMax);
		if (logScale) {
			histo->SetMinimum(.2);
			histo->SetMaximum(histo->GetMaximum()*1000);
		}*/
		histo->GetXaxis()->SetTitle(xaxisTitle.c_str());
		histo->GetXaxis()->SetLabelSize(0.04);
		//histo->GetXaxis()->SetNdivisions(505);
		//histo->GetXaxis()->SetLimits(-4.5,4.5);

		histo->GetXaxis()->SetTitleSize(.04);
		histo->GetYaxis()->SetTitle(yaxisTitle.c_str());
		histo->GetYaxis()->SetTitleSize(.04);
		histo->GetYaxis()->SetTitleOffset(1.75);
		//dataHisto->GetYaxis()->SetLabelSize(.04);
		histo->GetYaxis()->SetLabelSize(.04);

		ATLASLabel3(xStart,yStart,"Internal",.85,.04);
		Double_t x=xStart;Double_t y=yStart-.05;
		for (int j=0;j<stuffToWrite.size();j++) {
			latex.DrawLatex(x,  y, stuffToWrite[j].c_str());
			y-=.05;
		}

		std::string temp(histo->GetName());
		if (rebin) {
			temp=temp+"rebin";
		}
		temp=temp+"example";
		if (savePNG) {
			c1->Print((outPutDir+HistrogramName+".png").c_str(),"png");
		}
		if (savePDF) {
			c1->Print((outPutDir+temp+".pdf").c_str(),"pdf");
		}
	}

}
void saveTwoHistoMarker(TH1* histoOrig,TH1* histoOrig2,TLegend* legend,std::string xaxisTitle,std::string yaxisTitle,vector<std::string> stuffToWrite,std::string outPutDir,bool normalizedArea,Double_t xStart,Double_t yStart,Int_t yAxisStart,Int_t yAxisEnd,bool logScaleX=false,bool logScale=false,bool rebin=false);
void saveTwoHistoMarker(TH1* histoOrig,TH1* histoOrig2,TLegend* legend,std::string xaxisTitle,std::string yaxisTitle,vector<std::string> stuffToWrite,std::string outPutDir,bool normalizedArea,Double_t xStart,Double_t yStart,Int_t yAxisStart,Int_t yAxisEnd,bool logScaleX,bool logScale,bool rebin) {
	TH1D *histo = (TH1D*)histoOrig->Clone();
	TH1D *histo2 = (TH1D*)histoOrig2->Clone();
	if (histo->GetEntries()+histo2->GetEntries()==0) {
		cout<<"histogram was empty"<<endl;
	} else {
		TCanvas *c1 = new TCanvas("c1","c1",0,0,600,600);

		if (logScale) {
			c1->SetLogy();
		}
		if (logScaleX) {
			c1->SetLogx();
		}
		TLatex latex;
		latex.SetNDC();
		latex.SetTextColor(1);
		latex.SetTextFont(42);
		latex.SetTextSize(.04);

		if (normalizedArea) {
			histo->Scale(1/(histo->Integral(1,histo->GetNbinsX())));
			histo2->Scale(1/(histo2->Integral(1,histo2->GetNbinsX())));

		}
		if (rebin) {
			histo->Rebin();
			histo2->Rebin();
		}

		histo->Draw("HIST PE");
		//histo->GetXaxis()->SetNdivisions(30);
		//histo2->SetLineColor(4);
		histo2->Draw("PE same HIST");
		Double_t yMax=0;
		Double_t yMin=0;
		if (histo->GetMaximum()>histo2->GetMaximum()) {
			yMax=histo->GetMaximum()*1.33;
		} else {
			yMax=histo2->GetMaximum()*1.33;
		}
		if (histo->GetMinimum()>histo2->GetMinimum()) {
			yMin=histo2->GetMinimum()*.95;
		} else {
			yMin=histo->GetMinimum()*.95;
		}
		histo->SetMinimum(yMin);
		histo->SetMaximum(yMax);
		if (logScale) {
			histo->SetMinimum(.2);
			histo->SetMaximum(histo->GetMaximum()*1000);
		}
		histo->GetXaxis()->SetTitle(xaxisTitle.c_str());
		histo->GetXaxis()->SetLabelSize(0.04);
		//histo->GetXaxis()->SetNdivisions(505);
		//histo->GetXaxis()->SetLimits(-4.5,4.5);

		histo->GetXaxis()->SetTitleSize(.04);
		histo->GetYaxis()->SetTitle(yaxisTitle.c_str());
		histo->GetYaxis()->SetTitleSize(.04);
		histo->GetYaxis()->SetTitleOffset(1.75);
		//dataHisto->GetYaxis()->SetLabelSize(.04);
		histo->GetYaxis()->SetLabelSize(.04);

		legend->Draw();
		ATLASLabel3(xStart,yStart,"Internal",.85,.04);
		Double_t x=xStart;Double_t y=yStart-.045;
		for (int j=0;j<stuffToWrite.size();j++) {
			latex.DrawLatex(x,  y, stuffToWrite[j].c_str());
			y-=.045;
		}

		std::string temp(histo->GetName());
		if (rebin) {
			temp=temp+"rebin";
		}
		if (savePNG) {
			c1->Print((outPutDir+temp+".png").c_str(),"png");
		}
		if (savePDF) {
			c1->Print((outPutDir+temp+".pdf").c_str(),"pdf");
		}
	}

}
void saveThreeHistoMarker(TH1* histoOrig,TH1* histoOrig2,TH1* histoOrig3,TLegend* legend,std::string xaxisTitle,std::string yaxisTitle,vector<std::string> stuffToWrite,std::string outPutDir,bool normalizedArea,Double_t xStart,Double_t yStart,Int_t yAxisStart,Int_t yAxisEnd,bool logScaleX=false,bool logScale=false,bool rebin=false);
void saveThreeHistoMarker(TH1* histoOrig,TH1* histoOrig2,TH1* histoOrig3,TLegend* legend,std::string xaxisTitle,std::string yaxisTitle,vector<std::string> stuffToWrite,std::string outPutDir,bool normalizedArea,Double_t xStart,Double_t yStart,Int_t yAxisStart,Int_t yAxisEnd,bool logScaleX,bool logScale,bool rebin) {
	TH1D *histo = (TH1D*)histoOrig->Clone();
	TH1D *histo2 = (TH1D*)histoOrig2->Clone();
	TH1D *histo3 = (TH1D*)histoOrig3->Clone();
	if (histo->GetEntries()+histo2->GetEntries()==0) {
		cout<<"histogram was empty"<<endl;
	} else {
		TCanvas *c1 = new TCanvas("c1","c1",0,0,600,600);

		if (logScale) {
			c1->SetLogy();
		}
		if (logScaleX) {
			c1->SetLogx();
		}
		TLatex latex;
		latex.SetNDC();
		latex.SetTextColor(1);
		latex.SetTextFont(42);
		latex.SetTextSize(.04);

		if (normalizedArea) {
			histo->Scale(1/(histo->Integral(1,histo->GetNbinsX())));
			histo2->Scale(1/(histo2->Integral(1,histo2->GetNbinsX())));

		}
		if (rebin) {
			histo->Rebin();
			histo2->Rebin();
		}

		histo->Draw("HIST PE");
		//histo->GetXaxis()->SetNdivisions(30);
		//histo2->SetLineColor(4);
		histo2->Draw("PE same HIST");
		histo3->Draw("PE same HIST");
		Double_t yMax=0;
		Double_t yMin=0;
		if (histo->GetMaximum()>histo2->GetMaximum()) {
			yMax=histo->GetMaximum()*1.33;
		} else {
			yMax=histo2->GetMaximum()*1.33;
		}
		if (histo->GetMinimum()>histo2->GetMinimum()) {
			yMin=histo2->GetMinimum()*.95;
		} else {
			yMin=histo->GetMinimum()*.95;
		}
		//histo->SetMinimum(yMin);
		//histo->SetMaximum(yMax);
		if (logScale) {
			histo->SetMinimum(.2);
			histo->SetMaximum(histo->GetMaximum()*1000);
		}
		histo->GetXaxis()->SetTitle(xaxisTitle.c_str());
		histo->GetXaxis()->SetLabelSize(0.04);
		//histo->GetXaxis()->SetNdivisions(505);
		//histo->GetXaxis()->SetLimits(-4.5,4.5);

		histo->GetXaxis()->SetTitleSize(.04);
		histo->GetYaxis()->SetTitle(yaxisTitle.c_str());
		histo->GetYaxis()->SetTitleSize(.04);
		histo->GetYaxis()->SetTitleOffset(1.75);
		//dataHisto->GetYaxis()->SetLabelSize(.04);
		histo->GetYaxis()->SetLabelSize(.04);

		legend->Draw();
		ATLASLabel3(xStart,yStart,"Internal",.85,.04);
		Double_t x=xStart;Double_t y=yStart-.045;
		for (int j=0;j<stuffToWrite.size();j++) {
			latex.DrawLatex(x,  y, stuffToWrite[j].c_str());
			y-=.045;
		}

		std::string temp(histo->GetName());
		if (rebin) {
			temp=temp+"rebin";
		}
		if (savePNG) {
			c1->Print((outPutDir+temp+".png").c_str(),"png");
		}
		if (savePDF) {
			c1->Print((outPutDir+temp+".pdf").c_str(),"pdf");
		}
	}

}
void saveFourHistoMarker(TH1* histoOrig,TH1* histoOrig2,TH1* histoOrig3,TH1* histoOrig4,TLegend* legend,std::string xaxisTitle,std::string yaxisTitle,vector<std::string> stuffToWrite,std::string outPutDir,bool normalizedArea,Double_t xStart,Double_t yStart,Int_t yAxisStart,Int_t yAxisEnd,bool logScaleX=false,bool logScale=false,bool rebin=false);
void saveFourHistoMarker(TH1* histoOrig,TH1* histoOrig2,TH1* histoOrig3,TH1* histoOrig4,TLegend* legend,std::string xaxisTitle,std::string yaxisTitle,vector<std::string> stuffToWrite,std::string outPutDir,bool normalizedArea,Double_t xStart,Double_t yStart,Int_t yAxisStart,Int_t yAxisEnd,bool logScaleX,bool logScale,bool rebin) {
	TH1D *histo = (TH1D*)histoOrig->Clone();
	TH1D *histo2 = (TH1D*)histoOrig2->Clone();
	TH1D *histo3 = (TH1D*)histoOrig3->Clone();
	TH1D *histo4 = (TH1D*)histoOrig4->Clone();
	if (histo->GetEntries()+histo2->GetEntries()+histo3->GetEntries()+histo4->GetEntries()==0) {
		cout<<"histograms were empty"<<endl;
	} else {
		TCanvas *c1 = new TCanvas("c1","c1",0,0,600,600);

		if (logScale) {
			c1->SetLogy();
		}
		if (logScaleX) {
			c1->SetLogx();
		}
		TLatex latex;
		latex.SetNDC();
		latex.SetTextColor(1);
		latex.SetTextFont(42);
		latex.SetTextSize(.04);

		if (normalizedArea) {
			histo->Scale(1/(histo->Integral(1,histo->GetNbinsX())));
			histo2->Scale(1/(histo2->Integral(1,histo2->GetNbinsX())));
			histo3->Scale(1/(histo3->Integral(1,histo3->GetNbinsX())));
			histo4->Scale(1/(histo4->Integral(1,histo4->GetNbinsX())));

		}
		if (rebin) {
			histo->Rebin();
			histo2->Rebin();
			histo3->Rebin();
			histo4->Rebin();
		}

		histo->Draw("HIST P");
		//histo->GetXaxis()->SetNdivisions(30);
		//histo2->SetLineColor(4);
		histo2->Draw("P same HIST");
		histo3->Draw("P same HIST");
		histo4->Draw("P same HIST");
		Double_t yMax=histo->GetMaximum();
		Double_t yMin=histo->GetMinimum();

		if (histo2->GetMaximum()>yMax) {
			yMax=histo2->GetMaximum();
		}
		if (histo3->GetMaximum()>yMax) {
			yMax=histo3->GetMaximum();
		}
		if (histo4->GetMaximum()>yMax) {
			yMax=histo4->GetMaximum();
		}
		yMax=yMax*1.33;

		if (histo2->GetMinimum()<yMin) {
			yMin=histo2->GetMinimum();
		}
		if (histo3->GetMinimum()<yMin) {
			yMin=histo3->GetMinimum();
		}
		if (histo4->GetMinimum()<yMin) {
			yMin=histo4->GetMinimum();
		}
		yMin=yMin*.95;
		histo->SetMinimum(yMin);
		histo->SetMaximum(yMax);
		if (logScale) {
			histo->SetMinimum(.2);
			histo->SetMaximum(histo->GetMaximum()*1000);
		}
		histo->GetXaxis()->SetTitle(xaxisTitle.c_str());
		histo->GetXaxis()->SetLabelSize(0.04);
		//histo->GetXaxis()->SetNdivisions(505);
		//histo->GetXaxis()->SetLimits(-4.5,4.5);

		histo->GetXaxis()->SetTitleSize(.04);
		histo->GetYaxis()->SetTitle(yaxisTitle.c_str());
		histo->GetYaxis()->SetTitleSize(.04);
		histo->GetYaxis()->SetTitleOffset(1.75);
		//dataHisto->GetYaxis()->SetLabelSize(.04);
		histo->GetYaxis()->SetLabelSize(.04);

		legend->Draw();
		ATLASLabel3(xStart,yStart,"Internal",.85,.04);
		Double_t x=xStart;Double_t y=yStart-.045;
		for (int j=0;j<stuffToWrite.size();j++) {
			latex.DrawLatex(x,  y, stuffToWrite[j].c_str());
			y-=.045;
		}

		std::string temp(histo->GetName());
		if (rebin) {
			temp=temp+"rebin";
		}
		if (savePNG) {
			c1->Print((outPutDir+temp+".png").c_str(),"png");
		}
		if (savePDF) {
			c1->Print((outPutDir+temp+".pdf").c_str(),"pdf");
		}
	}

}
void saveTwoHistoRatio(TH1* histoOrig,TH1* histoOrig2,std::string xaxisTitle,std::string yaxisTitle,vector<std::string> stuffToWrite,std::string outPutDir,bool normalizedArea,Double_t xStart,Double_t yStart,Int_t yAxisStart,Int_t yAxisEnd,bool logScale=false,bool rebin=false);
void saveTwoHistoRatio(TH1* histoOrig,TH1* histoOrig2,std::string xaxisTitle,std::string yaxisTitle,vector<std::string> stuffToWrite,std::string outPutDir,bool normalizedArea,Double_t xStart,Double_t yStart,Int_t yAxisStart,Int_t yAxisEnd,bool logScale,bool rebin) {
	TH1D *histo = (TH1D*)histoOrig->Clone();
	TH1D *histo2 = (TH1D*)histoOrig2->Clone();
	TCanvas *c1 = new TCanvas("c1","c1",0,0,600,600);
	TPad *pad1 = new TPad("pad1","pad1",0,0.35,1,1);
	pad1->SetBottomMargin(0.0);
	pad1->SetLeftMargin(0.185);
	pad1->Draw();
	c1->cd();
	TPad *pad2 = new TPad("pad2","pad2",0,0,1,0.35);
	pad2->SetTopMargin(0.0);
	//pad2->SetBottomMargin(0.15);
	pad2->SetBottomMargin(0.3);
	pad2->SetLeftMargin(0.185);
	pad2->Draw();
	//pad2->SetGridy();

	pad1->cd();
	if (logScale) {
		c1->SetLogy();
	}
	TLatex latex;
	latex.SetNDC();
	latex.SetTextColor(1);
	latex.SetTextFont(42);
	latex.SetTextSize(.04);

	if (normalizedArea) {
		histo->Scale(1/(histo->Integral(1,histo->GetNbinsX())));
		histo2->Scale(1/(histo2->Integral(1,histo2->GetNbinsX())));

	}
	if (rebin) {
		histo->Rebin();
		histo2->Rebin();
	}

	histo->Draw("hist");
	histo2->SetLineColor(2);
	histo2->Draw("hist same");
	if (logScale) {
		histo->SetMinimum(.2);
		histo->SetMaximum(histo->GetMaximum()*1000);
	}
	histo->GetXaxis()->SetTitle(xaxisTitle.c_str());
	histo->GetXaxis()->SetLabelSize(0);
	histo->GetXaxis()->SetNdivisions(505);
	histo->GetXaxis()->SetTitleSize(.04);
	histo->GetYaxis()->SetTitle(yaxisTitle.c_str());
	histo->GetYaxis()->SetTitleSize(.04);
	histo->GetYaxis()->SetTitleOffset(1.75);
	//dataHisto->GetYaxis()->SetLabelSize(.04);
	histo->GetYaxis()->SetLabelSize(.04);


	ATLASLabel3(xStart,yStart,"Internal",.85,.04);
	Double_t x=xStart;Double_t y=yStart-.045;
	for (int j=0;j<stuffToWrite.size();j++) {
		latex.DrawLatex(x,  y, stuffToWrite[j].c_str());
		y-=.045;
	}
	pad2->cd();

	TH1F *h1_ratio = (TH1F*)histo->Clone();
	//h1_ratio->Add(h1_ratio,stack1,1,-1);
	h1_ratio->Divide(histo2);
	//h1_ratio->SetMarkerColor(colourAdd);
	//h1_ratio->GetYaxis()->SetRangeUser(0, 2.);
	h1_ratio->GetYaxis()->SetRangeUser(.5, 1.5);
	h1_ratio->GetYaxis()->SetTitle("Pos/Neg #eta");
	//h1_ratio->SetMaximum(.19);
	//h1_ratio->SetMinimum(-.19);
	h1_ratio->GetYaxis()->SetTitleSize(.12);
	h1_ratio->GetYaxis()->SetTitleOffset(.70);
	h1_ratio->GetYaxis()->SetLabelSize(.11);
	h1_ratio->GetXaxis()->SetTitle(xaxisTitle.c_str());
	h1_ratio->GetXaxis()->SetLabelSize(.11);
	h1_ratio->GetXaxis()->SetTitleOffset(1.1);
	/*h1_ratio->GetYaxis()->SetTitleSize(.08);
	h1_ratio->GetYaxis()->SetTitleOffset(.8);
	h1_ratio->GetYaxis()->SetLabelSize(.08);
	h1_ratio->GetXaxis()->SetTitle(xaxisTitle.c_str());
	h1_ratio->GetXaxis()->SetLabelSize(.08);
	h1_ratio->GetXaxis()->SetTitleOffset(.8);*/
	h1_ratio->GetYaxis()->SetNdivisions(10);
	//h1_ratio->GetYaxis()->SetTitleOffset(1.85);
	//h1_ratio->GetYaxis()->SetTitleFont(43);
	//h1_ratio->GetYaxis()->SetLabelSize(0);
	//h1_ratio->GetYaxis()->SetTitleSize(28);
	//h1_ratio->GetXaxis()->SetLabelFont(43);
	//h1_ratio->GetXaxis()->SetTitleFont(43);
	//h1_ratio->GetXaxis()->SetLabelSize(28);
	h1_ratio->GetXaxis()->SetTitleSize(.12);
	//h1_ratio->GetXaxis()->SetRangeUser(xmin, xmax);
	//TGaxis *ratioaxis = new TGaxis(xmin, 0.5, xmin, 1.5, 0.5, 1.5, 503, "");
	//ratioaxis->SetLabelFont(43);
	//ratioaxis->SetLabelSize(28);

	h1_ratio->Draw("P");


	TLine *line=new TLine(h1_ratio->GetXaxis()->GetXmin(),1,h1_ratio->GetXaxis()->GetXmax(),1);
	line->SetLineStyle(7);
	line->Draw();
	//ratioaxis->Draw();
	//cout<<"before save"<<endl;


	std::string temp(histo->GetName());
	if (rebin) {
		temp=temp+"rebin";
	}
	temp=temp+"ratio";
	if (savePNG) {
		c1->Print((outPutDir+temp+".png").c_str(),"png");
	}
	if (savePDF) {
		c1->Print((outPutDir+temp+".pdf").c_str(),"pdf");
	}

}
void saveTwoHistoRatioAsy(TH1* histoOrig,TH1* histoOrig2,TLegend* legend,std::string xaxisTitle,std::string yaxisTitle,vector<std::string> stuffToWrite,std::string outPutDir,bool normalizedArea,Double_t xStart,Double_t yStart,Int_t yAxisStart,Int_t yAxisEnd,bool logScale=false,bool rebin=false);
void saveTwoHistoRatioAsy(TH1* histoOrig,TH1* histoOrig2,TLegend* legend,std::string xaxisTitle,std::string yaxisTitle,vector<std::string> stuffToWrite,std::string outPutDir,bool normalizedArea,Double_t xStart,Double_t yStart,Int_t yAxisStart,Int_t yAxisEnd,bool logScale,bool rebin) {
	TH1D *histo = (TH1D*)histoOrig->Clone();
	TH1D *histo2 = (TH1D*)histoOrig2->Clone();
	TCanvas *c1 = new TCanvas("c1","c1",0,0,600,600);
	TPad *pad1 = new TPad("pad1","pad1",0,0.35,1,1);
	pad1->SetBottomMargin(0.0);
	pad1->SetLeftMargin(0.185);
	pad1->Draw();
	c1->cd();
	TPad *pad2 = new TPad("pad2","pad2",0,0,1,0.35);
	pad2->SetTopMargin(0.0);
	//pad2->SetBottomMargin(0.15);
	pad2->SetBottomMargin(0.3);
	pad2->SetLeftMargin(0.185);
	pad2->Draw();
	//pad2->SetGridy();

	pad1->cd();
	if (logScale) {
		c1->SetLogy();
	}
	TLatex latex;
	latex.SetNDC();
	latex.SetTextColor(1);
	latex.SetTextFont(42);
	latex.SetTextSize(.04);
	Double_t chi2=histo->Chi2Test(histo2,"CHI2");
	Double_t chi2ndf=histo->Chi2Test(histo2,"CHI2/NDF");
	Double_t ksTest=histo->KolmogorovTest(histo2);
	stuffToWrite.push_back("CHI2="+std::to_string(chi2));
	stuffToWrite.push_back("chi2ndf="+std::to_string(chi2ndf));
	stuffToWrite.push_back("ksTest="+std::to_string(ksTest));
	if (normalizedArea) {
		histo->Scale(1/(histo->Integral(1,histo->GetNbinsX())));
		histo2->Scale(1/(histo2->Integral(1,histo2->GetNbinsX())));

	}
	if (rebin) {
		histo->Rebin();
		histo2->Rebin();
	}

	histo->Draw("hist");
	histo2->SetLineColor(2);
	histo2->Draw("hist same");
	legend->Draw();
	if (logScale) {
		histo->SetMinimum(.2);
		histo->SetMaximum(histo->GetMaximum()*1000);
	}
	histo->GetXaxis()->SetTitle(xaxisTitle.c_str());
	histo->GetXaxis()->SetLabelSize(0);
	histo->GetXaxis()->SetNdivisions(505);
	histo->GetXaxis()->SetTitleSize(.04);
	histo->GetYaxis()->SetTitle(yaxisTitle.c_str());
	histo->GetYaxis()->SetTitleSize(.04);
	histo->GetYaxis()->SetTitleOffset(1.75);
	//dataHisto->GetYaxis()->SetLabelSize(.04);
	histo->GetYaxis()->SetLabelSize(.04);


	ATLASLabel3(xStart,yStart,"Internal",.85,.04);
	Double_t x=xStart;Double_t y=yStart-.045;
	for (int j=0;j<stuffToWrite.size();j++) {
		latex.DrawLatex(x,  y, stuffToWrite[j].c_str());
		y-=.045;
	}
	pad2->cd();

	TH1F *h1_ratio = (TH1F*)histo->Clone();
	TH1F *h1_den=(TH1F*)histo->Clone();
	h1_den->Add(histo2);
	//h1_ratio->Add(h1_ratio,stack1,1,-1);
	h1_ratio->Add(histo2,-1);
	h1_ratio->Divide(h1_den);
	//h1_ratio->SetMarkerColor(colourAdd);
	//h1_ratio->GetYaxis()->SetRangeUser(0, 2.);
	h1_ratio->GetYaxis()->SetRangeUser(-.5, .5);
	h1_ratio->GetYaxis()->SetTitle("(Pos-Neg)/(Pos+Neg) #eta");
	//h1_ratio->SetMaximum(.19);
	//h1_ratio->SetMinimum(-.19);
	h1_ratio->GetXaxis()->SetTitle(xaxisTitle.c_str());
	h1_ratio->GetXaxis()->SetTitleSize(.075);
	h1_ratio->GetXaxis()->SetLabelSize(.075);
	//h1_ratio->GetYaxis()->SetTitle(yaxisTitle.c_str());
	h1_ratio->GetYaxis()->SetTitleSize(.075);
	h1_ratio->GetYaxis()->SetTitleOffset(1.0);
	//dataHisto->GetYaxis()->SetLabelSize(.04);
	h1_ratio->GetYaxis()->SetLabelSize(.075);

	/*h1_ratio->GetYaxis()->SetTitleSize(.08);
	h1_ratio->GetYaxis()->SetTitleOffset(.8);
	h1_ratio->GetYaxis()->SetLabelSize(.08);
	h1_ratio->GetXaxis()->SetTitle(xaxisTitle.c_str());
	h1_ratio->GetXaxis()->SetLabelSize(.08);
	h1_ratio->GetXaxis()->SetTitleOffset(.8);*/
	h1_ratio->GetYaxis()->SetNdivisions(5);
	//h1_ratio->GetXaxis()->SetRangeUser(xmin, xmax);
	//TGaxis *ratioaxis = new TGaxis(xmin, 0.5, xmin, 1.5, 0.5, 1.5, 503, "");
	//ratioaxis->SetLabelFont(43);
	//ratioaxis->SetLabelSize(28);

	h1_ratio->Draw("P");


	TLine *line=new TLine(h1_ratio->GetXaxis()->GetXmin(),1,h1_ratio->GetXaxis()->GetXmax(),1);
	line->SetLineStyle(7);
	line->Draw();
	//ratioaxis->Draw();
	//cout<<"before save"<<endl;


	std::string temp(histo->GetName());
	if (rebin) {
		temp=temp+"rebin";
	}
	temp=temp+"Asy";
	if (savePNG) {
		c1->Print((outPutDir+temp+".png").c_str(),"png");
	}
	if (savePDF) {
		c1->Print((outPutDir+temp+".pdf").c_str(),"pdf");
	}

}
Double_t getWidth(TH1D* histo) {
	int mean_bin = histo->FindBin(histo->GetMean());
	if (debug==true) {
		cout<< "mean_bin="<<mean_bin<<" mean="<<histo->GetMean()<<endl;
	}

    int binup = mean_bin;
    int bindown = mean_bin-1;
    Double_t total = histo->Integral();
    Double_t runningup = 0;
    Double_t runningdown = 0;
    if (total<50) {
		return -10.;
	}
    while (runningup < 0.6827/2.0*total) {
        runningup += histo->GetBinContent(binup);
        binup += 1;
	}
    while (runningdown < 0.6827/2.0*total) {
        runningdown += histo->GetBinContent(bindown);
        bindown -= 1;
	}

    Double_t width = (histo->GetBinCenter(binup)-histo->GetBinCenter(bindown))/2.;
    return width/1000.;
}
void Save2Plots(const std::string& configFile) {
	TEnv *config = new TEnv(configFile.c_str());
	if ( !config ) {
		printf("configFile=%s doesn't appear to exists\n",configFile.c_str());
	}
	std::string outPutDir = config->GetValue("outPutDir","blah");
	std::string inputFileFullPU = config->GetValue("inputFileFullPU","blah");
	std::string inputFileNoPU = config->GetValue("inputFileNoPU","blah");
	std::string nameOfHistogram = config->GetValue("nameOfHistogram","blah");
	std::string yAxisTitle = config->GetValue("yAxisTitle","blah");
	std::string PtBins= config->GetValue("PtBins","20:25");
	std::string temp;
	int nPtBins=-1;
	printf("PtBins=%s\n",PtBins.c_str());
	stringstream ss3(PtBins);
	vector<std::string> PtBinsVector;
	while(getline(ss3,temp,':')) {
		PtBinsVector.push_back(temp);
		nPtBins++;
	}
	Float_t bins[PtBinsVector.size()];
	for (int i=0;i<PtBinsVector.size();i++) {
		std::cout<<"PtBinsVector["<<i<<"]="<<PtBinsVector[i]<<std::endl;
		bins[i]=std::stoi(PtBinsVector[i]);
		std::cout<<"bins["<<i<<"]="<<bins[i]<<std::endl;
	}
	SetAtlasStyle();
	
	//int nEtaSteps=7;
	//Double_t etaBlocks[]={0,.8,1.2,2.1,2.8,3.2,3.6,4.5};
	//int nEtaStepsSecondary=7;
	//Double_t etaBlocksSecondary[]={2.1,2.2,2.3,2.4,2.5,2.6,2.7,2.8};
	//int nEtaStepsSecondary=1;
	//Double_t etaBlocksSecondary[]={0,2.1};
	//int startEtaStep=0;
	//int nEtaStepsToTake=7;
	int histNameInt=0;
	//int nBinsForA=1000;
	//int maxMu=35;
	//int maxNPV=30;


	TH1D *R4ConeEM_etaSlicesTruth1;
	TH1D *R4ConeEM_etaSlicesTruth2;


	//TF1 *myfit = new TF1("myfit","[1]/x^2 +[2]/x+[0]/x^3", 20, 3500);
	TF1 *myfit;// = new TF1("myfit","[1]/x", 20, 100);
	//TF1 *myfit = new TF1("myfit","[0]/x +[1]", 20, 3500);

	//TF1 *myfit = new TF1("myfit","[0]/x +[1]+[2]/sqrt(x)", 21, 3500);
	//TF1 *myfit = new TF1("myfit","[0]/x +[1]+[2]/x^(1/2)", 21, 1000);
	//TF1 *myfit = new TF1("myfit","exp(-[0]*x)*[1]", 21, 1000);
	/*myfit->SetParName(0,"N");
	myfit->SetParName(1,"C");
	myfit->SetParameter(0,1.);
	myfit->SetParameter(1,1.);
	myfit->SetLineColor(2);*/
	TF1 *fitResultsTemp;
	FILE* fitFile;
	int maxPt2=3800;
	int steps=100;
	int nSteps=37;
	int startFit=45;
	char buff1[15];
	//R4ConeEM_etaSlicesSecondaryTruth[0]->Fit(myfit,"WS");
	TFile *infile;
	infile = TFile::Open(inputFileNoPU.c_str());
	printf("opened the file\n");
	//infile = TFile::Open("FullPUResolutionV101RightCustomRange.root");
	//infile = TFile::Open("FitNoiseNew_NoPileupSamplesRightCustomRange.root");
	printf("going ot get %s\n", nameOfHistogram.c_str());
	R4ConeEM_etaSlicesTruth1=(TH1D*)infile->Get(nameOfHistogram.c_str())->Clone();

	printf("got a histo\n");
	R4ConeEM_etaSlicesTruth1->SetDirectory(0);
	R4ConeEM_etaSlicesTruth1->SetMarkerStyle(20);
	R4ConeEM_etaSlicesTruth1->SetMarkerColor(4);
	R4ConeEM_etaSlicesTruth1->SetLineColor(4);
	infile->Close();
	//infile = TFile::Open("NoPUResolutionV4RightFullRange.root");
	//infile = TFile::Open("NoPUResolutionV4RightCustomRange.root");
	infile = TFile::Open(inputFileFullPU.c_str());
	printf("opened the full PU file\n");
	R4ConeEM_etaSlicesTruth2=(TH1D*)infile->Get(nameOfHistogram.c_str())->Clone();
	printf("get a histo\n");
	R4ConeEM_etaSlicesTruth2->SetDirectory(0);
	R4ConeEM_etaSlicesTruth2->SetMarkerStyle(20);
	R4ConeEM_etaSlicesTruth2->SetMarkerColor(2);
	R4ConeEM_etaSlicesTruth2->SetLineColor(2);
	infile->Close();



	cout<<"Hi"<<endl;
	legend2 = new TLegend(.50,.825,1.,.92);
	legend2->SetFillColor(0);
	legend2->SetFillStyle(0);//make it transparent
	legend2->SetBorderSize(0);
	legend2->SetTextSize(0.04);
	legend2->SetTextFont(42);
	legend2->SetMargin(.15);
	legend2->AddEntry(R4ConeEM_etaSlicesTruth2,"2015 pile-up (<#mu>=12.86)","P");
	legend2->AddEntry(R4ConeEM_etaSlicesTruth1,"No pile-up with correction","P");
	vector<std::string> stuffToWrite4;
	//saveOneHistoMarker("FullPU",R4ConeEM_etaSlicesTruth1,"p_{T}^{true}","#sigma_{Res}(p_{T})",stuffToWrite4,"/home/khildebr/qualTask/run/TempPic/",false,0.20,0.88,0,0,true,false);
	saveTwoHistoMarker(R4ConeEM_etaSlicesTruth1,R4ConeEM_etaSlicesTruth2,legend2,"p_{T}^{true}",yAxisTitle,stuffToWrite4,outPutDir,false,0.20,0.88,0,0,true,false);
	//saveTwoHistoMarker(R4ConeEM_etaSlicesTruth2,R4ConeEM_etaSlicesTruth1,legend2,"p_{T}^{true}","#sigma_{Res}(p_{T})",stuffToWrite4,"/home/khildebr/qualTask/run/TempPic2/",false,0.20,0.88,0,0,true,false);


}
