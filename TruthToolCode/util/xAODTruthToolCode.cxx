
#include "TruthToolCode/TruthToolCode.h"
#include "xAODRootAccess/tools/TFileAccessTracer.h"
int main(int argc, char** argv) {
	xAOD::TFileAccessTracer::enableDataSubmission( false );
	xAOD::Init();
	xAOD::TStore store; // create a transient object store (needed for the tools)
	//xAOD::TEvent event;
	xAOD::TEvent event(xAOD::TEvent::kClassAccess);
	std::vector<TString> infiles = vectorise(TString(argv[1]),",");
	if (infiles.size()<1) {
		std::cout << "Couldn't find any input files!" << std::endl;
		abort();
	}

	for ( TString str : infiles )
		std::cout << str << std::endl;

	TFile *f = TFile::Open(infiles[0].Data(),"read");
	if ( !f->IsOpen() ) {
		std::cout << "Failed to open file " << infiles[0] << std::endl;
		abort();
	}


	std::string CONFIG = argv[2];
	if( CONFIG == "" ){
		std::cout << "no config file provided, using default " << std::endl;
		CONFIG="settings.config";
	}

	std::string m_rootCoreBin = std::string(getenv("ROOTCOREBIN"));
	std::string settings_file = m_rootCoreBin + "/../TruthToolCode/share/" + CONFIG;
	std::cout << "Loading config file " << settings_file <<"    ......" <<std::endl;


	event.readFrom(f);

	JetPerformance::TruthToolCode analysis;
	analysis.Initialise(event, settings_file);

	for (const TString& filename : infiles) {
		std::cout << "Opening " << filename << std::endl;

		TFile* f = TFile::Open(filename.Data());

		event.readFrom(f);
	        const unsigned int n_entries = event.getEntries();
                std::cout << " Processing file: " << filename << std::endl;
		std::cout << " Entries in file: " << n_entries << std::endl;

	        for (unsigned int ientry = 0; ientry < n_entries; ++ientry) {
		//for (unsigned int ientry = 0; ientry < 300; ++ientry) {
			if (ientry % 100 == 0)
				std::cout << " Processing event " << ientry << " / " << n_entries << std::endl;

			event.getEntry(ientry);
			analysis.ProcessEvent(event, store);
		}
	}

	analysis.Finalise();

	return 0;
}
