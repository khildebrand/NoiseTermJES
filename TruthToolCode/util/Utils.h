#ifndef Utils_h
#define Utils_h



const float GeV = 1000.;
float refRegion = 0.8;
float centralRegion = 2.4;

bool descendingPt(xAOD::Jet* a, xAOD::Jet* b) { return a->pt() > b->pt(); }

float deltaPhi(const xAOD::Jet& j1, const xAOD::Jet& j2) { 
  return TVector2::Phi_mpi_pi(j1.phi()-j2.phi());
}

TH1F* createHist1D(TString hname, TString title, int nbins, int xlow, int xhigh) {
  TH1F* h = new TH1F(hname,title,nbins,xlow,xhigh);
  return h;
}

TH1F* createHist1D(TString hname, TString title, int nbins, std::vector<double> bins) {
  TH1F* h = new TH1F(hname,title,nbins,&bins[0]);
  return h;
}

std::vector<TString> vectorise(TString str, TString sep=" ") {
  std::vector<TString> result; TObjArray *strings = str.Tokenize(sep.Data());
  if (strings->GetEntries()==0) { delete strings; return result; }
  TIter istr(strings);
  while (TObjString* os=(TObjString*)istr()) result.push_back(os->GetString());
  delete strings; return result;
}   

std::vector<double> vectoriseD(TString str, TString sep) {
  std::vector<double> result; std::vector<TString> vecS = vectorise(str);
  for (uint i=0;i<vecS.size();++i)
    result.push_back(atof(vecS[i]));
  return result;
}   

#endif
