
#ifndef TruthToolCode_h
#define TruthToolCode_h
#include <EventLoop/Algorithm.h>
//#include <EventLoop/TEventSvc.h>

// C++ includes
#include <iostream>
#include <string>
#include <map>
#include <unordered_map>

// xAOD ROOT access
#ifdef ROOTCORE
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"
#include "xAODRootAccess/TActiveStore.h"
#endif

#include "xAODEventInfo/EventInfo.h"
#include "xAODCore/ShallowCopy.h"
#include "xAODJet/JetContainer.h"
#include "xAODJet/JetAuxContainer.h"
#include "xAODTruth/TruthParticleContainer.h"
// ROOT includes
#include "TFile.h"
#include "TString.h"
#include "TH1.h"
#include "TH2.h"
#include "TH3.h"
#include "TEnv.h"
#include "TLorentzVector.h"
// Tools
#include "GoodRunsLists/GoodRunsListSelectionTool.h"
#include "JetCalibTools/JetCalibrationTool.h"
#include "JetSelectorTools/JetCleaningTool.h"

//Histo Libraries
#include "TruthToolCode/JetPerfHistograms.h"
#include "TruthToolCode/JetPerf3DHistograms.h"
#include "TruthToolCode/JetPerf4DHistograms.h"
#include "TruthToolCode/JetPerf4DHistogramsKevin.h"
#include "util/Utilities.h"


#include "xAODEventShape/EventShape.h"
#include "xAODEventShape/EventShapeAuxInfo.h"

#include "JetCalibTools/JetCalibrationTool.h"
#include "JetRec/PseudoJetGetter.h"//

#include "PileupReweighting/PileupReweightingTool.h"
//#include "EventShapeTools/EventDensityTool.h"//
//#include "EventShapeTools/EventShapeCopier.h"//


//class EventShapeCopier;


const float GeV = 1000.;

std::vector<TString> vectorise(TString str, TString sep=" ") {
  std::vector<TString> result; TObjArray *strings = str.Tokenize(sep.Data());
  if (strings->GetEntries()==0) { delete strings; return result; }
  TIter istr(strings);
  while (TObjString* os=(TObjString*)istr()) result.push_back(os->GetString());
  delete strings; return result;
}

std::vector<double> vectoriseD(TString str, TString sep=" ") {
  std::vector<double> result; std::vector<TString> vecS = vectorise(str);
  for (uint i=0;i<vecS.size();++i)
    result.push_back(atof(vecS[i]));
  return result;
}

std::vector<double> makeUniformVec(int N, double min, double max) {
  std::vector<double> vec; double dx=(max-min)/N;
  for (int i=0;i<=N;++i) vec.push_back(min+i*dx);
  return vec;
}

namespace JetPerformance {

  class TruthToolCode {

  public:
  double dataScaleFactor; //!
  double dataScaleFactorUP; //!
  double dataScaleFactorDOWN; //!
  float PileupWeight; //!
  CP::PileupReweightingTool*   mc_pileuptool; //!
  bool m_doPUreweighting;
  std::string mc_lumiCalcFileNames;
  std::string mc_PRWFileNames;
  float averageMu; //!
  float correctedAverageMu; //!
	  // Constructor
	  TruthToolCode();
	  // Destructor
	  ~TruthToolCode();

	  // Initialise
	  void Initialise(xAOD::TEvent& event, std::string settings_file);
	  // Per event analysis
	  void ProcessEvent(xAOD::TEvent& event , xAOD::TStore& transDataStore);
	  // Finalise
	  void Finalise();

//	  void InitialiseTools();
	  void InitialiseJetTools(TString jetAlgo);
	  void InitialiseHistograms(TString DR, TString jetalgo);
	  //void InitialiseRho(TString jetcollection);
	  void FillHistograms(TString recoPrefix, unsigned int jetIndex,xAOD::TEvent& event);
	  void WriteHistograms();
//	   virtual void    SetInputList(TList *input) { fInput = input; }
//	   virtual TList  *GetOutputList() const { return fOutput; }
    //xAOD::TEvent* m_event;
    //xAOD::TStore* transDataStore; //!

    //std::vector<EventDensityTool*> v_edtools;
    //EventDensityTool *edtool; //!
    TEnv* m_settings;

    float m_weight = 1;
    int m_bcid = -1 ;
    float m_mu;
    bool m_isMC;
    int  m_JX;
    int  m_mcChannelNumber;

    std::vector < TH2F * > m_bcidVsRespForward;

    // map reco jet prefix to corresponding truth jet prefix
    std::map< TString, TString > jetCollectionMap;
    // map reco jet prefix to corresponding jet size
    std::map< TString, double > jetSizeMap;

    //name of the config file for the jet calib tool
    TString  m_jetToolConf;

     //the vector of jet calibration names and tools
     std::vector<TString> v_reco_calibName;
     std::vector<JetCalibrationTool*> v_JESTools;
     bool m_applyRecalibration;
     bool m_applyRecalibrationJetArea = false;
     bool m_isxaod;
     bool m_ismc15;
     // Store the number of events for the reweighting
     TH1F * hEventCounter;
     TH1F * hNoTruthJetsEventCounter;

     // Needed for recalibration tool on NTUP_TOP
     bool m_isNTUPTOP = false;
//     bool m_applyMuScaling;
//     bool m_applyNPVcorr;
     unsigned int m_eventNumber;
     // OutputFile
     TFile * m_outputfile;
     TString m_outputFileName;
     double dRInput;
     int histnbr;
     int noTruthJetHistnbr;
     int nevent;
     // Flags for the flavor histograms
     // set to true if we are interested in gluon, light quark, flavour inclusive (=all) and unmatched jets histos
     bool m_doGluLight = true;
     // set to true if we are also (i.e. in addition to light and gluon plots) interested in c and b jet histos
     bool m_doCB = true;
     // number of flavor histos
     int m_nFlavorHistos;
     int requiredTracksPerVertex = 2;
     // Whether isolation cut is applied
     bool m_applyIsolation, m_applyIsolation_true;
     bool noWeight;
     // pt threshold for truth jets considered to calculate distance to the closest truth jet
     double ptMinTrueIso;
     double m_dR;
     // pt and energy thresholds for jets considered to calculate distance to the closest reco jet
     float ptMinRecoIso;
     float energyMinRecoIso;
     float m_truthrecmatch;
     //eta and pt bins
     std::string etaBins;
     std::string etaBins2;
     std::string ptBins;
     std::string NPVBins;
     int RatioBinsUpper;
     int RatioBinsLower;
     int RationBinsNumber;
     int PtTruthBinsUpper;
     int PtTruthBinsLower;
     int PtTruthBinsNumber;

     float deltaR(float eta1, float phi1, float eta2, float phi2);
     float deltaR(float eta1, float phi1, float eta2, float phi2, float ptEnergy2, float minptEnergy);
     float deltaR(float eta1, float phi1, float eta2, float phi2, float pt2, float e2, float minpt, float mine);
     float deltaPhi(float phi1, float phi2);

     bool isFirstEvent;
     bool m_applyBCIDCut;
     std::map<TString,int> recindex;

    // Tools
    std::string m_rootCoreBin; // path to $ROOTCOREBIN
    //JetCalibrationTool* m_jetCalibTool;

    //std::map<std::string,JetCleaningTool*>  m_jetCleaningTools;
    std::vector<JetCleaningTool*>  m_jetCleaningTools;
    //EventShapeCopier* m_EventShapeCopier;

    std::vector<TString> m_recoJetCollections;
    std::vector<TString> m_truthJetCollections;
    std::vector< unsigned int > m_bcid_excl;

    xAOD::JetContainer* goodJets; //!
    xAOD::JetAuxContainer* goodJetsAux; //!

    // Histograms
    std::vector<JetPerf4DHistograms*> m_allTestHistograms;
    std::vector<JetPerf4DHistograms*> m_allTestHistograms2;
    std::vector<JetPerf4DHistograms*> m_allTestHistograms3;
    std::vector<JetPerf4DHistograms*> m_allTestHistogramsMu;
    std::vector<JetPerf4DHistograms*> m_allTestHistogramsMuCorrected;
    std::vector<JetPerf4DHistograms*> m_allTestHistogramsNPV;
    std::vector<JetPerf4DHistograms*> m_allTestHistogramsNPVType;
    std::vector<JetPerf4DHistogramsKevin*> m_allKevinHistogramsRatioVsTruth;
    std::vector<JetPerf4DHistogramsKevin*> m_allKevinHistogramsRatioVsCalib;
    std::vector<JetPerf4DHistogramsKevin*> m_allKevinHistogramsRatioVsTruth2;
    std::vector<JetPerf4DHistogramsKevin*> m_allKevinHistogramsRatioVsCalib2;
    std::vector<JetPerf4DHistograms*> m_allPtHistograms;
    std::vector<JetPerfHistograms*> m_nJetsVsPtHistograms;
    std::vector<JetPerfHistograms*> m_nNoTruthJetsVsPtHistograms;

  };



}

#endif
