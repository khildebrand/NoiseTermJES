#ifndef JETPERFHISTOGRAMS
#define JETPERFHISTOGRAMS

#include "TH1D.h"
#include <map>
#include <iostream>

/** 
 * @class JetPerfHistograms
 *
 * @brief This class contains a set of TH1D vs pT, one for each eta bin
 *
 * The JetPerfHistograms class handles a map of nJets vs pT histograms, 
 * indexed by (low eta edge, high eta edge) 
 *
 * Helper functions allow to print the histo content on screen (for cutflow purposes),
 * obtain histograms of nJets vs pT integrating over all eta, 
 * and histogram of nJets as a function of eta 
 * Technical note: the implementation of inheriting from a map has the advantage 
 * that the histograms can be accessed outside the class via the find() method of the map. 
 *
 * @author Caterina Doglioni
 * 
 * $Header $
 */

class JetPerfHistograms : public std::map< std::pair<double, double> , TH1D *> {

public :

  typedef std::map< std::pair<double, double>, TH1D *> histoMap_t;


  /**
   * @brief Constructor
   * 
   * @param[in] name The name of the histogram bundle - every histogram will be called
   * @param[in] etaBinsString A string of low bin edges for eta, no spaces, separated by :
   * @param[in] ptBinsString A string of low bin edges for pt (for all eta histograms), no spaces, separated by :
   * @param[in] absEta Flag setting whether this histogram bundle is filled according to absolute values of eta. Note that consistency in the Fill function is not checked.  
   */
  JetPerfHistograms(const std::string & name, const std::string & etaBinsString, const std::string & ptBinsString, const bool absEta);
  //JetPerfHistograms(const std::string & name, const std::string & etaBinsString, std::vector<double> & ptBinsVecDouble, const bool absEta);
  //TODO: Deep-copy constructor!

  /**
   * @brief Destructor
   * 
   * Makes sure there are no memory leaks. Hopefully ROOT won't complain.... 
   */
  virtual ~JetPerfHistograms() {

  //this segfaults! I assume that ROOT will delete the histograms itself...
/*    const histoMap_t::iterator first = this->begin();
    const histoMap_t::iterator last = this->end();

    iterate on the map and kill the content 
    for (histoMap_t::iterator histoIter = first; histoIter != last; histoIter++){
      delete histoIter->second;
    }*/
  };

  /**
   * @brief Clears the content (but not the binning) of the histograms
   */
  void clearHistograms();

  /**
   * @brief Writes the histograms in the current directory
   */
  void writeHistograms();

  /**
   * @brief Writes the histograms in the current directory
   */
  void deleteHistograms();

  /**
   * @brief Fills the histogram with the jet 
   */
  //TODO: optimize!
  void fillHistograms(const double & jet_eta, const double & jet_pt, const double & jet_weight);


private :

  /**
   * @brief Function to look for the correct index of the eta bin
   * It will return an iterator in the map where one can fill the correct histogram
   */ 
  //TODO: call from Fill function, this is private!
  const histoMap_t::iterator findEtaHisto (double eta);

  /**
   * @brief Instantiates one histograms
   */
  void addHistogram(const double & lowEta, const double & highEta, const std::vector<double> & ptBins);

  /**
   * @brief Prepares the integrated eta histogram vs pt, to be called in finalization stage
   */
  void getIntegratedEtaHistogram();
  /**
   * @brief Prepares the integrated pt histogram vs eta, to be called in finalization stage
   */
  void getIntegratedPtHistogram();

  /**
   * @brief The binning vector for the eta dimensions: low edge eta
   */ 
  std::vector<double> m_etaBins;

  /**
   * @brief The flag for absolute value of eta
   */ 
  bool m_doAbsEta;

  /**
   * @brief The name of the histograms
   */ 
  std::string m_name;

  /**
   * @brief The binning vector for pT dimension 
   *
   * For now, it is assumed that all eta bins will have the same pT bin 
   * In the future, this can be extended to be a vector/map
   */ 
  std::vector<double> m_ptBins;

  /////Helpers

  /**
   * @brief Retrieves a vector of doubles starting from a colon(:)-delimited list of numbers
   */
  std::vector<double> tokenizeBins(const std::string & binString);

  /**
   * @brief Sanity checks on the vector of low bin edges (e.g. they are in increasing order)
   */
  bool checkBinVector(std::vector<double> binVector);

};


#endif
