#ifndef JETPERF3DHISTOGRAMS
#define JETPERF3DHISTOGRAMS
#include "TH1D.h"

#include <map>
#include <iostream>
/** 
 * @class JetPerfHistograms
 * 
 * @brief This class contains a set of TH1D, one for each eta and pt bin
 *
 * The JetPerfHistograms class handles a map of histograms, 
 * indexed by (low eta edge, high eta edge) and (low pt edge, high pt edge) 
 *
 * @author Caterina Doglioni, adapted by Stefanie Adomeit
 * 
 * $Header $
 */




typedef std::map< std::pair<double, double>, TH1D *> histoMap_t;


class JetPerf3DHistograms : public std::map< std::pair<double, double> , histoMap_t *>  {

public :

  typedef std::map< std::pair<double, double>, histoMap_t *> histoMap3d_t;  

  /**
   * @brief Constructor
   * 
   * @param[in] name The name of the histogram bundle - every histogram will be called
   * @param[in] etaBinsString A string of low bin edges for eta, no spaces, separated by :
   * @param[in] ptBinsString A string of low bin edges for pt (one for each eta bin), no spaces, separated by :
   * @param[in] binsString A string of low bin edges for the third dimension, no spaces, separated by :
   * @param[in] absEta Flag setting whether this histogram bundle is filled according to absolute values of eta. Note that consistency in the Fill function is not checked.  
   * @param[in] xAxisTitle The x-axis title of the histograms
   * @param[in] yAxisTitle The y-axis title of the histograms - by default Number of jets
   */
  JetPerf3DHistograms(const std::string & name, const std::string & etaBinsString, const std::string & ptBinsString, const std::string & binsString, const bool absEta, const std::string & xAxisTitle, const std::string & yAxisTitle = "Number of jets");
  //TODO: Deep-copy constructor!

  /**
   * @brief Destructor
   * 
   * Makes sure there are no memory leaks. Hopefully ROOT won't complain.... 
   */
  virtual ~JetPerf3DHistograms() {
    const histoMap3d_t::iterator first = this->begin();
    const histoMap3d_t::iterator last = this->end();
    
    for (histoMap3d_t::iterator histoListIter = first; histoListIter != last; histoListIter++){
      if (histoListIter->second != 0) {
	
	const histoMap_t::iterator firstHisto = (histoListIter->second)->begin();
        const histoMap_t::iterator lastHisto = (histoListIter->second)->end();
	
	for (histoMap_t::iterator histoIter = firstHisto; histoIter != lastHisto; histoIter++){
        if (histoIter->second != 0) delete histoIter->second;
        }
      }
    }
  };
  
  /**
   * @brief Clears the content (but not the binning) of the histograms
   */
  void clearHistograms();

  /**
   * @brief Writes the histograms in the current directory
   */
  void writeHistograms();

  /**
   * @brief Deletes all histograms
   */
  void deleteHistograms();

  /**
   * @brief Fills the histogram with the jet 
   */
  void fillHistograms(const double & jet_eta, const double & jet_pt, const double & jet_value, const double & jet_weight);  
   
  
private:
  
   /**
   * @brief Function to look for the correct index of the eta bin
   * It will return an iterator in the map where one can fill the correct histogram
   */ 
  //TODO: call from Fill function, this is private!
  const histoMap3d_t::iterator findEtaHisto(double eta); 
  
    /**
   * @brief Function to look for the correct index of the eta bin
   * It will return an iterator in the map where one can fill the correct histogram
   */ 
  //TODO: call from Fill function, this is private!
  const histoMap_t::iterator findPtHisto(double pt, const JetPerf3DHistograms::histoMap3d_t::iterator etaHistoIter);  

  /**
   * @brief The binning vector for the eta dimensions: low edge eta
   */ 
  std::vector<double> m_etaBins;

  /**
   * @brief The flag for absolute value of eta
   */ 
  bool m_doAbsEta;

  /**
   * @brief The name of the histograms
   */ 
  std::string m_name;

  /**
   * @brief The binning vector for pT dimension 
   *
   * For now, it is assumed that all eta bins will have the same pT bin 
   * In the future, this can be extended to be a vector/map
   */ 
  std::vector<double> m_ptBins; 
  
   /**
   * @brief The binning vector for the third dimensions: low edge bins
   */ 
  std::vector<double> m_3dBins;
  
   /**
   * @brief xAxisTitle
   */ 
  std::string m_xAxisTitle;  
  
   /**
   * @brief yAxisTitle
   */ 
  std::string m_yAxisTitle;  
   
 
  
  /**
   * @brief Book new histogram
   */  
  TH1D* bookHistogram(const double & lowEtaBin, const double & highEtaBin, const double & lowPtBin, const double & highPtBin, const std::vector<double> & Bins);

  /**
   * @brief Instantiates one histograms
   */  
  void addHistogram(const double & lowEta, const double & highEta, const double & lowPt, const double & highPt, const std::vector<double> & bins, histoMap_t *  histoMap);

};


#endif
