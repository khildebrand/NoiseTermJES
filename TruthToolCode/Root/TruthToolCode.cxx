/*
 * Jet performance code 2015
 */

#include "TruthToolCode/TruthToolCode.h"
#include "xAODRootAccess/tools/Message.h"
#define EL_RETURN_CHECK( CONTEXT, EXP )                     \
   do {                                                     \
      if( ! EXP.isSuccess() ) {                             \
         Error( CONTEXT,                                    \
                XAOD_MESSAGE( "Failed to execute: %s" ),    \
                #EXP );                                     \
      }                                                     \
   } while( false )

// Constructor
JetPerformance::TruthToolCode::TruthToolCode() {

}

// Destructor
JetPerformance::TruthToolCode::~TruthToolCode() {

}

// Initialise
void JetPerformance::TruthToolCode::Initialise(xAOD::TEvent& event, std::string settings_file) {
	//TEventSvc *const svc = dynamic_cast<TEventSvc*>(getAlg (TEventSvc::name));
	//transDataStore =  svc->store();
	mc_lumiCalcFileNames = "";
    mc_PRWFileNames      = "";
    dataScaleFactor		 = 1.;
    dataScaleFactorUP		 = -1;
    dataScaleFactorDOWN		 = -1;
    m_doPUreweighting   = false;
	TH1::SetDefaultSumw2(kTRUE);



	// get path to RootCoreBin and extract information from the config file

	m_rootCoreBin = std::string(getenv("ROOTCOREBIN"));
	if(settings_file == "")settings_file = m_rootCoreBin + "/../TruthToolCode/share/settings.config";
	std::cout << "Reading settings file " << settings_file << std::endl;
	m_settings = new TEnv();
	m_settings->ReadFile(settings_file.c_str(),EEnvLevel(0));

	mc_lumiCalcFileNames = m_settings->GetValue("LumiCalcFiles",       mc_lumiCalcFileNames.c_str());
    mc_PRWFileNames      = m_settings->GetValue("PRWFiles",            mc_PRWFileNames.c_str());
    dataScaleFactor      = m_settings->GetValue("DataScaleFactor",            dataScaleFactor);
    dataScaleFactorUP      = m_settings->GetValue("DataScaleFactorUP",            dataScaleFactorUP);
    dataScaleFactorDOWN      = m_settings->GetValue("DataScaleFactorDOWN",            dataScaleFactorDOWN);
    m_doPUreweighting   = m_settings->GetValue("DoPileupReweighting", m_doPUreweighting);

	m_recoJetCollections = vectorise(m_settings->GetValue("RecoJetCollections",""));
	v_reco_calibName = vectorise(m_settings->GetValue("RecoCalibName",""));
	m_truthJetCollections = vectorise(m_settings->GetValue("TruthJetCollections",""));
	m_truthrecmatch = m_settings->GetValue("DrMatch",0.3);
	m_applyIsolation = m_settings->GetValue("ApplyIsolationCut",true);
	m_applyIsolation_true = m_settings->GetValue("ApplyTruthIsolationCut",true);
	m_applyRecalibration = m_settings->GetValue("ApplyRecalibration",true);
	m_doGluLight = m_settings->GetValue("DoGluLight",true);
	m_doCB = m_settings->GetValue("DoCB",true);
        m_jetToolConf = m_settings->GetValue("JetCalibConfigFile","JES_2015dataset_recommendation_Feb2016.config");
          std::cout << "The name of the jet tool is : " << m_jetToolConf << std::endl;
        ptMinTrueIso = m_settings->GetValue("ClosestTruthPtMin",7000.);
	ptMinRecoIso = m_settings->GetValue("ClosestRecoPtMin",7000.);
	energyMinRecoIso = m_settings->GetValue("ClosestRecoEMin",0.);
	etaBins = m_settings->GetValue("EtaBins","0:0.3:0.8:1.2:2.1:2.8:3.6:4.4");

	etaBins2 = m_settings->GetValue("EtaBins2","0:0.3:0.8:1.2:2.1:2.8:3.6:4.4");
	RatioBinsUpper = m_settings->GetValue("RatioBinsUpper",2);
	RatioBinsLower = m_settings->GetValue("RatioBinsLower",0);
	RationBinsNumber = m_settings->GetValue("RationBinsNumber",200);
	PtTruthBinsUpper = m_settings->GetValue("PtTruthBinsUpper",5000);
	PtTruthBinsLower = m_settings->GetValue("PtTruthBinsLower",0);
	PtTruthBinsNumber = m_settings->GetValue("PtTruthBinsNumber",1000);

	ptBins = m_settings->GetValue("PtBins","10:20:30:45:60:80:110:160:210:260:310:400:500:600:800:1000:1200:1500:1800:2500");
	NPVBins = m_settings->GetValue("NPVBins","0:1:2:3:4:5:6:7:8:9:10:11:12:13:14:15:16:17:18:19:20:21:22:23:24:24:26:27:28:29:30");
	noWeight = m_settings->GetValue("NoWeight",false);
	m_isxaod = m_settings->GetValue("Isxaod",true);
	m_ismc15 = m_settings->GetValue("IsMC15",false);
	m_bcid_excl = Utilities::VectorizeUint( m_settings->GetValue("bcid_excluded","0") );
	m_applyBCIDCut = m_settings->GetValue("ApplyBCIDCut",false);

	const xAOD::EventInfo* eventInfo = 0;
	//  if( !event.retrieve( eventInfo, "EventInfo").isSuccess() ) {
	if( event.retrieve( eventInfo, "EventInfo") == xAOD::TReturnCode::kFailure ) {
		std::cout << "Failed to get EventInfo!" << std::endl;
		abort();
	}
	else
		std::cout << " Successfully retrieved the event info." << std::endl;

	// check if input dataset is MC
	m_isMC = false;
	if ( eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) )
		m_isMC = true;
	std::cout << " isMC? " << m_isMC << std::endl;

	// if MC, determine which dataset and jet slice we're running over
	if (m_isMC) {
		m_mcChannelNumber = eventInfo->mcChannelNumber();
		m_JX = m_mcChannelNumber % 10;
		std:: cout << " mc_channel_number = " <<  m_mcChannelNumber << std::endl;
		std:: cout << " JX = " <<  m_JX << std::endl;
	}



	//m_EventShapeCopier = new EventShapeCopier("copier");
	//m_EventShapeCopier->initialize();



	//	InitialiseTools();

	isFirstEvent = true;
	nevent = 0;
	TString DR, Calib;

	//When there is just one type of jets...

	if(m_recoJetCollections.size()==1)
	{ double m_dR;
    //if(m_isxaod)InitialiseRho(m_recoJetCollections[0]);
		if(m_recoJetCollections[0].Contains("4") || m_truthJetCollections[0].Contains("4")){DR="4";m_dR=0.4;}
		else if(m_recoJetCollections[0].Contains("6") || m_truthJetCollections[0].Contains("6")){DR="6";m_dR=0.6;}
		else if(m_recoJetCollections[0].Contains("5") || m_truthJetCollections[0].Contains("5")){DR="5";m_dR=0.5;}
		else if(m_recoJetCollections[0].Contains("7") || m_truthJetCollections[0].Contains("7")){DR="7";m_dR=0.7;}
		else {DR="";m_dR=0.4;}
		Calib = Utilities::FormCalibName(m_recoJetCollections[0],m_truthJetCollections[0]);
		InitialiseHistograms(DR,Calib);
		InitialiseJetTools(v_reco_calibName[0]);
		jetCollectionMap[m_recoJetCollections[0]] = m_truthJetCollections[0];
		jetSizeMap[m_recoJetCollections[0]] = m_dR;
	}

	// NOTE: we first need to loop over the reco prefixes before doing the truth prefixes...otherwise the responses of the various jet collections end up in the wrong histos (thanks Fabrice!)
	else if(m_recoJetCollections.size() >= m_truthJetCollections.size() )
	{
		double m_dR;
		int index=0;
		for(unsigned int irjet = 0; irjet<m_recoJetCollections.size() ; irjet++)
		{
			//if(m_isxaod)InitialiseRho(m_recoJetCollections[irjet]);
			if(m_recoJetCollections[irjet].Contains("4"))
			{
				m_dR=0.4;
				for(unsigned int itjet = 0; itjet<m_truthJetCollections.size() ; itjet++)
				{
					if(!m_truthJetCollections[itjet].Contains("4"))continue;
					Calib = Utilities::FormCalibName(m_recoJetCollections[irjet],"");

					InitialiseHistograms("4", Calib);
					recindex[m_recoJetCollections[irjet]]=index;
					index++;
					InitialiseJetTools(v_reco_calibName[irjet]);
					jetCollectionMap[m_recoJetCollections[irjet]] = m_truthJetCollections[itjet];
					jetSizeMap[m_recoJetCollections[irjet]] = m_dR;
				}
			}
			else if(m_recoJetCollections[irjet].Contains("6"))
			{ m_dR=0.6;
				for(unsigned int itjet = 0; itjet<m_truthJetCollections.size() ; itjet++)
				{
					if(!m_truthJetCollections[itjet].Contains("6"))continue;
					Calib = Utilities::FormCalibName(m_recoJetCollections[irjet],"");
					InitialiseHistograms("6", Calib);
					recindex[m_recoJetCollections[irjet]]=index;
					index++;
					InitialiseJetTools(v_reco_calibName[irjet]);
					jetCollectionMap[m_recoJetCollections[irjet]] = m_truthJetCollections[itjet];
					jetSizeMap[m_recoJetCollections[irjet]] = m_dR;

				}
			}
			else if(m_recoJetCollections[irjet].Contains("5"))
			{ m_dR=0.5;
				for(unsigned int itjet = 0; itjet<m_truthJetCollections.size() ; itjet++)
				{
					if(!m_truthJetCollections[itjet].Contains("5"))continue;
					Calib = Utilities::FormCalibName(m_recoJetCollections[irjet],"");
					InitialiseHistograms("5", Calib);
					recindex[m_recoJetCollections[irjet]]=index;
					index++;
					InitialiseJetTools(v_reco_calibName[irjet]);
					jetCollectionMap[m_recoJetCollections[irjet]] = m_truthJetCollections[itjet];
					jetSizeMap[m_recoJetCollections[irjet]] = m_dR;
				}
			}
			else if(m_recoJetCollections[irjet].Contains("7"))
			{ m_dR=0.7;
				for(unsigned int itjet = 0; itjet<m_truthJetCollections.size() ; itjet++)
				{
					if(!m_truthJetCollections[itjet].Contains("7"))continue;
					Calib = Utilities::FormCalibName(m_recoJetCollections[irjet],"");
					InitialiseHistograms("7", Calib);
					recindex[m_recoJetCollections[irjet]]=index;
					index++;
					InitialiseJetTools(v_reco_calibName[irjet]);
					jetCollectionMap[m_recoJetCollections[irjet]] = m_truthJetCollections[itjet];
					jetSizeMap[m_recoJetCollections[irjet]] = m_dR;
				}
			}
			else   {
				std::cout << "No jet reco radius corresponding to truth choice found" << std::endl;
				exit(0);
			}

		}

	}
	else {
		std::cout << "No jet reco radius corresponding to truth choice found" << std::endl;
		exit(0);
	}

	TH1::SetDefaultSumw2(kTRUE);

	TString outputDir = m_settings->GetValue("OutputDir",".");
	if (outputDir!=""&&outputDir(outputDir.Length()-1)!='/') outputDir+="/";
	m_outputFileName = outputDir+ m_settings->GetValue("OutputName","test.root");
	hEventCounter = new TH1F("EventCounter","Number of Events",2,0,2);
	hNoTruthJetsEventCounter = new TH1F("NoTruthJetsEventCounter","Number of Events with no truth jets",2,0,2);
	m_outputfile = TFile::Open(m_outputFileName, "RECREATE");

	if( m_doPUreweighting ){
	  if( mc_lumiCalcFileNames.size() == 0){
		Error("runMC2()", "Pileup Reweighting is requested but no LumiCalc file is specified. Exiting" );
		exit(0);
	  }
	  if( mc_PRWFileNames.size() == 0){
		Error("runMC2()", "Pileup Reweighting is requested but no PRW file is specified. Exiting" );
		exit(0);
	  }
	}
  if ( m_doPUreweighting ) {
	std::cout<<"DOING PRW!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"<<std::endl;
    mc_pileuptool = new CP::PileupReweightingTool("Pileup2");

    std::vector<std::string> PRWFiles;
    std::vector<std::string> lumiCalcFiles;

    std::string tmp_lumiCalcFileNames = mc_lumiCalcFileNames;
    std::string tmp_PRWFileNames = mc_PRWFileNames;

    // Parse all comma seperated files
    while( tmp_PRWFileNames.size() > 0){
      int pos = tmp_PRWFileNames.find_first_of(',');
      if( pos == std::string::npos){
        pos = tmp_PRWFileNames.size();
        PRWFiles.push_back(tmp_PRWFileNames.substr(0, pos));
        tmp_PRWFileNames.erase(0, pos);
      }else{
        PRWFiles.push_back(tmp_PRWFileNames.substr(0, pos));
        tmp_PRWFileNames.erase(0, pos+1);
      }
    }
    while( tmp_lumiCalcFileNames.size() > 0){
      int pos = tmp_lumiCalcFileNames.find_first_of(',');
      if( pos == std::string::npos){
        pos = tmp_lumiCalcFileNames.size();
        lumiCalcFiles.push_back(tmp_lumiCalcFileNames.substr(0, pos));
        tmp_lumiCalcFileNames.erase(0, pos);
      }else{
        lumiCalcFiles.push_back(tmp_lumiCalcFileNames.substr(0, pos));
        tmp_lumiCalcFileNames.erase(0, pos+1);
      }
    }

    std::cout << "PileupReweighting Tool is adding Pileup files:" << std::endl;
    for( unsigned int i=0; i < PRWFiles.size(); ++i){
      std::cout << "    " << PRWFiles.at(i) << std::endl;
    }
    std::cout << "PileupReweighting Tool is adding Lumi Calc files:" << std::endl;
    for( unsigned int i=0; i < lumiCalcFiles.size(); ++i){
      std::cout << "    " << lumiCalcFiles.at(i) << std::endl;
    }
    std::cout << " dataScaleFactor=   " << 1./dataScaleFactor << std::endl;
	EL_RETURN_CHECK("treeMaker::initialize()", mc_pileuptool->setProperty("ConfigFiles", PRWFiles));
    EL_RETURN_CHECK("treeMaker::initialize()", mc_pileuptool->setProperty("LumiCalcFiles", lumiCalcFiles));
    EL_RETURN_CHECK("treeMaker::initialize()", mc_pileuptool->setProperty("DataScaleFactor", 1./dataScaleFactor));
    if (dataScaleFactorUP<0) {
		EL_RETURN_CHECK("treeMaker::initialize()", mc_pileuptool->setProperty("DataScaleFactorUP", 0.));
	} else {
		EL_RETURN_CHECK("treeMaker::initialize()", mc_pileuptool->setProperty("DataScaleFactorUP", 1./dataScaleFactorUP));
	}
    if (dataScaleFactorDOWN<0) {
		EL_RETURN_CHECK("treeMaker::initialize()", mc_pileuptool->setProperty("DataScaleFactorDOWN", 0.));
	} else {
		EL_RETURN_CHECK("treeMaker::initialize()", mc_pileuptool->setProperty("DataScaleFactorDOWN", 1./dataScaleFactorDOWN));
	}
	EL_RETURN_CHECK("treeMaker::initialize()", mc_pileuptool->initialize());
  }
	//	goodJets = new xAOD::JetContainer();
	//	goodJetsAux = new xAOD::JetAuxContainer();
	//	goodJets->setStore( goodJetsAux );


	//transDataStore = wk()->xaodStore();
	//for some derivations
	delete m_settings;
}

void JetPerformance::TruthToolCode::ProcessEvent(xAOD::TEvent& event, xAOD::TStore& transDataStore) {

	const xAOD::EventInfo* eventInfo = 0;
	if( !event.retrieve( eventInfo, "EventInfo").isSuccess() )
		std::cout << " Failed to retrieve event info." << std::endl;



	m_bcid = eventInfo->bcid();
	//m_mu = eventInfo->actualInteractionsPerCrossing();
	m_mu = eventInfo->averageInteractionsPerCrossing();
	//std::cout << " m_mu="<< m_mu<< std::endl;
	//std::cout << " m_mu="<< eventInfo->actualInteractionsPerCrossing()<< std::endl;
	  correctedAverageMu=m_mu;
	  PileupWeight=1;
	  if ( m_doPUreweighting ) {
		 mc_pileuptool->apply(*eventInfo,true);
		 static SG::AuxElement::ConstAccessor< float > pileupWeightAcc("PileupWeight");
		 if( eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION)) {
			PileupWeight = pileupWeightAcc(*eventInfo) ;
		 }
		  //Info("execute()", "PileupWeight = %f", PileupWeight );
		 if( !eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION)) {
			static SG::AuxElement::ConstAccessor< float > correctedMuAcc("corrected_averageInteractionsPerCrossing");
			correctedAverageMu=correctedMuAcc(*eventInfo) ;
		 }
		 //Info("execute()", "correctedAverageMu = %f", correctedAverageMu );
	  }
	if (!noWeight) {
		m_weight = eventInfo->mcEventWeight();
		if ( m_doPUreweighting ) {
			m_weight = m_weight*PileupWeight;
		}
	}
	if(m_applyBCIDCut){
		for(unsigned int ibc = 0; ibc <m_bcid_excl.size(); ibc++){
			if(m_bcid_excl[ibc] == m_bcid)return;
			}
		}

	histnbr = 0;
	noTruthJetHistnbr = 0;

	//m_EventShapeCopier->renameEventDensities();
	for(unsigned int irjet = 0; irjet<m_recoJetCollections.size() ; irjet++)
	{
		//std::cout <<irjet<<std::endl;
		//m_EventShapeCopier->renameEventDensities();
		FillHistograms( m_recoJetCollections.at(irjet), (unsigned int) recindex[m_recoJetCollections[irjet]], event);
		transDataStore.clear();
	}

	hEventCounter->Fill(0.,1.0);
	hEventCounter->Fill(1.,m_weight);
	nevent++;
	isFirstEvent = false;

}

void JetPerformance::TruthToolCode::WriteHistograms() {
	hEventCounter->Write();
	hNoTruthJetsEventCounter->Write();
	for (unsigned int iHistograms = 0; iHistograms < m_allTestHistograms.size(); iHistograms++) {
		m_allTestHistograms[iHistograms]->writeHistograms();
		//m_allTestHistograms2[iHistograms]->writeHistograms();
		m_nJetsVsPtHistograms[iHistograms]->writeHistograms();
		m_allPtHistograms[iHistograms]->writeHistograms();
	}
	for (unsigned int iHistograms = 0; iHistograms < m_allTestHistograms2.size(); iHistograms++) {

		m_allTestHistograms2[iHistograms]->writeHistograms();
	}
	for (unsigned int iHistograms = 0; iHistograms < m_allTestHistogramsMu.size(); iHistograms++) {

		m_allTestHistogramsMu[iHistograms]->writeHistograms();
	}
	for (unsigned int iHistograms = 0; iHistograms < m_allTestHistogramsMuCorrected.size(); iHistograms++) {

		m_allTestHistogramsMuCorrected[iHistograms]->writeHistograms();
	}
	for (unsigned int iHistograms = 0; iHistograms < m_allTestHistogramsNPV.size(); iHistograms++) {

		m_allTestHistogramsNPV[iHistograms]->writeHistograms();
	}
	/*for (unsigned int iHistograms = 0; iHistograms < m_allTestHistogramsNPVType.size(); iHistograms++) {

		m_allTestHistogramsNPVType[iHistograms]->writeHistograms();
	}*/
	/*
	for (unsigned int iHistograms = 0; iHistograms < m_allKevinHistogramsRatioVsTruth.size(); iHistograms++) {
		m_allKevinHistogramsRatioVsTruth[iHistograms]->writeHistograms();
		m_allKevinHistogramsRatioVsCalib[iHistograms]->writeHistograms();
	}
	for (unsigned int iHistograms = 0; iHistograms < m_allKevinHistogramsRatioVsTruth2.size(); iHistograms++) {
		m_allKevinHistogramsRatioVsTruth2[iHistograms]->writeHistograms();
		m_allKevinHistogramsRatioVsCalib2[iHistograms]->writeHistograms();
	}*/
	for (unsigned int iHistograms = 0; iHistograms < m_nNoTruthJetsVsPtHistograms.size(); iHistograms++) {
		m_nNoTruthJetsVsPtHistograms[iHistograms]->writeHistograms();
	}
}

// Finalise
void JetPerformance::TruthToolCode::Finalise() {
	m_outputfile->cd();
	//for (unsigned int ih = 0; ih< m_bcidVsRespForward.size(); ih++)m_bcidVsRespForward[ih]->Write();
	WriteHistograms();
	m_outputfile->Close();
	// delete tools
	//
	//	delete goodJets;
	//	delete goodJetsAux;

	for (unsigned int iTool = 0; iTool < v_JESTools.size(); iTool++)delete v_JESTools[iTool];
	//for (unsigned int iTool = 0; iTool < v_edtools.size(); iTool++)delete v_edtools[iTool];
	for (unsigned int iTool = 0; iTool < m_jetCleaningTools.size(); iTool++)delete m_jetCleaningTools[iTool];


	//if (m_EventShapeCopier) {
//		delete m_EventShapeCopier;
//		m_EventShapeCopier = 0;
//	}

}

// Initialise the tools
//  void JetPerformance::TruthToolCode::InitialiseTools() {
//	InitialiseGRL();
//	InitialiseJetTools();
//}


// Initialise the jet tools (cleaning, calibration)
void JetPerformance::TruthToolCode::InitialiseJetTools(TString jetAlgo) {
	// initialise jet calibration tool
	std::string jetAlgoStr = jetAlgo.Data();
	const std::string name = "CalibTool_" + jetAlgoStr;
	//  TString config = m_rootCoreBin+"/data/JetCalibTools/CalibrationConfigs/JES_Prerecommendation2015_Feb2015.config";
        //  TString config = "JES_Prerecommendation2015_Feb2015_Internal.config";
	TString config = m_jetToolConf;
        TString calibSeq;
	//if(jetAlgo.Contains("EM"))calibSeq = "JetArea_Residual";
	//if(jetAlgo.Contains("EM"))calibSeq = "EtaJES_GSC";
	if(jetAlgo.Contains("EM"))calibSeq = "JetArea_Residual_Origin_EtaJES_GSC";
	else calibSeq = "JetArea_Residual_EtaJES";
	std::cout<<"CalibSeq="<<calibSeq<<std::endl;
	bool isData = m_isMC ? false : true;
	JetCalibrationTool *m_jetCalibTool = new JetCalibrationTool(name, jetAlgo, config, calibSeq, isData);
	m_jetCalibTool->initializeTool(name);

	v_JESTools.push_back(m_jetCalibTool);
	// initialise jet cleaning tool
	JetCleaningTool *m_jetCleaningTool = new JetCleaningTool("JetCleaning");
	m_jetCleaningTool->setProperty( "CutLevel", "LooseBad");
	m_jetCleaningTool->initialize();
	m_jetCleaningTools.push_back(m_jetCleaningTool);
}
/*
void JetPerformance::TruthToolCode::InitialiseRho(TString jetcollection) {
	std::cout << " Initialising rho tools - needed by xAOD" << std::endl;
	EventDensityTool *edtool;
	if( jetcollection.Contains("EM") && jetcollection.Contains("4")){
		edtool = new EventDensityTool("Kt4EMDensityTool");
		edtool->setProperty("JetAlgorithm", "Kt");
		edtool->setProperty("JetRadius", 0.4);
		edtool->setProperty("AbsRapidityMin", 0.0);
		edtool->setProperty("AbsRapidityMax", 2.0);
		edtool->setProperty("AreaDefinition", "Voronoi");
		edtool->setProperty("VoronoiRfact", 0.9);
		edtool->setProperty("OutputContainer", "Kt4EMTopoEventShape");
		//edtool->msg().setLevel( MSG::DEBUG );

		PseudoJetGetter *emgetter = new PseudoJetGetter("emget");
		emgetter->setProperty("InputContainer", "CaloCalTopoCluster");
		emgetter->setProperty("OutputContainer", "PseudoJetEMTopo");
		emgetter->setProperty("Label", "EMTopo");
		emgetter->setProperty("SkipNegativeEnergy", true);
		emgetter->setProperty("GhostScale", 0.0);
		emgetter->initialize();
		asg::ToolStore::put(emgetter);
		ToolHandle<IPseudoJetGetter> hemget(emgetter);

		edtool->setProperty("JetInput", hemget);

		edtool->initialize();

	}
	else if(jetcollection.Contains("LC") && jetcollection.Contains("4")){

		edtool =  new EventDensityTool("Kt4LCDensityTool");
		edtool->setProperty("JetAlgorithm", "Kt");
		edtool->setProperty("JetRadius", 0.4);
		edtool->setProperty("AbsRapidityMin", 0.0);
		edtool->setProperty("AbsRapidityMax", 2.0);
		edtool->setProperty("AreaDefinition", "Voronoi");
		edtool->setProperty("VoronoiRfact", 0.9);
		edtool->setProperty("OutputContainer", "Kt4LCTopoEventShape");
		//edtool->msg().setLevel( MSG::DEBUG );

		PseudoJetGetter *lcgetter = new PseudoJetGetter("lcget");
		lcgetter->setProperty("InputContainer", "CaloCalTopoCluster");
		lcgetter->setProperty("OutputContainer", "PseudoJetLCTopo");
		lcgetter->setProperty("Label", "LCTopo");
		lcgetter->setProperty("SkipNegativeEnergy", true);
		lcgetter->setProperty("GhostScale", 0.0);
		lcgetter->initialize();
		asg::ToolStore::put(lcgetter);
		ToolHandle<IPseudoJetGetter> hlcget(lcgetter);

		edtool->setProperty("JetInput", hlcget);

		edtool->initialize();
	}
	else{
		Error("initialiseRho()", "no Rho implemeted for jet colection" + jetcollection);
		exit(10);
	}

	v_edtools.push_back(edtool);
}*/

// Initialise the histograms
void JetPerformance::TruthToolCode::InitialiseHistograms(TString DR, TString jetalgo) {

	std::string responseBins = Utilities::makeBinString(1000,0,10);
	std::string muBins = Utilities::makeBinString(100,0,100);
	std::string NPVBinsType = Utilities::makeBinString(10,0,10);
        //std::string pttruthBins = Utilities::makeBinString(10000,0,5000);
        std::string pttruthBins = Utilities::makeBinString(10000,0,5000);
	bool doAbsEta = true;
	// If we need the histos for the different flavors...
	TString jetFlavor[]={"All","Glu","Light","Other","B","C"};
	//TString jetFlavor[]={"All"};
	TString histoName = "",histoName2 = "",histoNameMu = "",histoNameMuCorrected = "",histoNameNPV = "",histoNameNPVType = "", histoPtName="", histoBCIDName="";
	TString nJetsHistoName = "";

	TH1::SetDefaultSumw2(kTRUE);

	nJetsHistoName = "nNoTruthJetsVsPt" + jetalgo;
	m_nNoTruthJetsVsPtHistograms.push_back(new JetPerfHistograms(nJetsHistoName.Data(), etaBins, ptBins, doAbsEta));
	// Only flavor inclusive histograms
	if(!m_doGluLight && !m_doCB) {
		m_nFlavorHistos = 1;
		histoName = "respVsPt" + jetalgo;
		histoName2 = "respDividedByPtTruthVsPt" + jetalgo;
		histoNameMu="averageMu"+jetalgo;
		histoNameMuCorrected="correctedAverageMu"+jetalgo;
		histoNameNPV="NPV"+jetalgo;
		histoNameNPVType="NPVType"+jetalgo;
                histoPtName = "PtTruth" + jetalgo;
                histoBCIDName = "BCIDVsResponse" + jetalgo;
		m_allTestHistograms.push_back(new JetPerf4DHistograms(histoName.Data(), etaBins, NPVBins, ptBins, responseBins, doAbsEta, "p_{T}^{reco}/p_{T}^{PUS}"));
		m_allTestHistograms2.push_back(new JetPerf4DHistograms(histoName2.Data(), etaBins, NPVBins, ptBins, responseBins, doAbsEta, "p_{T}^{reco}/p_{T}^{true}"));
		m_allTestHistogramsMu.push_back(new JetPerf4DHistograms(histoNameMu.Data(), etaBins, NPVBins, ptBins, muBins, doAbsEta, "<mu>"));
		m_allTestHistogramsMuCorrected.push_back(new JetPerf4DHistograms(histoNameMuCorrected.Data(), etaBins, NPVBins, ptBins, muBins, doAbsEta, "<mu>"));
		m_allTestHistogramsNPV.push_back(new JetPerf4DHistograms(histoNameNPV.Data(), etaBins, NPVBins, ptBins, muBins, doAbsEta, "NPV"));
		//m_allTestHistogramsNPVType.push_back(new JetPerf4DHistograms(histoNameNPVType.Data(), etaBins, NPVBins, ptBins, NPVBinsType, doAbsEta, "NPV"));
		m_allPtHistograms.push_back(new JetPerf4DHistograms(histoPtName.Data(), etaBins, NPVBins, ptBins, pttruthBins, doAbsEta, "p_{T}^{true}"));
		//m_bcidVsRespForward.push_back( new TH2F(histoBCIDName.Data(),"",5000,0.,5000.,3501.,-1.,3500. ));
		nJetsHistoName = "nJetsVsPt" + jetalgo;
		m_nJetsVsPtHistograms.push_back(new JetPerfHistograms(nJetsHistoName.Data(), etaBins, ptBins, doAbsEta));
	}
	// Additional light quark and gluon histograms
	else {
		if(!m_doCB) m_nFlavorHistos = 4;
		else m_nFlavorHistos = 6;
		for (int flavorI = 0; flavorI < m_nFlavorHistos; flavorI++) {
			histoName = "respVsPt" + jetFlavor[flavorI] + jetalgo;
			//histoName2 = "respDividedByPtTruthVsPt" + jetalgo;
                        histoPtName = "PtTruth" + jetFlavor[flavorI] + jetalgo;
                        histoBCIDName = "BCIDVsResponse" + jetFlavor[flavorI] + jetalgo;
		m_allTestHistograms.push_back(new JetPerf4DHistograms(histoName.Data(), etaBins, NPVBins, ptBins, responseBins, doAbsEta, "p_{T}^{reco}/p_{T}^{PUS}"));
		//m_allTestHistograms2.push_back(new JetPerf4DHistograms(histoName2.Data(), etaBins, NPVBins, ptBins, responseBins, doAbsEta, "p_{T}^{reco}//p_{T}^{true}"));
			m_allPtHistograms.push_back(new JetPerf4DHistograms(histoPtName.Data(), etaBins, NPVBins, ptBins, pttruthBins, doAbsEta, "p_{T}^{true}"));
			//m_bcidVsRespForward.push_back(  new TH2F(histoBCIDName.Data(),"",5000,0.,5000.,3501.,-1.,3500. ) );
			nJetsHistoName = "nJetsVsPt" + jetFlavor[flavorI] + jetalgo;
			m_nJetsVsPtHistograms.push_back(new JetPerfHistograms(nJetsHistoName.Data(), etaBins, ptBins, doAbsEta));


		}
		histoName2 = "respDividedByPtTruthVsPt" + jetalgo;
		histoNameMu="averageMu"+jetalgo;
		histoNameMuCorrected="correctedAverageMu"+jetalgo;
		histoNameNPV="NPV"+jetalgo;
		histoNameNPVType="NPVType"+jetalgo;
		m_allTestHistograms2.push_back(new JetPerf4DHistograms(histoName2.Data(), etaBins, NPVBins, ptBins, responseBins, doAbsEta, "p_{T}^{reco}//p_{T}^{true}"));
		m_allTestHistogramsMu.push_back(new JetPerf4DHistograms(histoNameMu.Data(), etaBins, NPVBins, ptBins, muBins, doAbsEta, "<mu>"));
		m_allTestHistogramsMuCorrected.push_back(new JetPerf4DHistograms(histoNameMuCorrected.Data(), etaBins, NPVBins, ptBins, muBins, doAbsEta, "<mu>"));
		m_allTestHistogramsNPV.push_back(new JetPerf4DHistograms(histoNameNPV.Data(), etaBins, NPVBins, ptBins, muBins, doAbsEta, "NPV"));
		//m_allTestHistogramsNPVType.push_back(new JetPerf4DHistograms(histoNameNPVType.Data(), etaBins, NPVBins, ptBins, NPVBins, doAbsEta, "NPV"));


	}
	/*
	std::string responseBins2 = Utilities::makeBinString(RationBinsNumber,RatioBinsLower,RatioBinsUpper);
	std::string pttruthBins2 = Utilities::makeBinString(PtTruthBinsNumber,PtTruthBinsLower,PtTruthBinsUpper);

	histoName = "ratioVsPtTruth" + jetalgo;
	m_allKevinHistogramsRatioVsTruth.push_back(new JetPerf4DHistogramsKevin(histoName.Data(), etaBins, NPVBins, ptBins,RationBinsNumber,RatioBinsLower,RatioBinsUpper, pttruthBins2, doAbsEta, "p_{T}^{true}"));

	histoName = "ratioVsPtCalib" + jetalgo;
	m_allKevinHistogramsRatioVsCalib.push_back(new JetPerf4DHistogramsKevin(histoName.Data(), etaBins, NPVBins, ptBins,RationBinsNumber,RatioBinsLower,RatioBinsUpper, pttruthBins2, doAbsEta, "p_{T}^{true}"));

	histoName = "ratioVsPtTruth2" + jetalgo;
	m_allKevinHistogramsRatioVsTruth2.push_back(new JetPerf4DHistogramsKevin(histoName.Data(), etaBins2, NPVBins, ptBins,RationBinsNumber,RatioBinsLower,RatioBinsUpper, pttruthBins2, doAbsEta, "p_{T}^{true}"));

	histoName = "ratioVsPtCalib2" + jetalgo;
	m_allKevinHistogramsRatioVsCalib2.push_back(new JetPerf4DHistogramsKevin(histoName.Data(), etaBins2, NPVBins, ptBins,RationBinsNumber,RatioBinsLower,RatioBinsUpper, pttruthBins2, doAbsEta, "p_{T}^{true}"));*
	*/
}

void JetPerformance::TruthToolCode::FillHistograms(TString recoPrefix, unsigned int jetIndex,xAOD::TEvent& event)

{
	double scale[19];
	scale[0]=1.30527;
	scale[1]=1.1524;
	scale[2]=1.08795;
	scale[3]=1.06267;
	scale[4]=1.05292;
	scale[5]=1.03949;
	scale[6]=1.03195;
	scale[7]=1.02317;
	scale[8]=1.02012;
	scale[9]=1.01748;
	scale[10]=1.0146;
	scale[11]=1.01236;
	scale[12]=1.01077;
	scale[13]=1.01005;
	scale[14]=1.00877;
	scale[15]=1.00844;
	scale[16]=1.0079;
	scale[17]=1.00596;
	scale[18]=1.00343;

	int scaleRange[20];
	scaleRange[0]=15;
	scaleRange[1]=20;
	scaleRange[2]=30;
	scaleRange[3]=45;
	scaleRange[4]=60;
	scaleRange[5]=80;
	scaleRange[6]=110;
	scaleRange[7]=160;
	scaleRange[8]=210;
	scaleRange[9]=260;
	scaleRange[10]=310;
	scaleRange[11]=400;
	scaleRange[12]=500;
	scaleRange[13]=600;
	scaleRange[14]=800;
	scaleRange[15]=1000;
	scaleRange[16]=1200;
	scaleRange[17]=1500;
	scaleRange[18]=1800;
	scaleRange[19]=2500;
	// Find the dR dorresponding to the current reco jet collection
	double dR = jetSizeMap[recoPrefix];

	const xAOD::VertexContainer* pVertices;
	const xAOD::Vertex* PV = 0;
	if( !event.retrieve(pVertices,"PrimaryVertices") )
		std::cout << " Failed to retrieve primary vertices." << std::endl;
	if( pVertices->size() > 0 )
		PV = (*pVertices)[0];
	else {
		// no primary vertices
		std::cout << "WARNING: no primary vertex found! Skipping event." << std::endl;
		return;
	}
	double NPV = pVertices->size();
	if ( int(PV->nTrackParticles())<requiredTracksPerVertex) return;

	// Get the jets
	const xAOD::JetContainer* jets = 0;
	if ( !event.retrieve( jets, recoPrefix.Data()).isSuccess() )
		std::cout << " Failed to retrieve the jets." << std::endl;

	//Perhaps a better way for retrieving JetContainers (much more time consuming)
	//	TLorentzVector jetTemp;
	//	std::pair<xAOD::JetContainer*, xAOD::ShallowAuxContainer*> jets_shallowCopy = xAOD::shallowCopyContainer( *jets );
	//	xAOD::JetContainer::iterator jetSC_itr = (jets_shallowCopy.first)->begin();
	//	xAOD::JetContainer::iterator jetSC_end = (jets_shallowCopy.first)->end();
	//
	//	for( ; jetSC_itr != jetSC_end; ++jetSC_itr ) {
	//		if( ! m_jetCleaningTool->accept(**jetSC_itr)) continue;
	//		if (m_jetCalibTool->applyCalibration(**jetSC_itr) == CP::CorrectionCode::Error) {
	//			Error("execute()", "JetCalibrationTool returns Error CorrectionCode");
	//		}
	//
	//		// create new attribute: detector level eta (convenience access)
	//		xAOD::JetFourMom_t jetconstitP4 = (*jetSC_itr)->getAttribute<xAOD::JetFourMom_t>("JetConstitScaleMomentum");
	//		jetTemp.SetPtEtaPhiE(jetconstitP4.pt(), jetconstitP4.eta(),jetconstitP4.phi(), jetconstitP4.e()  );
	//
	//		if(fabs(jetTemp.Rapidity()) > 4.4) continue;
	//		if(jetTemp.Pt() < 25000.) continue;
	//		goodJets->push_back( new xAOD::Jet(**jetSC_itr) );
	//	}
	//
	//	//std::cout<<"good Jets retrieved"<<std::endl;
	//	delete jets_shallowCopy.first;
	//	delete jets_shallowCopy.second;

	//Appparently it was fixed in MC15

	//if(m_isxaod && !m_ismc15)v_edtools[jetIndex]->fillEventShape();

	xAOD::JetContainer* goodJets = new xAOD::JetContainer();
	xAOD::JetAuxContainer* goodJetsAux = new xAOD::JetAuxContainer();
	goodJets->setStore( goodJetsAux );
	xAOD::JetContainer::const_iterator jet_itr = jets->begin();
	xAOD::JetContainer::const_iterator jet_end = jets->end();
	int counter2=0;
	for( ; jet_itr != jet_end; ++jet_itr ) {
		counter2++;
		//this needs to be validated, switch off atm
		//if( !m_jetCleaningTool->accept( **jet_itr )) continue; //only keep good clean jets
		xAOD::Jet* calib_jet = new xAOD::Jet();
		//if (counter2==1)std::cout<<"NEW EVENT"<<std::endl;
		//if (counter2==1)std::cout<<"before calibration="<<(*jet_itr)->pt()<<std::endl;
		v_JESTools[jetIndex]->calibratedCopy((**jet_itr),calib_jet);
		//if (counter2==1)std::cout<<"after calibration="<<calib_jet->pt()<<std::endl;

		//const scale if needed

		//if (counter2==1)std::cout<<calib_jet->pt()<<std::endl;
		//calib_jet->setJetP4( (*jet_itr ) -> getAttribute<xAOD::JetFourMom_t>("JetPileupScaleMomentum"));
		//if (counter2==1)std::cout<<calib_jet->pt()<<std::endl;

		if(!m_applyRecalibration)calib_jet->setJetP4( (*jet_itr ) -> getAttribute<xAOD::JetFourMom_t>("JetConstitScaleMomentum") );

		goodJets->push_back( calib_jet );
	}

	const xAOD::JetContainer *truthJets = 0;
	if ( !event.retrieve( truthJets, jetCollectionMap[recoPrefix].Data()).isSuccess() )
		std::cout << "Failed to retrieve the truth jets" << std::endl;
	if (truthJets->size()<1) {
		std::cout << "WARNING: no truth jets! Skipping event." << std::endl;
		hNoTruthJetsEventCounter->Fill(0.,1.0);
		hNoTruthJetsEventCounter->Fill(1.,m_weight);
		return;
	}

	int counter=0;
	// loop over the jets in the container
	for( xAOD::JetContainer::const_iterator recojet_itr = goodJets->begin(); recojet_itr != goodJets->end(); ++recojet_itr ) {
		counter++;
		float calibPt = (*recojet_itr)->pt();
		float calibEta = (*recojet_itr)->eta();
		float calibPhi = (*recojet_itr)->phi();

		float othercalibPt2, othercalibEta2, othercalibPhi2, othercalibE2;
		//xAOD::Jet* calib_jet2 = new xAOD::Jet();
		//if (counter==1)std::cout<<"reco="<<(*recojet_itr)->pt()<<std::endl;
		//v_JESTools[jetIndex]->calibratedCopy((**recojet_itr),calib_jet2);
		//if (counter==1)std::cout<<"reco="<<(*recojet_itr)->pt()<<std::endl;
		//if (counter==1)std::cout<<"othercalibPt2="<<calib_jet2->pt()<<std::endl;
		//calib_jet2->setJetP4( (*recojet_itr) -> getAttribute<xAOD::JetFourMom_t>("JetConstitScaleMomentum") );
		//if (counter==1)std::cout<<"othercalibPt2="<<calib_jet2->pt()<<std::endl;
		//calib_jet2->setJetP4( (*recojet_itr) -> getAttribute<xAOD::JetFourMom_t>("JetPileupScaleMomentum") );
		//if (counter==1)std::cout<<"othercalibPt2="<<calib_jet2->pt()<<std::endl;
		//calib_jet2->setJetP4( (*recojet_itr) -> getAttribute<xAOD::JetFourMom_t>("JetEtaJESScaleMomentum") );
		//if (counter==1)std::cout<<"othercalibPt2="<<calib_jet2->pt()<<std::endl;
		//calib_jet2->setJetP4( (*recojet_itr) -> getAttribute<xAOD::JetFourMom_t>("JetGSCScaleMomentum") );
		//if (counter==1)std::cout<<"othercalibPt2="<<calib_jet2->pt()<<std::endl;
		//calib_jet2->setJetP4( (*recojet_itr ) -> getAttribute<xAOD::JetFourMom_t>("JetConstitScaleMomentum") );
		//if (counter==1)std::cout<<"othercalibPt2="<<calib_jet2->pt()<<std::endl;
		//(*recojet_itr) -> getAttribute<xAOD::JetFourMom_t>("JetPileupScaleMomentum") ->pt();
		//if (counter==1)std::cout<<"JetEMScaleMomentum="<<(*recojet_itr) -> getAttribute<xAOD::JetFourMom_t>("JetEMScaleMomentum").Pt()<<std::endl;
		//if (counter==1)std::cout<<"JetConstitScaleMomentum="<<(*recojet_itr) -> getAttribute<xAOD::JetFourMom_t>("JetConstitScaleMomentum").Pt()<<std::endl;
		//if (counter==1)std::cout<<"JetPileupScaleMomentum="<<(*recojet_itr) -> getAttribute<xAOD::JetFourMom_t>("JetPileupScaleMomentum").Pt()<<std::endl;
		//if (counter==1)std::cout<<"JetOriginConstitScaleMomentum="<<(*recojet_itr) -> getAttribute<xAOD::JetFourMom_t>("JetOriginConstitScaleMomentum").Pt()<<std::endl;
		//if (counter==1)std::cout<<"JetEtaJESScaleMomentum="<<(*recojet_itr) -> getAttribute<xAOD::JetFourMom_t>("JetEtaJESScaleMomentum").Pt()<<std::endl;
		//if (counter==1)std::cout<<"JetGSCScaleMomentum="<<(*recojet_itr) -> getAttribute<xAOD::JetFourMom_t>("JetGSCScaleMomentum").Pt()<<std::endl;

		//if (counter==1)std::cout<<"othercalibPt2="<<(*recojet_itr) -> getAttribute<xAOD::JetFourMom_t>("JetGSCScaleMomentum").Pt()<<std::endl;
		//if (counter==1)std::cout<<"othercalibPt2="<<(*recojet_itr) -> getAttribute<xAOD::JetFourMom_t>("JetEtaJESScaleMomentum").Pt();<<std::endl;
		othercalibPt2 = (*recojet_itr) -> getAttribute<xAOD::JetFourMom_t>("JetPileupScaleMomentum").Pt();
		//othercalibPt2 = calib_jet2->pt();
		//othercalibEta2 = calib_jet2->eta();
		//othercalibPhi2 = calib_jet2->phi();
		//othercalibE2 = calib_jet2->e();
		//delete calib_jet2;

		/*if (calibPt!=othercalibPt2) {
			std::cout<<calibPt<< " "<<othercalibPt2 <<std::endl;
		}*/
		//if (counter==1)std::cout<<"reco="<<(*recojet_itr)->pt()<<std::endl;
		//if (counter==1)std::cout<<"calibPt="<<calibPt<<std::endl;
		//if (counter==1)std::cout<<"othercalibPt2="<<othercalibPt2<<std::endl;
		//Fill BCID histogram
		//m_bcidVsRespForward[histnbr] -> Fill(m_mu , m_bcid, m_weight);
		//m_bcidVsRespForward[histnbr] -> Fill(calibPt/1000. , m_bcid, m_weight);
		// Cuts on reco jet pT
		//if(m_mu>15) continue;
		//if(m_mu<25) continue;
		if (calibPt < 7000) continue;

		// reco isolation
		// Calculate distance to closest reco jet to check isolation
		bool isolated = true;

		float dRclosest = 9999.;
		//one has to apply a calibration to these jets even when doing CONST scale studies...

		for( xAOD::JetContainer::const_iterator recojet_otheritr = goodJets->begin(); recojet_otheritr != goodJets->end(); ++recojet_otheritr){
			if( recojet_itr!= recojet_otheritr) {
				float othercalibPt, othercalibEta, othercalibPhi, othercalibE;

				if(!m_applyRecalibration){
					xAOD::Jet* calib_jet = new xAOD::Jet();
					v_JESTools[jetIndex]->calibratedCopy((**recojet_otheritr),calib_jet);


				othercalibPt = calib_jet->pt();
				othercalibEta = calib_jet->eta();
				othercalibPhi = calib_jet->phi();
				othercalibE = calib_jet->e();
				//removed delete
				delete calib_jet;
				}
				else{
				othercalibPt = (*recojet_otheritr)->pt();
				othercalibEta = (*recojet_otheritr)->eta();
				othercalibPhi = (*recojet_otheritr)->phi();
				othercalibE = (*recojet_otheritr)->e();
				}

				double currentdR;
				if (m_isNTUPTOP || recoPrefix.Contains("EM")) currentdR = TruthToolCode::deltaR(calibEta,calibPhi,othercalibEta,othercalibPhi,othercalibPt,othercalibE,ptMinRecoIso, energyMinRecoIso);
				else currentdR = TruthToolCode::deltaR(calibEta,calibPhi,othercalibEta,othercalibPhi,othercalibPt,othercalibE,ptMinRecoIso, energyMinRecoIso);
				if (currentdR < dRclosest) dRclosest = currentdR;

			}
		}

		//Reco jet isolation

		if (m_applyIsolation) {
			//if(dRclosest < 2.5 * dR) isolated = false;
			if(dRclosest < 1.5 * m_dR) isolated = false;
		}

		// Only continue if jet is isolated
		if(!isolated) continue;
		// Reco-Truth Matching
		float dRRecoTruth = 9999.;
		double truthmatchedPt = 9999;
		double truthmatchedEta = 9999;
		double truthmatchedPhi = 9999;
		// closest distance between matched truth jet to any other truth jet above ptMinTrueIso
		float dRMatchedTruthclosest = 9999;
		if (truthJets->size() == 0) {
			m_nNoTruthJetsVsPtHistograms[noTruthJetHistnbr]->fillHistograms((*recojet_itr)->eta(),(*recojet_itr)->pt()/GeV,m_weight);
			//this is because we want to only check on inclusive jets
		}
		bool isotruth=true;
		int parentPDGid = 9999; // easier in mc15
		float dRtruthclosest = 9999.;
		for(xAOD::JetContainer::const_iterator truthjet_itr = truthJets->begin(); truthjet_itr < truthJets->end(); ++truthjet_itr)
		{
			float currentdR = TruthToolCode::deltaR(calibEta,calibPhi,(*truthjet_itr)->eta(),(*truthjet_itr)->phi());
			//truth isolation (not applied anywhere for the time being for consistency with JetPerformance code)
			//float dRtruthclosest = 9999.;
			for(xAOD::JetContainer::const_iterator truthjet_otheritr = truthJets->begin(); truthjet_otheritr < truthJets->end(); ++truthjet_otheritr){
				if( truthjet_otheritr!= truthjet_itr)
				{
					double currentTruthdR = TruthToolCode::deltaR((*truthjet_itr)->eta(),(*truthjet_itr)->phi(),(*truthjet_otheritr)->eta(),(*truthjet_otheritr)->phi(),(*truthjet_otheritr)->pt(),ptMinTrueIso);
					if(currentTruthdR < dRtruthclosest) dRtruthclosest = currentTruthdR;
				}
			}
			if(currentdR < dRRecoTruth)
			{
				dRRecoTruth = currentdR;
				dRMatchedTruthclosest = dRtruthclosest;
				truthmatchedPt = (*truthjet_itr)->pt();
				truthmatchedEta = (*truthjet_itr)->eta();
				truthmatchedPhi = (*truthjet_itr)->phi();
				//truth parton matching
				if(m_ismc15){
					//debuggin
					parentPDGid =(*truthjet_itr) -> getAttribute<int>("PartonTruthLabelID");
					if( parentPDGid < 0 )parentPDGid=9999;
					//std::cout<<"pdg ID of truth jet = "<<parentPDGid<<std::endl;
					}
			}

		}		//end loop on truth
		//Truth isolation

		if(m_applyIsolation_true) {
			if(dRtruthclosest < 2.5 * m_dR) isotruth = false;
		}
		if(!isotruth) continue;
		// Continue if we could not match any truth jet
		if(dRRecoTruth>m_truthrecmatch) continue;


		// TRUTH jet to parton matching!!!
		//float maxDR = dRMatchedTruthclosest<2*m_dR ? dRMatchedTruthclosest/2. : m_dR;
		// Default matching radius;

		float maxDR = dR;

		// Match to the highest energy parton within maxDR
		float partonE = 0;

		int badmc = 0, goodmc=0;
		if(m_doGluLight || m_doCB){
			//different in MC15 vs MC14
			if(!m_ismc15){
				const xAOD::TruthParticleContainer* mcparticles = 0;
				if ( !event.retrieve( mcparticles, "TruthParticle"))
					std::cout << " Failed to retrieve truth particles" << std::endl;

				xAOD::TruthParticleContainer::const_iterator particle_itr = mcparticles->begin();
				xAOD::TruthParticleContainer::const_iterator particle_end = mcparticles->end();
				//int badmc = 0, goodmc=0;
				for( ;  particle_itr!= particle_end; ++particle_itr) {
					// Look for the interesting partons...
					if(fabs((*particle_itr)->pdgId()) > 5 && fabs((*particle_itr)->pdgId()) != 21 && fabs((*particle_itr)->pdgId())!=9) continue;
					if(fabs((*particle_itr)->pdgId()) == 0) continue;

					//dummy soft particles

					if((*particle_itr)->pt() < 0.0001){
						//std::cout<<"pdgID = "<<(*particle_itr)->pdgId() <<std::endl;
						//std::cout<<"status = "<<(*particle_itr)->status()<<std::endl;
						badmc++;
						continue;
					}
					goodmc++;
					// Match RECO jet to parton
					//      float dRJetParton = TruthToolCode::deltaR(calibEta,calibPhi,(*particle_itr)->eta(),(*particle_itr)->phi());

					// Match TRUTH jet to parton
					//std::cout<<"before calculateing dR" <<std::endl;
					float dRJetParton = TruthToolCode::deltaR(truthmatchedEta,truthmatchedPhi,(*particle_itr)->eta(),(*particle_itr)->phi());

					//std::cout<<"after calculateing dR = " <<dRJetParton <<std::endl;
					//std::cout<<"maxdR = " <<maxDR <<std::endl;

					if(dRJetParton>maxDR) continue;

					if(partonE < (*particle_itr)->e()){
						parentPDGid = (*particle_itr)->pdgId();
						partonE = (*particle_itr)->e();
					}
				}
			}
		}
		//std::cout<<"n bad mc =  "<<badmc<<std::endl;
		//std::cout<<"n good mc =  "<<goodmc<<std::endl;
		//std::cout<<"parent PDG ID =  "<<parentPDGid<<std::endl;

		//kevin added these next few lines

		//othercalibPt2=1.;



		//std::cout<<"calibPt="<<calibPt<<std::endl;
		//std::cout<<"othercalibPt2="<<othercalibPt2<<std::endl;
		//std::cout<<"truthmatchedPt/1000="<<truthmatchedPt/1000<<std::endl;
		//std::cout<<"calibPT="<<calibPt/1000<<std::endl;
		/*
		for (int x=1;x<=19;x++) {
			if (calibPt/1000<scaleRange[x]) {
				calibPt=calibPt/scale[x-1];
				//std::cout<<"scale="<<scale[x-1]<<std::endl;
				break;
			}
		}*/
		//std::cout<<"calibPT="<<calibPt/1000<<std::endl;
		float respo = calibPt/othercalibPt2;
		//float respo = calibPt/truthmatchedPt;
		float respo2 = calibPt/truthmatchedPt;
		/*
		std::cout<<"   "<<respo2<<std::endl;
		std::cout<<"respo2="<<respo2<<std::endl;
		std::cout<<"calibPt="<<calibPt<<std::endl;
		std::cout<<"othercalibPt2="<<othercalibPt2<<std::endl;
		std::cout<<"truthmatchedPt/1000="<<truthmatchedPt/1000<<std::endl;*/
		/*if (othercalibPt2<100) {
			std::cout<<"calibPt="<<calibPt<<std::endl;
			std::cout<<"othercalibPt2="<<othercalibPt2<<std::endl;
		}*/
		//Fill flavor inclusive response histo
		m_allTestHistograms[histnbr]->fillHistograms(calibEta, NPV, truthmatchedPt/1000, respo, m_weight);
		m_allTestHistograms2[histnbr]->fillHistograms(calibEta, NPV, truthmatchedPt/1000, respo2, m_weight);
		m_allTestHistogramsMu[histnbr]->fillHistograms(calibEta, NPV, truthmatchedPt/1000, m_mu, m_weight);
		m_allTestHistogramsMuCorrected[histnbr]->fillHistograms(calibEta, NPV, truthmatchedPt/1000, correctedAverageMu, m_weight);
		m_allTestHistogramsNPV[histnbr]->fillHistograms(calibEta, NPV, truthmatchedPt/1000, NPV, m_weight);
		/*const xAOD::VertexContainer* vertices;
		if( !event.retrieve(vertices,"PrimaryVertices") )
			std::cout << " Failed to retrieve primary vertices." << std::endl;
		xAOD::VertexContainer::const_iterator vertices_itr = vertices->begin();
	    xAOD::VertexContainer::const_iterator vertices_end = vertices->end();

	    for( ; vertices_itr != vertices_end; ++vertices_itr ) {
			//std::cout <<(*vertices_itr)->vertexType()<< std::endl;
		  m_allTestHistogramsNPVType[histnbr]->fillHistograms(calibEta, NPV, truthmatchedPt/1000, (*vertices_itr)->vertexType(), m_weight);
	    }*/

		/*m_allKevinHistogramsRatioVsTruth[histnbr]->fillHistograms(calibEta, NPV, truthmatchedPt/1000,truthmatchedPt/1000, respo2, m_weight);
		m_allKevinHistogramsRatioVsCalib[histnbr]->fillHistograms(calibEta, NPV, truthmatchedPt/1000,calibPt/1000, respo2, m_weight);
		m_allKevinHistogramsRatioVsTruth2[histnbr]->fillHistograms(calibEta, NPV, truthmatchedPt/1000,truthmatchedPt/1000, respo2, m_weight);
		m_allKevinHistogramsRatioVsCalib2[histnbr]->fillHistograms(calibEta, NPV, truthmatchedPt/1000,calibPt/1000, respo2, m_weight);*/
		m_nJetsVsPtHistograms[histnbr]->fillHistograms(calibEta, truthmatchedPt/1000, m_weight);
		m_allPtHistograms[histnbr]->fillHistograms(calibEta, NPV, truthmatchedPt/1000, truthmatchedPt/1000, m_weight);
		if(m_doGluLight) {
			// light quark histos
			if(fabs(parentPDGid) != 0 && fabs(parentPDGid)<=3 ){
				m_allTestHistograms[histnbr+2]->fillHistograms(calibEta, NPV, truthmatchedPt/1000, respo, m_weight);
				m_nJetsVsPtHistograms[histnbr+2]->fillHistograms(calibEta, truthmatchedPt/1000, m_weight);
			        m_allPtHistograms[histnbr+2]->fillHistograms(calibEta, NPV, truthmatchedPt/1000, truthmatchedPt/1000, m_weight);
			}
			// Gluon histos
			else if(fabs(parentPDGid)==21 || fabs(parentPDGid)==9){
				m_allTestHistograms[histnbr+1]->fillHistograms(calibEta, NPV, truthmatchedPt/1000, respo, m_weight);
				m_nJetsVsPtHistograms[histnbr+1]->fillHistograms(calibEta, truthmatchedPt/1000, m_weight);
                                m_allPtHistograms[histnbr+1]->fillHistograms(calibEta, NPV, truthmatchedPt/1000, truthmatchedPt/1000, m_weight);
			}
			// Unmatched jets
			else if (fabs(parentPDGid) > 5 && fabs(parentPDGid) != 21 && fabs(parentPDGid) != 9){
				m_allTestHistograms[histnbr+3]->fillHistograms(calibEta, NPV, truthmatchedPt/1000, respo, m_weight);
				m_nJetsVsPtHistograms[histnbr+3]->fillHistograms(calibEta, truthmatchedPt/1000, m_weight);
                                m_allPtHistograms[histnbr+3]->fillHistograms(calibEta, NPV, truthmatchedPt/1000, truthmatchedPt/1000, m_weight);
			}
		}
		if(m_doCB) {
			if(fabs(parentPDGid)==4){
				m_allTestHistograms[histnbr+5]->fillHistograms(calibEta, NPV, truthmatchedPt/1000, respo, m_weight);
				m_nJetsVsPtHistograms[histnbr+5]->fillHistograms(calibEta, truthmatchedPt/1000, m_weight);
                                m_allPtHistograms[histnbr+5]->fillHistograms(calibEta, NPV, truthmatchedPt/1000, truthmatchedPt/1000, m_weight);
			}
			else if(fabs(parentPDGid)==5) {
				m_allTestHistograms[histnbr+4]->fillHistograms(calibEta, NPV, truthmatchedPt/1000, respo, m_weight);
				m_nJetsVsPtHistograms[histnbr+4]->fillHistograms(calibEta, truthmatchedPt/1000, m_weight);
                                m_allPtHistograms[histnbr+4]->fillHistograms(calibEta, NPV, truthmatchedPt/1000, truthmatchedPt/1000, m_weight);
			}
		}
	}
	histnbr= histnbr + m_nFlavorHistos; //index of the histogram
	noTruthJetHistnbr++;
	delete goodJets;
	delete goodJetsAux;
	//transDataStore->clear();


}

float JetPerformance::TruthToolCode::deltaR(float eta1, float phi1, float eta2, float phi2){
	return sqrt(pow(eta1-eta2,2)+pow(JetPerformance::TruthToolCode::deltaPhi(phi1,phi2),2));
}

float JetPerformance::TruthToolCode::deltaPhi(float phi1, float phi2){
	float result = fabs(phi1-phi2);
	result = result > 3.141592 ? fabs(2*3.141592-result) : result;
	return result;
}

float JetPerformance::TruthToolCode::deltaR(float eta1, float phi1, float eta2, float phi2, float ptenergy2, float minptenergy)
{
	if(ptenergy2 < minptenergy) return 9999.;

	return deltaR(eta1, phi1, eta2, phi2);
}

float JetPerformance::TruthToolCode::deltaR(float eta1, float phi1, float eta2, float phi2, float pt2, float e2, float minpt, float mine)
{
	if(e2 < mine) return 9999.;

	return deltaR(eta1, phi1, eta2, phi2, pt2, minpt);
}

