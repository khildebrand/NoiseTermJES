#include "TruthToolCode/JetPerf3DHistograms.h"
#include "util/Utilities.h"
#include <cstdlib>
#include <TMath.h>

// Constructor
JetPerf3DHistograms::JetPerf3DHistograms(const std::string & name, const std::string & etaBinsString, const std::string & ptBinsString, const std::string & binsString, const bool doAbsEta, const std::string & xAxisTitle, const std::string & yAxisTitle)
{
  
  m_doAbsEta = doAbsEta;
  m_name = name;
  m_etaBins = Utilities::tokenizeBins(etaBinsString);
  m_ptBins = Utilities::tokenizeBins(ptBinsString);  
  m_3dBins = Utilities::tokenizeBins(binsString);  
  m_xAxisTitle = xAxisTitle;
  m_yAxisTitle = yAxisTitle;  


  Utilities::checkBinVector(m_etaBins);
  Utilities::checkBinVector(m_ptBins);
  Utilities::checkBinVector(m_3dBins);
 

  
  //adding eta histograms - starting from the _high_, so we don't go out of range
  for (unsigned int iEtaHigh = 1; iEtaHigh < m_etaBins.size(); iEtaHigh++) {

    unsigned int iEtaLow = iEtaHigh-1;
    double etaLow = m_etaBins[iEtaLow];
    double etaHigh = m_etaBins[iEtaHigh];
    
    //histoMap for the different pt bins
    histoMap_t * histoMap = new histoMap_t();
    
      //adding pt histograms - one for each eta bin
      for (unsigned int iPtHigh = 1; iPtHigh < m_ptBins.size(); iPtHigh++) {
        unsigned int iPtLow = iPtHigh-1;
        double ptLow = m_ptBins[iPtLow];
        double ptHigh = m_ptBins[iPtHigh];
	addHistogram(etaLow, etaHigh, ptLow, ptHigh, m_3dBins, histoMap);
      }

  this->operator[](std::make_pair(etaLow, etaHigh)) = histoMap;

  }
  
}

TH1D* JetPerf3DHistograms::bookHistogram(const double & lowEtaBin, const double & highEtaBin, const double & lowPtBin, const double & highPtBin, const std::vector<double> & Bins) {
  
  
  std::string lowEtaString = Utilities::doubleToString(lowEtaBin);
  std::string highEtaString = Utilities::doubleToString(highEtaBin);
  
  std::string lowPtString = Utilities::doubleToString(lowPtBin);
  std::string highPtString = Utilities::doubleToString(highPtBin); 
  
  std::string histoName= "";
  std::string histoTitle = "";

  if (m_doAbsEta) {
    histoTitle = m_name+", "+lowEtaString+"<|#eta|<"+highEtaString+", "+lowPtString+"<p_{T}^{jet}<"+highPtString;
    histoName = m_name+"_absEta_"+lowEtaString+"_"+highEtaString+"_pt_"+lowPtString+"_"+highPtString;
  }
  else {
    histoTitle = m_name+", "+lowEtaString+"<#eta<"+highEtaString+", "+lowPtString+"<p_{T}^{jet}<"+highPtString;
    histoName = m_name+ "_eta_"+lowEtaString+"_"+highEtaString+"_pt_"+lowPtString+"_"+highPtString;
  }


  std::string histoTitleAxis = histoTitle+";"+m_xAxisTitle+";"+m_yAxisTitle;

  //Array to vector: take advantage of the fact that standard C++ forces memory allocation of stl::vectors to be contiguous 
  const double * BinsArray = &Bins[0];

  //Book histogram
  TH1D * histo = new TH1D(histoName.c_str(), histoTitleAxis.c_str(), Bins.size()-1, BinsArray);
  //histo->Sumw2();
  
  //some info for the user
  std::cout << "INFO: in JetPerf3DHistograms::bookHistogram() - booked new histogram" << std::endl;
  std::string binsInfo = "in JetPerf3DHistograms::bookHistogram() - bins: ";
  for (unsigned int i=0; i<Bins.size(); i++) {
    binsInfo = binsInfo + Utilities::doubleToString(BinsArray[i]) + " "; 
  }
  std::string etaPtInfo = "JetPerfHistograms::bookHistogram() - "+histoTitle;
  std::cout << etaPtInfo << std::endl;
  std::cout << binsInfo << std::endl;

  return histo;
  
}

void JetPerf3DHistograms::addHistogram(const double & lowEta, const double & highEta, const double & lowPt, const double & highPt, const std::vector<double> & bins, histoMap_t *  histoMap) {
  
  histoMap->operator[](std::make_pair(lowPt, highPt)) = bookHistogram(lowEta, highEta, lowPt, highPt ,bins); 
  
}


void JetPerf3DHistograms::fillHistograms(const double & jet_eta, const double & jet_pt, const double & jet_value, const double & jet_weight) {

  //find the eta histoMap where this jet belongs
  JetPerf3DHistograms::histoMap3d_t::iterator histoListIter = JetPerf3DHistograms::findEtaHisto(jet_eta);
 
  //we're implicitly doing an eta acceptance cut here
  if (histoListIter != this->end() && histoListIter->second != 0) {
    //find the pt histogram where this jet belongs
    histoMap_t::iterator histoIter = JetPerf3DHistograms::findPtHisto(jet_pt, histoListIter);
    //we're implicitly doing a pt acceptance cut here	
    if (histoIter != (histoListIter->second)->end() && histoIter->second != 0)   {  
      //fill the histo
      histoIter->second->Fill(jet_value, jet_weight);
    }
  }
}

void JetPerf3DHistograms::clearHistograms() {
  
  const histoMap3d_t::iterator first = this->begin();
  const histoMap3d_t::iterator last = this->end();
    
  for (histoMap3d_t::iterator histoListIter = first; histoListIter != last; histoListIter++){
    if (histoListIter->second != 0) {
	
      const histoMap_t::iterator firstHisto = (histoListIter->second)->begin();
      const histoMap_t::iterator lastHisto = (histoListIter->second)->end();
	
      for (histoMap_t::iterator histoIter = firstHisto; histoIter != lastHisto; histoIter++){
        if (histoIter->second != 0) histoIter->second->Clear();
      }
    }
  }
}

void JetPerf3DHistograms::deleteHistograms() {
  
  const histoMap3d_t::iterator first = this->begin();
  const histoMap3d_t::iterator last = this->end();
    
  for (histoMap3d_t::iterator histoListIter = first; histoListIter != last; histoListIter++){
    if (histoListIter->second != 0) {
	
      const histoMap_t::iterator firstHisto = (histoListIter->second)->begin();
      const histoMap_t::iterator lastHisto = (histoListIter->second)->end();
	
      for (histoMap_t::iterator histoIter = firstHisto; histoIter != lastHisto; histoIter++){
        if (histoIter->second != 0) delete histoIter->second;
      }
    }
  }
}

void JetPerf3DHistograms::writeHistograms() {
  
  const histoMap3d_t::iterator first = this->begin();
  const histoMap3d_t::iterator last = this->end();
    
  for (histoMap3d_t::iterator histoListIter = first; histoListIter != last; histoListIter++){
    if (histoListIter->second != 0) {
	
      const histoMap_t::iterator firstHisto = (histoListIter->second)->begin();
      const histoMap_t::iterator lastHisto = (histoListIter->second)->end();
	
      for (histoMap_t::iterator histoIter = firstHisto; histoIter != lastHisto; histoIter++){
        if (histoIter->second != 0)
	  histoIter->second->Write();
      }
    }
  }
}


const JetPerf3DHistograms::histoMap3d_t::iterator JetPerf3DHistograms::findEtaHisto(double eta) {

  //loop over map and check whether eta is within the range 
  const histoMap3d_t::iterator first = this->begin();
  const histoMap3d_t::iterator last = this->end();

  for (histoMap3d_t::iterator histoListIter = first; histoListIter != last; histoListIter++){
    
    //reminder:  histoIter->first is std::pair<low eta edge, high eta edge>
    double lowEtaEdge = histoListIter->first.first;
    double highEtaEdge = histoListIter->first.second;

    if (eta > lowEtaEdge && eta <= highEtaEdge) {
      return histoListIter;
    }

  }
  return last; //need to check validity of this - if eta of jet is outside coverage. 
}

const histoMap_t::iterator JetPerf3DHistograms::findPtHisto(double pt, const JetPerf3DHistograms::histoMap3d_t::iterator etaHistoIter) {
  
  //loop over map and check whether pt is within the range 
  const histoMap_t::iterator first = (etaHistoIter->second)->begin();
  const histoMap_t::iterator last = (etaHistoIter->second)->end();
  

  //loop over map and check whether pt is within the range 
  for (histoMap_t::iterator histoIter = first; histoIter != last; histoIter++){

    double lowPtEdge = histoIter->first.first;
    double highPtEdge = histoIter->first.second;
    

    if (pt > lowPtEdge && pt <= highPtEdge) {
      return histoIter;
    }

  }
  return last; //need to check validity of this - if pt of jet is outside coverage. 
}

