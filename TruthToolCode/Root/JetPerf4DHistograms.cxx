#include "TruthToolCode/JetPerf4DHistograms.h"
#include "util/Utilities.h"
#include <cstdlib>
#include <TMath.h>

// Constructor
JetPerf4DHistograms::JetPerf4DHistograms(const std::string & name, const std::string & etaBinsString, const std::string & NPVBinsString, const std::string & ptBinsString, const std::string & binsString, const bool doAbsEta, const std::string & xAxisTitle, const std::string & yAxisTitle)
{
  
  m_doAbsEta = doAbsEta;
  m_name = name;
  m_etaBins = Utilities::tokenizeBins(etaBinsString);
  m_NPVBins = Utilities::tokenizeBins(NPVBinsString);
  m_ptBins = Utilities::tokenizeBins(ptBinsString);
  m_3dBins = Utilities::tokenizeBins(binsString);  
  m_xAxisTitle = xAxisTitle;
  m_yAxisTitle = yAxisTitle;  


  Utilities::checkBinVector(m_etaBins);
  Utilities::checkBinVector(m_NPVBins);
  Utilities::checkBinVector(m_ptBins);
  Utilities::checkBinVector(m_3dBins);
 

  
  //adding eta histograms - starting from the _high_, so we don't go out of range
  for (unsigned int iEtaHigh = 1; iEtaHigh < m_etaBins.size(); iEtaHigh++) {

    unsigned int iEtaLow = iEtaHigh-1;
    double etaLow = m_etaBins[iEtaLow];
    double etaHigh = m_etaBins[iEtaHigh];
    
    //histoMap3d for the different NPV/pt bins
    histoMap3d_t * histoMap3d = new histoMap3d_t();

    for (unsigned int iNPVHigh = 1; iNPVHigh < m_NPVBins.size(); iNPVHigh++) {
      unsigned int iNPVLow = iNPVHigh-1;
      double NPVLow = m_NPVBins[iNPVLow];
      double NPVHigh = m_NPVBins[iNPVHigh];
      
      //histoMap for the different pt bins
      histoMap_t * histoMap = new histoMap_t();
      
      //adding pt histograms - one for each eta bin
      for (unsigned int iPtHigh = 1; iPtHigh < m_ptBins.size(); iPtHigh++) {
        unsigned int iPtLow = iPtHigh-1;
        double ptLow = m_ptBins[iPtLow];
        double ptHigh = m_ptBins[iPtHigh];

	addHistogram(etaLow, etaHigh, NPVLow, NPVHigh, ptLow, ptHigh, m_3dBins, histoMap);
      }

      histoMap3d->operator[](std::make_pair(NPVLow, NPVHigh)) = histoMap;
    }

  this->operator[](std::make_pair(etaLow, etaHigh)) = histoMap3d;

  }
  
}

TH1D* JetPerf4DHistograms::bookHistogram(const double & lowEtaBin, const double & highEtaBin, const double & lowNPVBin, const double & highNPVBin, const double & lowPtBin, const double & highPtBin, const std::vector<double> & Bins) {
  
  
  std::string lowEtaString = Utilities::doubleToString(lowEtaBin);
  std::string highEtaString = Utilities::doubleToString(highEtaBin);

  std::string lowNPVString = Utilities::doubleToString(lowNPVBin);
  std::string highNPVString = Utilities::doubleToString(highNPVBin);
  
  std::string lowPtString = Utilities::doubleToString(lowPtBin);
  std::string highPtString = Utilities::doubleToString(highPtBin); 
  
  std::string histoName= "";
  std::string histoTitle = "";

  if (m_doAbsEta) {
    histoTitle = m_name+", "+lowEtaString+"<|#eta|<"+highEtaString+","+lowNPVString+"<=N_{PV}<"+highNPVString+", "+lowPtString+"<p_{T}^{jet}<"+highPtString;
    histoName = m_name+"_absEta_"+lowEtaString+"_"+highEtaString+"_NPV_"+lowNPVString+"_"+highNPVString+"_pt_"+lowPtString+"_"+highPtString;
  }
  else {
    histoTitle = m_name+", "+lowEtaString+"<#eta<"+highEtaString+","+lowNPVString+"<=N_{PV}<"+highNPVString+", "+lowPtString+"<p_{T}^{jet}<"+highPtString;
    histoName = m_name+ "_eta_"+lowEtaString+"_"+highEtaString+"_NPV_"+lowNPVString+"_"+highNPVString+"_pt_"+lowPtString+"_"+highPtString;
  }


  std::string histoTitleAxis = histoTitle+";"+m_xAxisTitle+";"+m_yAxisTitle;

  //Array to vector: take advantage of the fact that standard C++ forces memory allocation of stl::vectors to be contiguous 
  const double * BinsArray = &Bins[0];

  //Book histogram
  TH1D * histo = new TH1D(histoName.c_str(), histoTitleAxis.c_str(), Bins.size()-1, BinsArray);
  //histo->Sumw2();
  
  //some info for the user - commented out because it also gives epilepsy to the user
  /*std::cout << "INFO: in JetPerf4DHistograms::bookHistogram() - booked new histogram" << std::endl;
  std::string binsInfo = "in JetPerf4DHistograms::bookHistogram() - bins: ";
  for (unsigned int i=0; i<Bins.size(); i++) {
    binsInfo = binsInfo + Utilities::doubleToString(BinsArray[i]) + " "; 
  }
  std::string etaPtInfo = "JetPerfHistograms::bookHistogram() - "+histoTitle;
  std::cout << etaPtInfo << std::endl;
  std::cout << binsInfo << std::endl;
  */

  return histo;
  
}

void JetPerf4DHistograms::addHistogram(const double & lowEta, const double & highEta, const double & lowNPV, const double & highNPV, const double & lowPt, const double & highPt, const std::vector<double> & bins, histoMap_t *  histoMap) {
  
  histoMap->operator[](std::make_pair(lowPt, highPt)) = bookHistogram(lowEta, highEta, lowNPV, highNPV, lowPt, highPt, bins); 
  
}


void JetPerf4DHistograms::fillHistograms(const double & jet_eta, const double & jet_NPV, const double & jet_pt, const double & jet_value, const double & jet_weight) {

  //find the eta histoMap where this jet belongs
  JetPerf4DHistograms::histoMap4d_t::iterator histoListIter = JetPerf4DHistograms::findEtaHisto(jet_eta);
 
  //we're implicitly doing an eta acceptance cut here
  if (histoListIter != this->end() && histoListIter->second != 0) {
    //find the NPV histogram where this jet belongs
    histoMap3d_t::iterator histoNPVIter = JetPerf4DHistograms::findNPVHisto(jet_NPV, histoListIter);
    //we're implicitly doing an NPV acceptance cut here
    if(histoNPVIter != (histoListIter->second)->end() && histoNPVIter->second != 0) {
      //find the pt histogram where the jet belongs
      histoMap_t::iterator histoPtIter = JetPerf4DHistograms::findPtHisto(jet_pt, histoNPVIter);
      //we're implicitly doing a pt acceptance cut here
      if(histoPtIter != (histoNPVIter->second)->end() && histoPtIter->second != 0) {
	//fill the histo
	histoPtIter->second->Fill(jet_value, jet_weight);
      }
    }
  }
}

void JetPerf4DHistograms::clearHistograms() {
  
  const histoMap4d_t::iterator first4d = this->begin();
  const histoMap4d_t::iterator last4d = this->end();
  
  for (histoMap4d_t::iterator histoListIter = first4d; histoListIter != last4d; histoListIter++){
    if (histoListIter->second != 0) {
      
      const histoMap3d_t::iterator first3d = (histoListIter->second)->begin();
      const histoMap3d_t::iterator last3d = (histoListIter->second)->end();

      for (histoMap3d_t::iterator histoNPVIter = first3d; histoNPVIter != last3d; histoNPVIter++){
	if (histoNPVIter->second != 0) {

	  const histoMap_t::iterator firstHisto = (histoNPVIter->second)->begin();
	  const histoMap_t::iterator lastHisto = (histoNPVIter->second)->end();

	  for (histoMap_t::iterator histoIter = firstHisto; histoIter != lastHisto; histoIter++){
	    if (histoIter->second != 0) histoIter->second->Clear();
	  }
	}
      }
    }
  }
}


void JetPerf4DHistograms::deleteHistograms() {
  
  const histoMap4d_t::iterator first4d = this->begin();
  const histoMap4d_t::iterator last4d = this->end();
  
  for (histoMap4d_t::iterator histoListIter = first4d; histoListIter != last4d; histoListIter++){
    if (histoListIter->second != 0) {
      
      const histoMap3d_t::iterator first3d = (histoListIter->second)->begin();
      const histoMap3d_t::iterator last3d = (histoListIter->second)->end();

      for (histoMap3d_t::iterator histoNPVIter = first3d; histoNPVIter != last3d; histoNPVIter++){
	if (histoNPVIter->second != 0) {

	  const histoMap_t::iterator firstHisto = (histoNPVIter->second)->begin();
	  const histoMap_t::iterator lastHisto = (histoNPVIter->second)->end();

	  for (histoMap_t::iterator histoIter = firstHisto; histoIter != lastHisto; histoIter++){
	    if (histoIter->second != 0) delete histoIter->second;
	  }
	}
      }
    }
  }
}

void JetPerf4DHistograms::writeHistograms() {
  
  const histoMap4d_t::iterator first4d = this->begin();
  const histoMap4d_t::iterator last4d = this->end();

  for (histoMap4d_t::iterator histoListIter = first4d; histoListIter != last4d; histoListIter++){
    if (histoListIter->second != 0) {
      
      const histoMap3d_t::iterator first3d = (histoListIter->second)->begin();
      const histoMap3d_t::iterator last3d = (histoListIter->second)->end();

      //std::cout<<"HERE"<<std::endl;
  
      for (histoMap3d_t::iterator histoNPVIter = first3d; histoNPVIter != last3d; histoNPVIter++){
	//std::cout<<"HERE"<<std::endl;
  
	if (histoNPVIter->second != 0) {

	  const histoMap_t::iterator firstHisto = (histoNPVIter->second)->begin();
	  const histoMap_t::iterator lastHisto = (histoNPVIter->second)->end();

	  for (histoMap_t::iterator histoIter = firstHisto; histoIter != lastHisto; histoIter++){
	    if (histoIter->second != 0) histoIter->second->Write();
	  }
	}
      }
    }
  }
}

const JetPerf4DHistograms::histoMap4d_t::iterator JetPerf4DHistograms::findEtaHisto(double eta) {

  double theEta = eta;
  if (m_doAbsEta) theEta = fabs(eta);

  //loop over map and check whether eta is within the range 
  const histoMap4d_t::iterator first = this->begin();
  const histoMap4d_t::iterator last = this->end();
  for (histoMap4d_t::iterator histoListIter = first; histoListIter != last; histoListIter++){
    
    //reminder:  histoIter->first is std::pair<low eta edge, high eta edge>
    double lowEtaEdge = histoListIter->first.first;
    double highEtaEdge = histoListIter->first.second;

    if (theEta > lowEtaEdge && theEta <= highEtaEdge) {
      return histoListIter;
    }

  }
  return last; //need to check validity of this - if eta of jet is outside coverage. 
}

const histoMap3d_t::iterator JetPerf4DHistograms::findNPVHisto(double NPV, const JetPerf4DHistograms::histoMap4d_t::iterator etaHistoIter) {

  //loop over map and check whether NPV is within the range
  const histoMap3d_t::iterator first = (etaHistoIter->second)->begin();
  const histoMap3d_t::iterator last = (etaHistoIter->second)->end();

  for (histoMap3d_t::iterator histoIter = first; histoIter != last; histoIter++){
    
    double lowNPVEdge = histoIter->first.first;
    double highNPVEdge = histoIter->first.second;

    if (NPV >= lowNPVEdge && NPV < highNPVEdge) {
      return histoIter;
    }

  }
  return last; //need to check validity of this - if NPV is outside coverage.
}

const histoMap_t::iterator JetPerf4DHistograms::findPtHisto(double pt, const histoMap3d_t::iterator NPVHistoIter) {
  
  //loop over map and check whether pt is within the range 
  const histoMap_t::iterator first = (NPVHistoIter->second)->begin();
  const histoMap_t::iterator last = (NPVHistoIter->second)->end();
  
  for (histoMap_t::iterator histoIter = first; histoIter != last; histoIter++){

    double lowPtEdge = histoIter->first.first;
    double highPtEdge = histoIter->first.second;
    

    if (pt > lowPtEdge && pt <= highPtEdge) {
      return histoIter;
    }

  }
  return last; //need to check validity of this - if pt of jet is outside coverage. 
}

