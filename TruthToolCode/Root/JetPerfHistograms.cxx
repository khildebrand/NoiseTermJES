
#include "TruthToolCode/JetPerfHistograms.h"
#include <cstdlib>
#include <TMath.h>
//#include "Logger.h"
#include "util/Utilities.h"
#include <iostream>



  JetPerfHistograms::JetPerfHistograms(const std::string & name, const std::string & etaBinsString, const std::string & ptBinsString, const bool absEta){
//JetPerfHistograms::JetPerfHistograms(const std::string & name, const std::string & etaBinsString, std::vector<double> & ptBinsVecDouble, const bool doAbsEta) {

  m_doAbsEta = absEta;
  m_name = name;
  m_etaBins = Utilities::tokenizeBins(etaBinsString);
  m_ptBins = Utilities::tokenizeBins(ptBinsString);

  checkBinVector(m_etaBins);
  checkBinVector(m_ptBins);

  //m_ptBins = ptBinsVecDouble;

  //adding eta histograms - starting from the _high_, so we don't go out of range
  for (unsigned int iEtaHigh = 1; iEtaHigh < m_etaBins.size(); iEtaHigh++) {

    unsigned int iEtaLow = iEtaHigh-1;
    double etaLow = m_etaBins[iEtaLow];
    double etaHigh = m_etaBins[iEtaHigh];
    addHistogram(etaLow, etaHigh, m_ptBins);

  }


}

std::vector<double> JetPerfHistograms::tokenizeBins(const std::string & binString) {

  //TODO: some checks on string validity go here 

  //convert a string into tokens
  //first of all make it into a c string, and remove the const qualifier -> this gets us in trouble!
  //copy the string 
  std::string binStringTmp = binString;

  char * binCString = const_cast<char *>(binStringTmp.c_str());

  //tokenize!
  char * token;
  double Dtoken; 
  std::vector<double> binVector;
  //printf ("Splitting string \"%s\" into tokens:\n", binCString);
  token = strtok(binCString,":");
  while (token != NULL)  {
    //printf ("%s\n",token);
    //convert to double
    Dtoken = strtod(token, NULL);
    //push back in vector of bins
    binVector.push_back(Dtoken);
    //go to next token
    token = strtok (NULL, ":");
  }

  return binVector;

}

bool JetPerfHistograms::checkBinVector(std::vector<double> binVector) {


  //temp variable for previous 
  double previousBin = -9999999;

  //check that all bins are in increasing order
  for (unsigned int i = 0; i<binVector.size(); i++) {
  //TODO: try and understand why these exceptions don't work
//     try {
//       if (previousBin > binVector[i]) throw binOrderingEx;
//     }
//     catch(exception& e) {
//       cout << e.what() << endl;
//       log->Message(Logger::ERROR, "in checkBinVector()");
//       log->Message(Logger::ERROR, e.what());
//     }

//     std::cout << binVector[i] << std::endl;
    if (previousBin > binVector[i]) {
      std::cout << "ERROR: in JetPerfHistograms::checkBinVector() - low edges of bins not in increasing order." << std::endl;
      return false;
    }

    else previousBin = binVector[i];
  }

  return true;
}

void JetPerfHistograms::clearHistograms() {

    const histoMap_t::iterator first = this->begin();
    const histoMap_t::iterator last = this->end();

    //iterate on the map and clear the histogram
    for (histoMap_t::iterator histoIter = first; histoIter != last; histoIter++){
      if (histoIter->second != 0) histoIter->second->Clear();
    }
}

void JetPerfHistograms::fillHistograms(const double & jet_eta, const double & jet_pt, const double & jet_weight) {

      //find the eta histogram where this jet belongs
      JetPerfHistograms::histoMap_t::iterator histoIter = JetPerfHistograms::findEtaHisto(jet_eta);
 
      //we're implicitly doing an eta acceptance cut here
      if (histoIter != this->end() && histoIter->second != 0) {
        //fill it (if it's there)
        histoIter->second->Fill(jet_pt, jet_weight);
      }
 
}

void JetPerfHistograms::writeHistograms() {

    //taking care of the integrated histograms
    //the user doesn't care if we always had them in anyways
    getIntegratedEtaHistogram();
    getIntegratedPtHistogram();

    const histoMap_t::iterator first = this->begin();
    const histoMap_t::iterator last = this->end();

    //iterate on the map and clear the histogram
    for (histoMap_t::iterator histoIter = first; histoIter != last; histoIter++){
      if (histoIter->second != 0) histoIter->second->Write();
    }

}

void JetPerfHistograms::deleteHistograms() {

    //FIXME:hoping the integrated histograms get deleted by ROOT as I used Clone...

    const histoMap_t::iterator first = this->begin();
    const histoMap_t::iterator last = this->end();

    //iterate on the map and delete the histogram
    for (histoMap_t::iterator histoIter = first; histoIter != last; histoIter++){
      if (histoIter->second != 0) delete histoIter->second;
    }

}

void JetPerfHistograms::getIntegratedEtaHistogram(){

  const histoMap_t::iterator firstIter = this->begin();
  const histoMap_t::iterator lastIter = this->end();

  //remember: dealing with histoMap_t == <pair<double,double>, TH1D*>
  if(firstIter != lastIter && firstIter->second != 0){
    //copy the first histogram in the list to get the first binning
    TH1D * ptHisto = (TH1D*) firstIter->second->Clone();
    ptHisto->Reset();
    std::string lowestEta = Utilities::doubleToString(firstIter->first.first); 
    std::string highestEta = "";
  
    //iterate on the map and add the histogram
    for (histoMap_t::iterator histoIter = firstIter; histoIter != lastIter; histoIter++){
      if (histoIter->second != 0) {
        ptHisto->Add(histoIter->second);
        highestEta = Utilities::doubleToString(histoIter->first.second);
      }
    }
  
    //some basic formatting
    std::string histoName = "";
    std::string histoTitle = "";
  
    if (m_doAbsEta) {
      histoTitle = m_name+", "+lowestEta+"<|#eta|<"+highestEta;
      histoName = m_name+"_absEta_"+lowestEta+"_"+highestEta;
    }
    else {
      histoTitle = m_name+", "+lowestEta+"<#eta<"+highestEta;
      histoName = m_name+"_eta_"+lowestEta+"_"+highestEta;
    }
  
    std::string xAxisTitle = "p_{T}^{jet} [GeV]";
    std::string yAxisTitle = "Number of jets";
    std::string histoTitleAxis = histoTitle+";"+xAxisTitle+";"+yAxisTitle;
  
    ptHisto->SetName(histoName.c_str());
    ptHisto->SetTitle(histoTitle.c_str());
  
    //ptHisto->Print();
    ptHisto->Write();
  }

  else {
      std::cout << "WARNING: in JetPerfHistograms::getIntegratedEtaHistogram(), no histogram found" << std::endl;
  }
 

}

void JetPerfHistograms::getIntegratedPtHistogram(){

  //TODO: code duplication - it is the same as addHistogram, just that we have to deal with a histogram in eta bins
  //TODO: can be fixed with external booking function


  //Take one of the histograms as an example
  const histoMap_t::iterator firstIter = this->begin();
  const histoMap_t::iterator lastIter = this->end();

  //remember: dealing with histoMap_t == <pair<double,double>, TH1D*>
  if(firstIter != lastIter && firstIter->second != 0){

    //basic formatting 

    //Calculate the lowest pT (note: we DO NOT WANT underflow/overflow!)
    double lowPt = firstIter->second->GetBinLowEdge(1);//this is the low edge of the first bin, excluding overflow
    double highPt = firstIter->second->GetBinLowEdge(firstIter->second->GetNbinsX()+1);//this is the low edge of the overflow bin
  
    //Names and titles
    std::string lowPtString = Utilities::doubleToString(lowPt);
    std::string highPtString = Utilities::doubleToString(highPt);
  
    std::string histoName = m_name+"_pt_"+lowPtString+"_"+highPtString;
    std::string histoTitle = m_name+", "+lowPtString+"<p_{T}^{jet}<"+highPtString;
    std::string xAxisTitle = "";
  
    if (m_doAbsEta) {
      xAxisTitle = "|#eta^{jet}|";
    }
    else {
      xAxisTitle = "#eta^{jet}";
    }
  
    std::string yAxisTitle = "Number of jets";
  
    std::string histoTitleAxis = histoTitle+";"+xAxisTitle+";"+yAxisTitle;
  
    //Array to vector: take advantage of the fact that standard C++ forces memory allocation of stl::vectors to be contiguous 
    const double * etaBinsArray = &m_etaBins[0];
  
    //Book histogram
    //we want to use the histogram constructor here: 
    //TH1::TH1(const char *name,const char *title,Int_t nbins,const Double_t *xbins)
    //
    //From ROOT docs:
    //
    //   -*-*-*-*-*Normal constructor for variable bin size histograms*-*-*-*-*-*-*
    //             ===================================================
    //
    //  Creates the main histogram structure:
    //     name   : name of histogram (avoid blanks)
    //     title  : histogram title
    //              if title is of the form "stringt;stringx;stringy;stringz"
    //              the histogram title is set to stringt,
    //              the x axis title to stringx, the y axis title to stringy, etc.
    //     nbins  : number of bins
    //     xbins  : array of low-edges for each bin
    //              This is an array of size nbins+1
    //
    //   -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
  
    TH1D * etaHisto = new TH1D(histoName.c_str(), histoTitleAxis.c_str(),  m_etaBins.size()-1, etaBinsArray);
    //etaHisto->Sumw2();

  //   //some info for the user
  //   log->Message(Logger::INFO, "in JetPerfHistograms::addHistogram() - booked new histogram");
  //   std::string ptInfo = "in JetPerfHistograms::addHistogram() - pt bins: ";
  //   for (unsigned int i=0; i<ptBins.size(); i++) {
  //     ptInfo = ptInfo + Utilities::doubleToString(ptBinsArray[i]) + " ";
  //   }
  //   std::string etaInfo = "JetPerfHistograms::addHistogram() - "+histoTitle;
  //   log->Message(Logger::INFO, etaInfo);
  //   log->Message(Logger::INFO, ptInfo);

    //now pick up the integrals for each of the single eta histograms and fill the correct eta bin of this one

    for (histoMap_t::iterator histoIter = firstIter; histoIter != lastIter; histoIter++){
      if (histoIter->second != 0) {
        //find the midpoint of the eta bin in question from the map
        double etaMidPoint = (histoIter->first.first + histoIter->first.second)/2;
        int etaBin = etaHisto->FindBin(etaMidPoint);
        etaHisto->SetBinContent(etaBin, histoIter->second->Integral());//only bins in range are considered
      }
    }//end loop on histomap

    etaHisto->Write();

  }//end if histograms found

  else {
      std::cout << "WARNING: in JetPerfHistograms::getIntegratedEtaHistogram(), no histogram found" << std::endl;
  }

} 

void JetPerfHistograms::addHistogram(const double & lowEta, const double & highEta, const std::vector<double> & ptBins) {

  //we want to use the histogram constructor here: 
  //TH1::TH1(const char *name,const char *title,Int_t nbins,const Double_t *xbins)
  //
  //From ROOT docs:
  //
  //   -*-*-*-*-*Normal constructor for variable bin size histograms*-*-*-*-*-*-*
  //             ===================================================
  //
  //  Creates the main histogram structure:
  //     name   : name of histogram (avoid blanks)
  //     title  : histogram title
  //              if title is of the form "stringt;stringx;stringy;stringz"
  //              the histogram title is set to stringt,
  //              the x axis title to stringx, the y axis title to stringy, etc.
  //     nbins  : number of bins
  //     xbins  : array of low-edges for each bin
  //              This is an array of size nbins+1
  //
  //   -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

  //Names and titles
  //(We will assume in the following that histograms of number of jets vs pT in GeV is what we're after)
  std::string lowEtaString = Utilities::doubleToString(lowEta);
  std::string highEtaString = Utilities::doubleToString(highEta);
  
  std::string histoName = "";
  std::string histoTitle = "";

  //TODO: fix code duplication in integrated histo functions through external function

  if (m_doAbsEta) {
    histoTitle = m_name+", "+lowEtaString+"<|#eta|<"+highEtaString;
    histoName = m_name+"_absEta_"+lowEtaString+"_"+highEtaString;
  }
  else {
    histoTitle = m_name+", "+lowEtaString+"<#eta<"+highEtaString;
    histoName = m_name+"_eta_"+lowEtaString+"_"+highEtaString;
  }

  std::string xAxisTitle = "p_{T}^{jet} [GeV]";
  std::string yAxisTitle = "Number of jets";
  std::string histoTitleAxis = histoTitle+";"+xAxisTitle+";"+yAxisTitle;

  //Array to vector: take advantage of the fact that standard C++ forces memory allocation of stl::vectors to be contiguous 
  const double * ptBinsArray = &ptBins[0];

  //Book histogram
  TH1D * histo = new TH1D(histoName.c_str(), histoTitleAxis.c_str(), ptBins.size()-1, ptBinsArray);
  //histo->Sumw2();

  //insert histogram into map
  this->operator[](std::make_pair(lowEta, highEta)) = histo;

  //some info for the user
  std::cout << "INFO: in JetPerfHistograms::addHistogram() - booked new histogram" << std::endl;
  std::string ptInfo = "in JetPerfHistograms::addHistogram() - pt bins: ";
  for (unsigned int i=0; i<ptBins.size(); i++) {
    ptInfo = ptInfo + Utilities::doubleToString(ptBinsArray[i]) + " ";
  }
  std::string etaInfo = "JetPerfHistograms::addHistogram() - "+histoTitle;
  std::cout << etaInfo << std::endl;
  std::cout << ptInfo << std::endl;

}

const JetPerfHistograms::histoMap_t::iterator JetPerfHistograms::findEtaHisto (double eta) {

  //loop over map and check whether eta is within the range 
  const histoMap_t::iterator first = this->begin();
  const histoMap_t::iterator last = this->end();

  //iterate on the map and clear the histogram
  for (histoMap_t::iterator histoIter = first; histoIter != last; histoIter++){
    
    //reminder:  histoIter->first is std::pair<low eta edge, high eta edge>

    double lowEtaEdge = histoIter->first.first;
    double highEtaEdge = histoIter->first.second;

    if (eta > lowEtaEdge && eta <= highEtaEdge) {
      return histoIter;
    }

  }

  return last; //need to check validity of this - if eta of jet is outside coverage. 

}

