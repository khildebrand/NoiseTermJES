# JESProfileMCResolutionPackage

#### Author: Kevin Hildebrand
#### Email: khildebrand@uchicago.edu

This package gives the resolution of MC samples using the width of pt calib over pt truth. This is accomplished through the use of 2 scripts. 

## What this package does:

Quick Overiew of the 3 scripts in the order in which they need to be ran for taking the noise term at the constituent scale from teh random cone method to the JES:

### JESProfileNew.cxx

This is a script that is very simple. The histograms from TruthToolCode has a separate plots for each pt truth slice. This script takes the plots of pt calib over pt truth and fits them with a guassian. Plots of sigma and the mean of the gaussian are made as function of the pt truth bin.


### JESProfileNoiseFitNew4Parameters2.cxx

This scripts perform fits on the plots produced by JESProfileNew.cxx. It performs a 4 parameter fit.

## How to configure this package:

This package has three different scripts each with there own config file.

Let us assume you are using the config files given in the package. 

The following options are available in the config file for JESProfileNew.cxx:

1. What input root file to use. This is the output of the package TruthToolCode. (default is blah, example in config script is "inputFile /share/t3data3/khildebr/submitDirJESV65weighted/allHistosAdded.root")
2. Name of the output root file. (default is blah,example in config script is "outputFile /share/home/khildebr/qualTask/run/JESProfileV65NewTest.root")
3. Name of the test output root file.The histograms that are saved here are the ones that mean is taken from. So you can check to see where the mean is coming from if you want too (default is blah,example in config script is "outputFileTest /share/home/khildebr/qualTask/run/TestTestPURandomConeV111.root")
4. Whether or not to write to the test output root file (default is false, example in config script is "writeToFileTest False")
5. What eta blocks to use. This must be the same as in the config script used in TruthToolCode defined as EtaBins in that packages config script. The one difference is that in TruthToolCode it is a double colon separated list, but for this package it needs to be a comma separated list (default is "0,.8", example in config file is "etaBlocks 0,0.8,1.2,2.1,2.8,3.2,3.6,4.5")
6. What pt bins to use. his must be the same as in the config script used in TruthToolCode defined as PtBins in that packages config script. It must be double colon separated list. (default is 20:25, example in config script is "PtBins 15:20:30:45:60:80:110:160:210:260:310:400:500:600:800:1000:1200:1500:1800:2500")
7. Whether the jet scale is at EM scale or LC scale. This is set by saying whether or not it is EM scale. (Default is true, example in config script "isEM true")
8. maxNPV/minNPV is the max and min NPV used in TruthToolCode. This must be the same as in the config script used in TruthToolCode defined as NPVBins in that packages config script (default is 0 and 100, examples in config script is "minNPV 0" and "maxNPV 100")

The following options are available in the config file for JESProfileNoiseFitNew4Parameters2.cxx:

1. The output directory to save the plots in (default is blah, example in config script is "outPutDir   /share/home/khildebr/qualTask/plots/fitNew3_test/") This directory must be created before running the script. If it isn't the script will complain that it doesn't exist. If plots of the same name already exist in the directory they will be overwritten.
2. What input root file to use. This needs to be the output root file from JESProfileNoiseNew.cxx. (default is blah, example in config script is "inputFile /share/home/khildebr/qualTask/run/JESProfileNoiseNewTESTV65AddError.root")
3. Name of the output root file. (default is blah,example in config script is "outputFile /share/home/khildebr/qualTask/run/JESProfile_Fit3NEW.root")
Note On 4 and 5, It can only do one at a time. As the input will either be EM or LC.
4. Whether or not to do EM scale (defaultis false, example in config script is "doEM true").
5. Whether or not to do LC scale (defaultis false, example in config script is "doLC false").
8. What eta blocks to use. This must be the same as in the config script used in TruthToolCode defined as EtaBins in that packages config script. The one difference is that in TruthToolCode it is a double colon separated list, but for this package it needs to be a comma separated list (default is "0,.8", example in config file is "etaBlocks 0,0.8,1.2,2.1,2.8,3.2,3.6,4.5")
9. What pt bins to use. his must be the same as in the config script used in TruthToolCode defined as PtBins in that packages config script. It must be double colon separated list. (default is 20:25, example in config script is "PtBins 15:20:30:45:60:80:110:160:210:260:310:400:500:600:800:1000:1200:1500:1800:2500")
10. The number of pt bins to skip when doing the fit. So if for example if you have "PtBins 15:20:30:45:60" and the number of pt bins to skip is "2" then it will start the fits at 30 GeV. (Default is 0, example in config script is "nPtBinsToSkip 2")
11. maxNPV/minNPV is the max and min NPV used in TruthToolCode. This must be the same as in the config script used in TruthToolCode defined as NPVBins in that packages config script (default is 0 and 100, examples in config script is "minNPV 0" and "maxNPV 100")

## How to run this package:

The first script that needs to be run is JESProfileNew.cxx. This script is ran on the output from the TruthToolCode. The TruthToolCode output is usually in more than one root file and when ran on more than
one dataset they needed to weighted appropriately before hadd them together. This can be done using the ReweightHistograms package. See that packages documentation on how to properly do this (This is also mentioned in the TruthToolCode documentation)

So assuming you have properly added all the output from TruthToolCode into a single root file and setup your config file as desired you just need to run the command:

```
root -b -q 'AtlasStyle.C' 'JESProfileNew.cxx ("JESProfileNew.config")'
```

If you want to run both EM scale and LC scale then you will need to have two appropriate config files, one for each scale and run the script twice.

The next step is fitting the resolution with a 4 parameter fit. This is done using the script 'JESProfileNoiseFitNew4Parameters2.cxx'.

To run them you just do the following commmand after setting up the config script: 

```
root -b -q 'AtlasStyle.C' 'JESProfileNoiseFitNew4Parameters2.cxx ("JESProfileNoiseFitNew4Parameters2.config")'
```

This will perform fits on the plots from JESProfileNew.cxx and you can see the fit parameters. JESProfileNoiseFitNew4Parameters2.cxx performs the fits over varying fit ranges starting at the bin corresponding to nPtBinsToSkip and increases the range
one bin at a time. ALl teh plots are saved and can be viewed by looking at your plots directory as defined in the config file.