bool debug=true;
bool savePNG=true;
bool savePDF=false;
void ATLASLabel3(Double_t x,Double_t y,const char* text,double labelOffset=1.,double tsize=.08,Color_t color = 1);
void ATLASLabel3(Double_t x,Double_t y,const char* text,double labelOffset,double tsize,Color_t color)
{
  TLatex l; //l.SetTextAlign(12); l.SetTextSize(tsize);
  l.SetTextSize(tsize);
  l.SetNDC();
  l.SetTextFont(72);
  l.SetTextColor(color);

  double delx = labelOffset*0.115*696*gPad->GetWh()/(472*gPad->GetWw());

  l.DrawLatex(x,y,"ATLAS");
  if (text) {
    TLatex p;
    p.SetTextSize(tsize);
    p.SetNDC();
    p.SetTextFont(42);
    p.SetTextColor(color);
    p.DrawLatex(x+delx,y,text);
  }
}
void saveOneHistoMarker(std::string HistrogramName,TH1* histoOrig,std::string xaxisTitle,std::string yaxisTitle,vector<std::string> stuffToWrite,std::string outPutDir,bool normalizedArea,Double_t xStart,Double_t yStart,Int_t yAxisStart,Int_t yAxisEnd,bool logScaleX=false,bool logScale=false,bool rebin=false);
void saveOneHistoMarker(std::string HistrogramName,TH1* histoOrig,std::string xaxisTitle,std::string yaxisTitle,vector<std::string> stuffToWrite,std::string outPutDir,bool normalizedArea,Double_t xStart,Double_t yStart,Int_t yAxisStart,Int_t yAxisEnd,bool logScaleX,bool logScale,bool rebin) {
	TH1D *histo = (TH1D*)histoOrig->Clone();
	if (histo->GetEntries()==0) {
		cout<<"histogram was empty"<<endl;
	} else {
		TCanvas *c1 = new TCanvas("c1","c1",0,0,600,600);

		if (logScale) {
			c1->SetLogy();
		}
		if (logScaleX) {
			c1->SetLogx();
		}
		TLatex latex;
		latex.SetNDC();
		latex.SetTextColor(1);
		latex.SetTextFont(42);
		latex.SetTextSize(.04);

		if (normalizedArea) {
			histo->Scale(1/(histo->Integral(1,histo->GetNbinsX())));

		}
		if (rebin) {
			histo->Rebin();
		}

		histo->Draw("P");
		//histo->GetXaxis()->SetNdivisions(30);
		//histo2->SetLineColor(4);
		/*
		Double_t yMax=0;
		Double_t yMin=0;
		yMax=histo->GetMaximum()*1.33;
		yMin=histo->GetMinimum()*.95;

		histo->SetMinimum(yMin);
		histo->SetMaximum(yMax);
		if (logScale) {
			histo->SetMinimum(.2);
			histo->SetMaximum(histo->GetMaximum()*1000);
		}*/
		histo->GetXaxis()->SetTitle(xaxisTitle.c_str());
		histo->GetXaxis()->SetLabelSize(0.04);
		//histo->GetXaxis()->SetNdivisions(505);
		//histo->GetXaxis()->SetLimits(-4.5,4.5);

		histo->GetXaxis()->SetTitleSize(.04);
		histo->GetYaxis()->SetTitle(yaxisTitle.c_str());
		histo->GetYaxis()->SetTitleSize(.04);
		histo->GetYaxis()->SetTitleOffset(1.75);
		//dataHisto->GetYaxis()->SetLabelSize(.04);
		histo->GetYaxis()->SetLabelSize(.04);

		ATLASLabel3(xStart,yStart,"Internal",.85,.04);
		Double_t x=xStart;Double_t y=yStart-.05;
		for (int j=0;j<stuffToWrite.size();j++) {
			latex.DrawLatex(x,  y, stuffToWrite[j].c_str());
			y-=.05;
		}

		std::string temp(histo->GetName());
		if (rebin) {
			temp=temp+"rebin";
		}
		temp=temp+"example";
		if (savePNG) {
			c1->Print((outPutDir+HistrogramName+".png").c_str(),"png");
		}
		if (savePDF) {
			c1->Print((outPutDir+temp+".pdf").c_str(),"pdf");
		}
	}

}
Double_t findMinRangeForFit(TH1D* histoOrig,Double_t percent) {
	TH1D *histo = (TH1D*)histoOrig->Clone();
	Double_t totalNumber=histo->Integral(1,histo->GetNbinsX());
	for (int x=1;x<histo->GetNbinsX();x++) {
		if (histo->GetBinContent(x)/totalNumber>percent) {
			return histo->GetBinLowEdge(x);
		}

	}
	cout <<"This should not of been reached 1"<<endl;
	return 1000;

}
Double_t findMaxRangeForFit(TH1D* histoOrig,Double_t percent) {
	TH1D *histo = (TH1D*)histoOrig->Clone();
	Double_t totalNumber=histo->Integral(1,histo->GetNbinsX());
	for (int x=histo->GetNbinsX();x>1;x--) {
		if (histo->GetBinContent(x)/totalNumber>percent) {
			return histo->GetBinLowEdge(x);
		}

	}
	cout <<"This should not of been reached 2"<<endl;
	return 1000;

}
Double_t getWidth(TH1D* histo) {
	int mean_bin = histo->FindBin(histo->GetMean());
	if (debug==true) {
		cout<< "mean_bin="<<mean_bin<<" mean="<<histo->GetMean()<<endl;
	}

    int binup = mean_bin;
    int bindown = mean_bin-1;
    Double_t total = histo->Integral(1,histo->FindBin(2));
    cout<< "total="<<total<<endl;
    Double_t runningup = 0;
    Double_t runningdown = 0;
    if (total<-1) {
		return -10.;
	}
    while (runningup < 0.6827/2.0*total) {
        runningup += histo->GetBinContent(binup);
        binup += 1;
	}
	cout<< "binup="<<binup<<endl;
    while (runningdown < 0.6827/2.0*total) {
        runningdown += histo->GetBinContent(bindown);
        bindown -= 1;
	}
	cout<< "bindown="<<bindown<<endl;
    Double_t width = (histo->GetBinCenter(binup)-histo->GetBinCenter(bindown))/2.;
    return width;
}
void JESProfileNew(const std::string& configFile) {
	TEnv *config = new TEnv(configFile.c_str());
	if ( !config ) {
		printf("configFile=%s doesn't appear to exists\n",configFile.c_str());
	}
	std::string inputFile = config->GetValue("inputFile","blah");
	std::string outputFile = config->GetValue("outputFile","blah");
	std::string outputFileTest = config->GetValue("outputFileTest","blah");
	bool writeToFileTest = config->GetValue("writeToFileTest",false);
	//bool doSecondary = config->GetValue("doSecondary",false);
	bool isEM = config->GetValue("isEM",true);
	debug = config->GetValue("debug",false);
	std::string etaBlocksString= config->GetValue("etaBlocks","0,.8");
	std::string PtBins= config->GetValue("PtBins","20:25");
	int minNPV= config->GetValue("minNPV",0);
	int maxNPV= config->GetValue("maxNPV",100);
	std::string temp;

	int nEtaSteps=-1;
	printf("etaBlocksString=%s\n",etaBlocksString.c_str());
	stringstream ss(etaBlocksString);
	vector<std::string> etaBlocksVector;
	while(getline(ss,temp,',')) {
		etaBlocksVector.push_back(temp);
		nEtaSteps++;
	}

	for (int i=0;i<etaBlocksVector.size();i++) {
		std::cout<<"etaBlocks["<<i<<"]="<<etaBlocksVector[i]<<std::endl;
	}
	int nPtBins=-1;
	printf("PtBins=%s\n",PtBins.c_str());
	stringstream ss2(PtBins);
	vector<std::string> PtBinsVector;
	while(getline(ss2,temp,':')) {
		PtBinsVector.push_back(temp);
		nPtBins++;
	}
	Float_t bins[PtBinsVector.size()];
	for (int i=0;i<PtBinsVector.size();i++) {
		std::cout<<"PtBinsVector["<<i<<"]="<<PtBinsVector[i]<<std::endl;
		bins[i]=std::stoi(PtBinsVector[i]);
		std::cout<<"bins["<<i<<"]="<<bins[i]<<std::endl;
	}

	//SetAtlasStyle();

	//int nEtaSteps=7;
	//Double_t etaBlocks[]={0,.8,1.2,2.1,2.8,3.2,3.6,4.5};
	//int nEtaStepsSecondary=7;
	//Double_t etaBlocksSecondary[]={2.1,2.2,2.3,2.4,2.5,2.6,2.7,2.8};
	//int nEtaStepsSecondary=1;
	//Double_t etaBlocksSecondary[]={0,2.1};
	//int startEtaStep=0;
	//int nEtaStepsToTake=7;
	int histNameInt=0;
	//int nBinsForA=1000;
	//int maxMu=35;
	//int maxNPV=30;
	TH1D *R4ConeEM_etaSlicesCalib[nEtaSteps];
	TH1D *R4ConeEM_etaSlicesCalibMean[nEtaSteps];

	TH1D *tempHisto;



	TFile *infile;
	infile = TFile::Open(inputFile.c_str());
	TFile *Testfile;
	if (writeToFileTest) {
		Testfile = TFile::Open(outputFileTest.c_str(),"RECREATE");
	}
	vector<std::string> stuffToWrite3;
	for (int x=0;x<nEtaSteps;x++) {
		cout<<x<<endl;
		cout<<"ratioVsPtCalibAntiKt4TopoEM_absEta_"+etaBlocksVector[x]+"_"+etaBlocksVector[x+1]+"_NPV_"+std::to_string(minNPV)+"_"+std::to_string(maxNPV)<<endl;
		if (isEM) {
			R4ConeEM_etaSlicesCalib[x]=new TH1D(("ratioVsPtTruthAntiKt4TopoEM_absEta_"+etaBlocksVector[x]+"_"+etaBlocksVector[x+1]+"_NPV_"+std::to_string(minNPV)+"_"+std::to_string(maxNPV)).c_str(),"ratio vs Pt truth",nPtBins,bins);
			R4ConeEM_etaSlicesCalibMean[x]=new TH1D(("MeanratioVsPtTruthAntiKt4TopoEM_absEta_"+etaBlocksVector[x]+"_"+etaBlocksVector[x+1]+"_NPV_"+std::to_string(minNPV)+"_"+std::to_string(maxNPV)).c_str(),"ratio vs Pt truth",nPtBins,bins);


		} else {
			R4ConeEM_etaSlicesCalib[x]=new TH1D(("ratioVsPtTruthAntiKt4LCTopo_absEta_"+etaBlocksVector[x]+"_"+etaBlocksVector[x+1]+"_NPV_"+std::to_string(minNPV)+"_"+std::to_string(maxNPV)).c_str(),"ratio vs Pt truth",nPtBins,bins);
			R4ConeEM_etaSlicesCalibMean[x]=new TH1D(("MeanratioVsPtTruthAntiKt4LCTopo_absEta_"+etaBlocksVector[x]+"_"+etaBlocksVector[x+1]+"_NPV_"+std::to_string(minNPV)+"_"+std::to_string(maxNPV)).c_str(),"ratio vs Pt truth",nPtBins,bins);

		}
		R4ConeEM_etaSlicesCalib[x]->SetDirectory(0);
		R4ConeEM_etaSlicesCalibMean[x]->SetDirectory(0);
		TF1* f1;
		TF1 *fitResultsTemp;
		for (int i=0;i<nPtBins;i++) {
			if (isEM) {
				tempHisto=(TH1D*)infile->Get(("respDividedByPtTruthVsPtAntiKt4TopoEM_absEta_"+etaBlocksVector[x]+"_"+etaBlocksVector[x+1]+"_NPV_"+std::to_string(minNPV)+"_"+std::to_string(maxNPV)+"_pt_"+PtBinsVector[i]+"_"+PtBinsVector[i+1]).c_str())->Clone();
			} else {
				tempHisto=(TH1D*)infile->Get(("respDividedByPtTruthVsPtAntiKt4LCTopo_absEta_"+etaBlocksVector[x]+"_"+etaBlocksVector[x+1]+"_NPV_"+std::to_string(minNPV)+"_"+std::to_string(maxNPV)+"_pt_"+PtBinsVector[i]+"_"+PtBinsVector[i+1]).c_str())->Clone();
			}
			if (writeToFileTest) {
				tempHisto->Write();
			}
			Double_t percent=.0001;

			if (tempHisto->GetEntries() >0) {

				f1=new TF1("f1","gaus",findMinRangeForFit(tempHisto,percent),findMaxRangeForFit(tempHisto,percent));
				//f1=new TF1("f1","gaus",0,10);
				f1->SetParameter(0,1E+8);
				f1->SetParameter(1,1);
				f1->SetParameter(2,.1);
				f1->SetNpx(1000);
				tempHisto->Fit(f1,"qR");
				fitResultsTemp = tempHisto->GetFunction("f1");
				std::string temp(tempHisto->GetName());
				R4ConeEM_etaSlicesCalibMean[x]->SetBinContent(i+1,fitResultsTemp->GetParameter(1));
				cout<<PtBinsVector[i]<<" "<<fitResultsTemp->GetParameter(1)<<endl;
				R4ConeEM_etaSlicesCalibMean[x]->SetBinError(i+1,fitResultsTemp->GetParError(1));
				stuffToWrite3.push_back(PtBinsVector[i]+"< p_{T}^{true} <"+PtBinsVector[i+1]);
				stuffToWrite3.push_back("c="+std::to_string(fitResultsTemp->GetParameter(0))+" +/- "+std::to_string(fitResultsTemp->GetParError(0)));
				stuffToWrite3.push_back("m="+std::to_string(fitResultsTemp->GetParameter(1))+" +/- "+std::to_string(fitResultsTemp->GetParError(1)));
				stuffToWrite3.push_back("s="+std::to_string(fitResultsTemp->GetParameter(2))+" +/- "+std::to_string(fitResultsTemp->GetParError(2)));
				stuffToWrite3.push_back("chi2= "+std::to_string(fitResultsTemp->GetChisquare())+" NDF= "+std::to_string(fitResultsTemp->GetNDF()));
				//saveOneHistoMarker(temp,tempHisto,"#frac{p_{T}^{calib}}{p_{T}^{true}}","# Events",stuffToWrite3,"/home/khildebr/qualTask/run/temp1/",false,0.45,0.88,0,0,false,false);
				stuffToWrite3.clear();
				//TF1 *fit1 = (TF1*)tempHisto->GetFunction("gaus");
				//R4ConeEM_etaSlicesCalib[x]->SetBinContent(i+1,tempHisto->GetMean());
				//R4ConeEM_etaSlicesCalib[x]->SetBinContent(i+1,fit1->GetParameter(2));


				//R4ConeEM_etaSlicesCalib[x]->SetBinContent(i+1,getWidth(tempHisto));
				//R4ConeEM_etaSlicesCalib[x]->SetBinError(i+1,0.1);


				R4ConeEM_etaSlicesCalib[x]->SetBinContent(i+1,fitResultsTemp->GetParameter(2));

				R4ConeEM_etaSlicesCalib[x]->SetBinError(i+1,fitResultsTemp->GetParError(2));

				/*
				R4ConeEM_etaSlicesCalib[x]->SetBinContent(i+1,tempHisto->GetStdDev());

				R4ConeEM_etaSlicesCalib[x]->SetBinError(i+1,tempHisto->GetStdDevError());*/
			}
			//R4ConeEM_etaSlicesCalib[x]->SetBinError(i+1,tempHisto->GetStdDev());
			//Double_t errorBar=tempHisto->GetMeanError();
			//if (errorBar>0.01) {
				//R4ConeEM_etaSlicesCalib[x]->SetBinError(i+1,tempHisto->GetMeanError());
			//} else {
				//R4ConeEM_etaSlicesCalib[x]->SetBinError(i+1,0.01);
			//}
			//cout<<tempHisto->GetMeanError()<<" "<<tempHisto->GetStdDev()<<" "<<tempHisto->GetRMS()<<" "<<tempHisto->GetEntries()<<endl;

			tempHisto->Delete();
		}
		//std::string temp(R4ConeEM_etaSlicesCalibMean[x]->GetName());
		//saveOneHistoMarker(temp,R4ConeEM_etaSlicesCalibMean[x],"p_{T}^{true}","Mean of Fit",stuffToWrite3,"/home/khildebr/qualTask/run/temp1/",false,0.45,0.88,0,0,true,false);


	}
	if (writeToFileTest) {
		Testfile->Close();
	}

	infile->Close();
	//R4ConeEM_etaSlicesCalibProfile[0]->Draw();
	//R4ConeEM_etaSlicesCalib[0]->Draw();
	TFile *outfile=new TFile(outputFile.c_str(),"RECREATE");
	for (int x=0;x<nEtaSteps;x++) {
		R4ConeEM_etaSlicesCalib[x]->Write();
		R4ConeEM_etaSlicesCalibMean[x]->Write();
	}

	outfile->Close();
}