# TruthToolCode

### Author: Kevin Hildebrand
### Email: khildebrand@uchicago.edu

This project contains the package TruthToolCode `https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/JetPerformance2015`
The package is used for evaluating jet response by comparing calorimeter jets with the matched truth jets in simulations.
I do not write this pacakge, however I did modify it slightly for use in the random cone noise study. 
It is used get the conversion from the noise term at the constituent scale to the noise term at the JES.

## What this package does:

### First Time setup of the package:

Run the commands(From within the NoiseTermJES directory):

```
setupATLAS
rcSetup Base,2.4.17
rc find_packages
rc compile
```

Hopefully everything will have compiled correctly. The next time you log in you just have to run "rcSetup". In order to run the code before doing rcSetup, you need to setup Rucio,fax,your grid proxy, and Panda.
So do the following commands prior to "rcSetup":

```
localSetupRucioClients
localSetupFAX
voms-proxy-init -voms atlas
[enter your password for your grid certificate.]
localSetupPandaClient
```

## How to configure this package:

This package is submitted to the grid using a bash script which tells which dataset to run on and which config script to use. The config scripts are located in the share directory. Since I did not write this
package I will not go into too much detail of the entire config script. I will point to the document which the whole random cone noise study was based around (https://cds.cern.ch/record/2044941/). The config
file is set such to match up with the description given in this conf note (Section 4).

I will just point out a few:

1. the eta binning to use. This is very similar to that in the RandomConeMaker Project. The only difference is here it needs to be a double colon separated list (example 0:.8:1.2)
2. the pt binning to use. This is a double colon separated list.

## How to run this package:

We will assume you have successfully compiled this package and setup Rucio and Panda.

You will want to make a bash submit script similar to that of `scripts/gridSubmit_Pythia8_2016puKevinTestNew.sh`. That is copy this script and change a few things. First change USERNAME at the top to your own 
cern id. Change the datasets to the ones you want to run over. Change OUTDS if necessary to something appropraite. On line 53 where you see "settings_20p7.config" the config file change it to the one you want to use.
For EM scale jets I recommend using settings_20p7.config and for LC scale jets I recommend using settings_20p7LC.config. Note that the two scales must be run separately.

Once you have your bash script done then you can simply submit the job to the grid by running the following command from the directory "NoiseTermJES/TruthToolCode":

```
source scripts/gridSubmit_Pythia8_2016puKevinTestNew.sh
```

This will submit the job to the grid. In my experience this package is incredibly prone to failing to run on the grid, and as far as I can tell, for no good reason. So you have to restart some jobs up to 4 times (in my experience).]
If the grid is saying it has failed for some serious issues or none of your jobs are even starting on the grid, try running locally to make sure it is isn't an actual problem.

Instructions for running this locally (don't recommmend running locally for anything other than testing) are given on the twiki above. Just run command "xAODTruthToolCode sample1,sample2  config_file_name"

## What is in the output:

Once the job is done running you need to download the output from the grid. The histograms contain a number of plots. Only a few are actually used for the random cone noise study. The ones that are used
being with `respVsPtAllAntiKt4Topo`. These are histograms of pt calibrated divided by pt after pileup subtraction in blocks of the matching truth jets pt.
And `respDividedByPtTruthVsPtAntiKt4Topo'. These are histograms of pt calibrated divided by pt truth in blocks of the matching truth jets pt.

Before one can add together the output of all the histograms they need to be reweighted using the Project ReweightHistograms. These histograms have a cutflow plot that should be used to determine the number of events.
More info on how to do this is in the documentation for the Project ReweightHistograms. THe name of the cutflow histogram is "EventCounter".


