# JESProfileResultsPackage

#### Author: Kevin Hildebrand
#### Email: khildebrand@uchicago.edu

This package is for plotting results of JESProfileRandomConesPackage and JESProfileMCResolutionPackage together on one plot.

## What this package does:

3 different scripts are included for 3 different things

### Save2Plots.cxx

This script is for saving 2 plots. It was designed to compare the outputs of JESProfileNew.cxx when ran on samples with pileup and samples without pileup. The design was to compare the mean and sigma of the gaussian fits.

##How to configure it:

1. The output directory to save plots in. (default is blah, example in config script "outPutDir   /share/home/khildebr/qualTask/plots/TestPileSamples/")
2. the full path of input file for the sample with pile up (default is blah, example in config script "inputFileFullPU /share/home/khildebr/qualTask/run/TestPURandomConeV111.root")
3. The full path of the input file for the sample without pile up (default is blah, example in config script "inputFileNoPU /share/home/khildebr/qualTask/run/JESProfileV60.root")
4. The name of the histogram in the input file to plot (default is blah, example in config script "nameOfHistogram MeanratioVsPtTruthAntiKt4TopoEM_absEta_0_0.8_NPV_0_100").
5. The title for the y-axis (default is blah, example in config script "yAxisTitle	Mean")

##How to run this script:

From the command line just run:
```
root -b -q 'AtlasStyle.C' 'Save2Plots.cxx ("Save2Plots.config")'
```

### Save4Plots.cxx

This script is for saving 4 plots. It was designed to compare the outputs from JESProfileMCResolutionPackage and JESProfileRandomConesPackage. It plots the resolution of the full pileup sample, the no pileup sample and the resolution from random cone method. It also plots the quadratic difference of the resolution from the full and no pile up samples. The input of this script was designed to be the output of JESProfileNoiseFitNew4ParametersRandomCone.cxx and JESProfileNoiseFitNew4Parameters2.cxx

##How to configure it:

1. The output directory to save plots in. (default is blah, example in config script "outPutDir   /share/home/khildebr/qualTask/plots/TestPileSamples/")
2. the full path of input file for the sample with pile up (default is blah, example in config script "inputFileFullPU /share/home/khildebr/qualTask/run/TestPURandomConeV111.root")
3. The full path of the input file for the sample without pile up (default is blah, example in config script "inputFileNoPU /share/home/khildebr/qualTask/run/JESProfileV60.root")
4. The full path of the input file for the random cone(default is blah, example in config script "inputFileNoPU /share/home/khildebr/qualTask/run/JESProfileV60.root")
5. The name of the histogram in the input file to plot (default is blah, example in config script "nameOfHistogram R4ConeEM_etaSlicesTruthDividedPt_0_0.8").
6. The name of the histogram in the input file for Random Cone to plot (default is blah, example in config script "nameOfHistogramRandomCone R4ConeEM_etaSlicesTruthDividedPtMC_0_0.8").

##How to run this script:

From the command line just run:
```
root -b -q 'AtlasStyle.C' 'Save4Plots.cxx ("Save4Plots.config")'
```