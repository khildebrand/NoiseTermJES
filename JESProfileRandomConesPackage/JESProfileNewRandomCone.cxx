bool debug=false;

void JESProfileNewRandomCone(const std::string& configFile) {
	TEnv *config = new TEnv(configFile.c_str());
	if ( !config ) {
		printf("configFile=%s doesn't appear to exists\n",configFile.c_str());
	}
	std::string inputFile = config->GetValue("inputFile","blah");
	std::string outputFile = config->GetValue("outputFile","blah");
	std::string outputFileTest = config->GetValue("outputFileTest","blah");
	bool writeToFileTest = config->GetValue("writeToFileTest",false);
	//bool doSecondary = config->GetValue("doSecondary",false);
	bool isEM = config->GetValue("isEM",true);
	debug = config->GetValue("debug",false);
	std::string etaBlocksString= config->GetValue("etaBlocks","0,.8");
	std::string PtBins= config->GetValue("PtBins","20:25");
	int minNPV= config->GetValue("minNPV",0);
	int maxNPV= config->GetValue("maxNPV",100);
	int minPt= config->GetValue("minPt",20);
	int maxPt= config->GetValue("maxPt",3900);
	std::string temp;

	int nEtaSteps=-1;
	printf("etaBlocksString=%s\n",etaBlocksString.c_str());
	stringstream ss(etaBlocksString);
	vector<std::string> etaBlocksVector;
	while(getline(ss,temp,',')) {
		etaBlocksVector.push_back(temp);
		nEtaSteps++;
	}

	for (int i=0;i<etaBlocksVector.size();i++) {
		std::cout<<"etaBlocks["<<i<<"]="<<etaBlocksVector[i]<<std::endl;
	}
	int nPtBins=-1;
	printf("PtBins=%s\n",PtBins.c_str());
	stringstream ss2(PtBins);
	vector<std::string> PtBinsVector;
	while(getline(ss2,temp,':')) {
		PtBinsVector.push_back(temp);
		nPtBins++;
	}
	Float_t bins[PtBinsVector.size()];
	for (int i=0;i<PtBinsVector.size();i++) {
		std::cout<<"PtBinsVector["<<i<<"]="<<PtBinsVector[i]<<std::endl;
		bins[i]=std::stoi(PtBinsVector[i]);
		std::cout<<"bins["<<i<<"]="<<bins[i]<<std::endl;
	}

	//SetAtlasStyle();

	//int nEtaSteps=7;
	//Double_t etaBlocks[]={0,.8,1.2,2.1,2.8,3.2,3.6,4.5};
	//int nEtaStepsSecondary=7;
	//Double_t etaBlocksSecondary[]={2.1,2.2,2.3,2.4,2.5,2.6,2.7,2.8};
	//int nEtaStepsSecondary=1;
	//Double_t etaBlocksSecondary[]={0,2.1};
	//int startEtaStep=0;
	//int nEtaStepsToTake=7;
	int histNameInt=0;
	//int nBinsForA=1000;
	//int maxMu=35;
	//int maxNPV=30;
	TH1D *R4ConeEM_etaSlicesCalib[nEtaSteps];
	TH1D *tempHisto;



	TFile *infile;
	infile = TFile::Open(inputFile.c_str());
	TFile *Testfile;
	if (writeToFileTest) {
		Testfile = TFile::Open(outputFileTest.c_str(),"RECREATE");
	}
	for (int x=0;x<nEtaSteps;x++) {
		cout<<x<<endl;
		cout<<"ratioVsPtCalibAntiKt4TopoEM_absEta_"+etaBlocksVector[x]+"_"+etaBlocksVector[x+1]+"_NPV_"+std::to_string(minNPV)+"_"+std::to_string(maxNPV)+"_pt_"+std::to_string(minPt)+"_"+std::to_string(maxPt)<<endl;
		if (isEM) {
			R4ConeEM_etaSlicesCalib[x]=new TH1D(("ratioVsPtTruthAntiKt4TopoEM_absEta_"+etaBlocksVector[x]+"_"+etaBlocksVector[x+1]+"_NPV_"+std::to_string(minNPV)+"_"+std::to_string(maxNPV)).c_str(),"ratio vs Pt truth",nPtBins,bins);
		} else {
			R4ConeEM_etaSlicesCalib[x]=new TH1D(("ratioVsPtTruthAntiKt4LCTopo_absEta_"+etaBlocksVector[x]+"_"+etaBlocksVector[x+1]+"_NPV_"+std::to_string(minNPV)+"_"+std::to_string(maxNPV)).c_str(),"ratio vs Pt truth",nPtBins,bins);
		}
		R4ConeEM_etaSlicesCalib[x]->SetDirectory(0);
		TF1 f1;
		for (int i=0;i<nPtBins;i++) {
			if (isEM) {
				tempHisto=(TH1D*)infile->Get(("respVsPtAllAntiKt4TopoEM_absEta_"+etaBlocksVector[x]+"_"+etaBlocksVector[x+1]+"_NPV_"+std::to_string(minNPV)+"_"+std::to_string(maxNPV)+"_pt_"+PtBinsVector[i]+"_"+PtBinsVector[i+1]).c_str())->Clone();
			} else {
				tempHisto=(TH1D*)infile->Get(("respVsPtAllAntiKt4LCTopo_absEta_"+etaBlocksVector[x]+"_"+etaBlocksVector[x+1]+"_NPV_"+std::to_string(minNPV)+"_"+std::to_string(maxNPV)+"_pt_"+PtBinsVector[i]+"_"+PtBinsVector[i+1]).c_str())->Clone();
			}
			if (writeToFileTest) {
				tempHisto->Write();
			}
			//f1=new TF1("f1","gaus",15,2500);
			//if (tempHisto->GetEntries() >0) {
				//tempHisto->Fit("gaus","0");
				//TF1 *fit1 = (TF1*)tempHisto->GetFunction("gaus");
				R4ConeEM_etaSlicesCalib[x]->SetBinContent(i+1,tempHisto->GetMean());
				//R4ConeEM_etaSlicesCalib[x]->SetBinContent(i+1,fit1->GetParameter(2));
				//R4ConeEM_etaSlicesCalib[x]->SetBinContent(i+1,tempHisto->GetStdDev());

			//	R4ConeEM_etaSlicesCalib[x]->SetBinError(i+1,0);
			//}
			//R4ConeEM_etaSlicesCalib[x]->SetBinError(i+1,tempHisto->GetStdDev());
			//Double_t errorBar=tempHisto->GetMeanError();
			//if (errorBar>0.01) {
				R4ConeEM_etaSlicesCalib[x]->SetBinError(i+1,tempHisto->GetMeanError());
			//} else {
				//R4ConeEM_etaSlicesCalib[x]->SetBinError(i+1,0.01);
			//}
			cout<<tempHisto->GetMeanError()<<" "<<tempHisto->GetStdDev()<<" "<<tempHisto->GetRMS()<<" "<<tempHisto->GetEntries()<<endl;

			tempHisto->Delete();
		}


	}
	if (writeToFileTest) {
		Testfile->Close();
	}
	infile->Close();
	//R4ConeEM_etaSlicesCalibProfile[0]->Draw();
	//R4ConeEM_etaSlicesCalib[0]->Draw();
	TFile *outfile=new TFile(outputFile.c_str(),"RECREATE");
	for (int x=0;x<nEtaSteps;x++) {
		R4ConeEM_etaSlicesCalib[x]->Write();

	}

	outfile->Close();
}