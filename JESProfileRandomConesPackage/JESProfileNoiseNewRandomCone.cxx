bool debug=false;
bool savePNG=true;
bool savePDF=false;
void ATLASLabel3(Double_t x,Double_t y,const char* text,double labelOffset=1.,double tsize=.08,Color_t color = 1);
void ATLASLabel3(Double_t x,Double_t y,const char* text,double labelOffset,double tsize,Color_t color)
{
  TLatex l; //l.SetTextAlign(12); l.SetTextSize(tsize);
  l.SetTextSize(tsize);
  l.SetNDC();
  l.SetTextFont(72);
  l.SetTextColor(color);

  double delx = labelOffset*0.115*696*gPad->GetWh()/(472*gPad->GetWw());

  l.DrawLatex(x,y,"ATLAS");
  if (text) {
    TLatex p;
    p.SetTextSize(tsize);
    p.SetNDC();
    p.SetTextFont(42);
    p.SetTextColor(color);
    p.DrawLatex(x+delx,y,text);
  }
}
void saveSingleHisto(TH1* histoOrig,std::string xaxisTitle,std::string yaxisTitle,vector<std::string> stuffToWrite,std::string outPutDir,bool normalizedArea,Double_t xStart,Double_t yStart,Int_t yAxisStart,Int_t yAxisEnd,bool logScale=false,bool rebin=false);
void saveSingleHisto(TH1* histoOrig,std::string xaxisTitle,std::string yaxisTitle,vector<std::string> stuffToWrite,std::string outPutDir,bool normalizedArea,Double_t xStart,Double_t yStart,Int_t yAxisStart,Int_t yAxisEnd,bool logScale,bool rebin) {
	TH1D *histo = (TH1D*)histoOrig->Clone();
	TCanvas *c1 = new TCanvas("c1","c1",0,0,600,600);

	if (logScale) {
		c1->SetLogy();
	}
	TLatex latex;
	latex.SetNDC();
	latex.SetTextColor(1);
	latex.SetTextFont(42);
	latex.SetTextSize(.04);

	if (normalizedArea) {
		/*cout<<stack->GetNbinsX()<<" " <<dataHisto->GetNbinsX()<<endl;
		cout<<stack->Integral()<< " " << dataHisto->Integral()<<endl;
		stack->Scale(dataHisto->Integral()/(stack->Integral()));
		cout<<stack->Integral()<<endl;
		//dataHisto->Scale(1/(dataHisto->Integral()));*/

		cout<<histo->GetNbinsX()<<endl;
		cout<<histo->Integral(1,histo->GetNbinsX())<<endl;
		histo->Scale(1/(histo->Integral(1,histo->GetNbinsX())));
		cout<<histo->Integral(1,histo->GetNbinsX())<<endl;
		//dataHisto->Scale(1/(dataHisto->Integral()));
		//stuffToWrite.push_back("Period A4");
	}
	if (rebin) {
		histo->Rebin();
	}

	histo->Draw("HIST");
	if (logScale) {
		histo->SetMinimum(.2);
		histo->SetMaximum(histo->GetMaximum()*1000);
	}
	histo->GetXaxis()->SetTitle(xaxisTitle.c_str());
	histo->GetXaxis()->SetLabelSize(0.04);
	histo->GetXaxis()->SetNdivisions(505);
	histo->GetXaxis()->SetTitleSize(.04);
	histo->GetYaxis()->SetTitle(yaxisTitle.c_str());
	histo->GetYaxis()->SetTitleSize(.04);
	histo->GetYaxis()->SetTitleOffset(1.75);
	//dataHisto->GetYaxis()->SetLabelSize(.04);
	histo->GetYaxis()->SetLabelSize(.04);


	ATLASLabel3(xStart,yStart,"Internal",.85,.04);
	Double_t x=xStart;Double_t y=yStart-.045;
	for (int j=0;j<stuffToWrite.size();j++) {
		latex.DrawLatex(x,  y, stuffToWrite[j].c_str());
		y-=.045;
	}

	std::string temp(histo->GetName());
	if (rebin) {
		temp=temp+"rebin";
	}
	if (savePNG) {
		c1->Print((outPutDir+temp+".png").c_str(),"png");
	}
	if (savePDF) {
		c1->Print((outPutDir+temp+".pdf").c_str(),"pdf");
	}

}
void saveTwoHisto(TH1* histoOrig,TH1* histoOrig2,TLegend* legend,std::string xaxisTitle,std::string yaxisTitle,vector<std::string> stuffToWrite,std::string outPutDir,bool normalizedArea,Double_t xStart,Double_t yStart,Int_t yAxisStart,Int_t yAxisEnd,bool logScale=false,bool rebin=false);
void saveTwoHisto(TH1* histoOrig,TH1* histoOrig2,TLegend* legend,std::string xaxisTitle,std::string yaxisTitle,vector<std::string> stuffToWrite,std::string outPutDir,bool normalizedArea,Double_t xStart,Double_t yStart,Int_t yAxisStart,Int_t yAxisEnd,bool logScale,bool rebin) {
	cout<<"heyo1"<<endl;
	TH1D *histo = (TH1D*)histoOrig->Clone();
	cout<<"heyo2"<<endl;
	TH1D *histo2 = (TH1D*)histoOrig2->Clone();
	cout<<"heyo3"<<endl;
	TCanvas *c1 = new TCanvas("c1","c1",0,0,600,600);
	cout<<"heyo4"<<endl;
	if (logScale) {
		c1->SetLogy();
	}
	TLatex latex;
	latex.SetNDC();
	latex.SetTextColor(1);
	latex.SetTextFont(42);
	latex.SetTextSize(.04);

	if (normalizedArea) {
		histo->Scale(1/(histo->Integral(1,histo->GetNbinsX())));
		histo2->Scale(1/(histo2->Integral(1,histo2->GetNbinsX())));

	}
	if (rebin) {
		histo->Rebin();
		histo2->Rebin();
	}

	histo->Draw("HIST");
	//histo2->SetLineColor(2);
	histo2->Draw("HIST same");
	if (logScale) {
		histo->SetMinimum(.2);
		histo->SetMaximum(histo->GetMaximum()*1000);
	}
	histo->GetXaxis()->SetTitle(xaxisTitle.c_str());
	histo->GetXaxis()->SetLabelSize(0.04);
	histo->GetXaxis()->SetNdivisions(505);
	histo->GetXaxis()->SetTitleSize(.04);
	histo->GetYaxis()->SetTitle(yaxisTitle.c_str());
	histo->GetYaxis()->SetTitleSize(.04);
	histo->GetYaxis()->SetTitleOffset(1.75);
	//dataHisto->GetYaxis()->SetLabelSize(.04);
	histo->GetYaxis()->SetLabelSize(.04);

	legend->Draw();
	ATLASLabel3(xStart,yStart,"Internal",.85,.04);
	Double_t x=xStart;Double_t y=yStart-.045;
	for (int j=0;j<stuffToWrite.size();j++) {
		latex.DrawLatex(x,  y, stuffToWrite[j].c_str());
		y-=.045;
	}

	std::string temp(histo->GetName());
	if (rebin) {
		temp=temp+"rebin";
	}
	if (!normalizedArea) {
		temp=temp+"NotNormalized";
	}
	if (savePNG) {
		c1->Print((outPutDir+temp+".png").c_str(),"png");
	}
	if (savePDF) {
		c1->Print((outPutDir+temp+".pdf").c_str(),"pdf");
	}

}
void saveOneHistoMarker(TH1* histoOrig,std::string xaxisTitle,std::string yaxisTitle,vector<std::string> stuffToWrite,std::string outPutDir,bool normalizedArea,Double_t xStart,Double_t yStart,Int_t yAxisStart,Int_t yAxisEnd,bool logScaleX=false,bool logScale=false,bool rebin=false);
void saveOneHistoMarker(TH1* histoOrig,std::string xaxisTitle,std::string yaxisTitle,vector<std::string> stuffToWrite,std::string outPutDir,bool normalizedArea,Double_t xStart,Double_t yStart,Int_t yAxisStart,Int_t yAxisEnd,bool logScaleX,bool logScale,bool rebin) {
	TH1D *histo = (TH1D*)histoOrig->Clone();
	if (histo->GetEntries()==0) {
		cout<<"histogram was empty"<<endl;
	} else {
		TCanvas *c1 = new TCanvas("c1","c1",0,0,600,600);

		if (logScale) {
			c1->SetLogy();
		}
		if (logScaleX) {
			c1->SetLogx();
		}
		TLatex latex;
		latex.SetNDC();
		latex.SetTextColor(1);
		latex.SetTextFont(42);
		latex.SetTextSize(.04);

		if (normalizedArea) {
			histo->Scale(1/(histo->Integral(1,histo->GetNbinsX())));

		}
		if (rebin) {
			histo->Rebin();
		}

		histo->Draw("HIST P");
		//histo->GetXaxis()->SetNdivisions(30);
		//histo2->SetLineColor(4);
		Double_t yMax=0;
		Double_t yMin=0;
		yMax=histo->GetMaximum()*1.33;
		yMin=histo->GetMinimum()*.95;

		histo->SetMinimum(yMin);
		histo->SetMaximum(yMax);
		if (logScale) {
			histo->SetMinimum(.2);
			histo->SetMaximum(histo->GetMaximum()*1000);
		}
		histo->GetXaxis()->SetTitle(xaxisTitle.c_str());
		histo->GetXaxis()->SetLabelSize(0.04);
		//histo->GetXaxis()->SetNdivisions(505);
		//histo->GetXaxis()->SetLimits(-4.5,4.5);

		histo->GetXaxis()->SetTitleSize(.04);
		histo->GetYaxis()->SetTitle(yaxisTitle.c_str());
		histo->GetYaxis()->SetTitleSize(.04);
		histo->GetYaxis()->SetTitleOffset(1.75);
		//dataHisto->GetYaxis()->SetLabelSize(.04);
		histo->GetYaxis()->SetLabelSize(.04);

		ATLASLabel3(xStart,yStart,"Internal",.85,.04);
		Double_t x=xStart;Double_t y=yStart-.045;
		for (int j=0;j<stuffToWrite.size();j++) {
			latex.DrawLatex(x,  y, stuffToWrite[j].c_str());
			y-=.045;
		}

		std::string temp(histo->GetName());
		if (rebin) {
			temp=temp+"rebin";
		}
		temp=temp+"example";
		if (savePNG) {
			c1->Print((outPutDir+temp+".png").c_str(),"png");
		}
		if (savePDF) {
			c1->Print((outPutDir+temp+".pdf").c_str(),"pdf");
		}
	}

}
void saveTwoHistoMarker(TH1* histoOrig,TH1* histoOrig2,TLegend* legend,std::string xaxisTitle,std::string yaxisTitle,vector<std::string> stuffToWrite,std::string outPutDir,bool normalizedArea,Double_t xStart,Double_t yStart,Int_t yAxisStart,Int_t yAxisEnd,bool logScaleX=false,bool logScale=false,bool rebin=false);
void saveTwoHistoMarker(TH1* histoOrig,TH1* histoOrig2,TLegend* legend,std::string xaxisTitle,std::string yaxisTitle,vector<std::string> stuffToWrite,std::string outPutDir,bool normalizedArea,Double_t xStart,Double_t yStart,Int_t yAxisStart,Int_t yAxisEnd,bool logScaleX,bool logScale,bool rebin) {
	TH1D *histo = (TH1D*)histoOrig->Clone();
	TH1D *histo2 = (TH1D*)histoOrig2->Clone();
	if (histo->GetEntries()+histo2->GetEntries()==0) {
		cout<<"histogram was empty"<<endl;
	} else {
		TCanvas *c1 = new TCanvas("c1","c1",0,0,600,600);

		if (logScale) {
			c1->SetLogy();
		}
		if (logScaleX) {
			c1->SetLogx();
		}
		TLatex latex;
		latex.SetNDC();
		latex.SetTextColor(1);
		latex.SetTextFont(42);
		latex.SetTextSize(.04);

		if (normalizedArea) {
			histo->Scale(1/(histo->Integral(1,histo->GetNbinsX())));
			histo2->Scale(1/(histo2->Integral(1,histo2->GetNbinsX())));

		}
		if (rebin) {
			histo->Rebin();
			histo2->Rebin();
		}

		histo->Draw("HIST P");
		//histo->GetXaxis()->SetNdivisions(30);
		//histo2->SetLineColor(4);
		histo2->Draw("P same HIST");
		Double_t yMax=0;
		Double_t yMin=0;
		if (histo->GetMaximum()>histo2->GetMaximum()) {
			yMax=histo->GetMaximum()*1.33;
		} else {
			yMax=histo2->GetMaximum()*1.33;
		}
		if (histo->GetMinimum()>histo2->GetMinimum()) {
			yMin=histo2->GetMinimum()*.95;
		} else {
			yMin=histo->GetMinimum()*.95;
		}
		histo->SetMinimum(yMin);
		histo->SetMaximum(yMax);
		if (logScale) {
			histo->SetMinimum(.2);
			histo->SetMaximum(histo->GetMaximum()*1000);
		}
		histo->GetXaxis()->SetTitle(xaxisTitle.c_str());
		histo->GetXaxis()->SetLabelSize(0.04);
		//histo->GetXaxis()->SetNdivisions(505);
		//histo->GetXaxis()->SetLimits(-4.5,4.5);

		histo->GetXaxis()->SetTitleSize(.04);
		histo->GetYaxis()->SetTitle(yaxisTitle.c_str());
		histo->GetYaxis()->SetTitleSize(.04);
		histo->GetYaxis()->SetTitleOffset(1.75);
		//dataHisto->GetYaxis()->SetLabelSize(.04);
		histo->GetYaxis()->SetLabelSize(.04);

		legend->Draw();
		ATLASLabel3(xStart,yStart,"Internal",.85,.04);
		Double_t x=xStart;Double_t y=yStart-.045;
		for (int j=0;j<stuffToWrite.size();j++) {
			latex.DrawLatex(x,  y, stuffToWrite[j].c_str());
			y-=.045;
		}

		std::string temp(histo->GetName());
		if (rebin) {
			temp=temp+"rebin";
		}
		if (savePNG) {
			c1->Print((outPutDir+temp+".png").c_str(),"png");
		}
		if (savePDF) {
			c1->Print((outPutDir+temp+".pdf").c_str(),"pdf");
		}
	}

}
void saveFourHistoMarker(TH1* histoOrig,TH1* histoOrig2,TH1* histoOrig3,TH1* histoOrig4,TLegend* legend,std::string xaxisTitle,std::string yaxisTitle,vector<std::string> stuffToWrite,std::string outPutDir,bool normalizedArea,Double_t xStart,Double_t yStart,Int_t yAxisStart,Int_t yAxisEnd,bool logScaleX=false,bool logScale=false,bool rebin=false);
void saveFourHistoMarker(TH1* histoOrig,TH1* histoOrig2,TH1* histoOrig3,TH1* histoOrig4,TLegend* legend,std::string xaxisTitle,std::string yaxisTitle,vector<std::string> stuffToWrite,std::string outPutDir,bool normalizedArea,Double_t xStart,Double_t yStart,Int_t yAxisStart,Int_t yAxisEnd,bool logScaleX,bool logScale,bool rebin) {
	TH1D *histo = (TH1D*)histoOrig->Clone();
	TH1D *histo2 = (TH1D*)histoOrig2->Clone();
	TH1D *histo3 = (TH1D*)histoOrig3->Clone();
	TH1D *histo4 = (TH1D*)histoOrig4->Clone();
	if (histo->GetEntries()+histo2->GetEntries()+histo3->GetEntries()+histo4->GetEntries()==0) {
		cout<<"histograms were empty"<<endl;
	} else {
		TCanvas *c1 = new TCanvas("c1","c1",0,0,600,600);

		if (logScale) {
			c1->SetLogy();
		}
		if (logScaleX) {
			c1->SetLogx();
		}
		TLatex latex;
		latex.SetNDC();
		latex.SetTextColor(1);
		latex.SetTextFont(42);
		latex.SetTextSize(.04);

		if (normalizedArea) {
			histo->Scale(1/(histo->Integral(1,histo->GetNbinsX())));
			histo2->Scale(1/(histo2->Integral(1,histo2->GetNbinsX())));
			histo3->Scale(1/(histo3->Integral(1,histo3->GetNbinsX())));
			histo4->Scale(1/(histo4->Integral(1,histo4->GetNbinsX())));

		}
		if (rebin) {
			histo->Rebin();
			histo2->Rebin();
			histo3->Rebin();
			histo4->Rebin();
		}

		histo->Draw("HIST P");
		//histo->GetXaxis()->SetNdivisions(30);
		//histo2->SetLineColor(4);
		histo2->Draw("P same HIST");
		histo3->Draw("P same HIST");
		histo4->Draw("P same HIST");
		Double_t yMax=histo->GetMaximum();
		Double_t yMin=histo->GetMinimum();

		if (histo2->GetMaximum()>yMax) {
			yMax=histo2->GetMaximum();
		}
		if (histo3->GetMaximum()>yMax) {
			yMax=histo3->GetMaximum();
		}
		if (histo4->GetMaximum()>yMax) {
			yMax=histo4->GetMaximum();
		}
		yMax=yMax*1.33;

		if (histo2->GetMinimum()<yMin) {
			yMin=histo2->GetMinimum();
		}
		if (histo3->GetMinimum()<yMin) {
			yMin=histo3->GetMinimum();
		}
		if (histo4->GetMinimum()<yMin) {
			yMin=histo4->GetMinimum();
		}
		yMin=yMin*.95;
		histo->SetMinimum(yMin);
		histo->SetMaximum(yMax);
		if (logScale) {
			histo->SetMinimum(.2);
			histo->SetMaximum(histo->GetMaximum()*1000);
		}
		histo->GetXaxis()->SetTitle(xaxisTitle.c_str());
		histo->GetXaxis()->SetLabelSize(0.04);
		//histo->GetXaxis()->SetNdivisions(505);
		//histo->GetXaxis()->SetLimits(-4.5,4.5);

		histo->GetXaxis()->SetTitleSize(.04);
		histo->GetYaxis()->SetTitle(yaxisTitle.c_str());
		histo->GetYaxis()->SetTitleSize(.04);
		histo->GetYaxis()->SetTitleOffset(1.75);
		//dataHisto->GetYaxis()->SetLabelSize(.04);
		histo->GetYaxis()->SetLabelSize(.04);

		legend->Draw();
		ATLASLabel3(xStart,yStart,"Internal",.85,.04);
		Double_t x=xStart;Double_t y=yStart-.045;
		for (int j=0;j<stuffToWrite.size();j++) {
			latex.DrawLatex(x,  y, stuffToWrite[j].c_str());
			y-=.045;
		}

		std::string temp(histo->GetName());
		if (rebin) {
			temp=temp+"rebin";
		}
		if (savePNG) {
			c1->Print((outPutDir+temp+".png").c_str(),"png");
		}
		if (savePDF) {
			c1->Print((outPutDir+temp+".pdf").c_str(),"pdf");
		}
	}

}
void saveTwoHistoRatio(TH1* histoOrig,TH1* histoOrig2,std::string xaxisTitle,std::string yaxisTitle,vector<std::string> stuffToWrite,std::string outPutDir,bool normalizedArea,Double_t xStart,Double_t yStart,Int_t yAxisStart,Int_t yAxisEnd,bool logScale=false,bool rebin=false);
void saveTwoHistoRatio(TH1* histoOrig,TH1* histoOrig2,std::string xaxisTitle,std::string yaxisTitle,vector<std::string> stuffToWrite,std::string outPutDir,bool normalizedArea,Double_t xStart,Double_t yStart,Int_t yAxisStart,Int_t yAxisEnd,bool logScale,bool rebin) {
	TH1D *histo = (TH1D*)histoOrig->Clone();
	TH1D *histo2 = (TH1D*)histoOrig2->Clone();
	TCanvas *c1 = new TCanvas("c1","c1",0,0,600,600);
	TPad *pad1 = new TPad("pad1","pad1",0,0.35,1,1);
	pad1->SetBottomMargin(0.0);
	pad1->SetLeftMargin(0.185);
	pad1->Draw();
	c1->cd();
	TPad *pad2 = new TPad("pad2","pad2",0,0,1,0.35);
	pad2->SetTopMargin(0.0);
	//pad2->SetBottomMargin(0.15);
	pad2->SetBottomMargin(0.3);
	pad2->SetLeftMargin(0.185);
	pad2->Draw();
	//pad2->SetGridy();

	pad1->cd();
	if (logScale) {
		c1->SetLogy();
	}
	TLatex latex;
	latex.SetNDC();
	latex.SetTextColor(1);
	latex.SetTextFont(42);
	latex.SetTextSize(.04);

	if (normalizedArea) {
		histo->Scale(1/(histo->Integral(1,histo->GetNbinsX())));
		histo2->Scale(1/(histo2->Integral(1,histo2->GetNbinsX())));

	}
	if (rebin) {
		histo->Rebin();
		histo2->Rebin();
	}

	histo->Draw("hist");
	histo2->SetLineColor(2);
	histo2->Draw("hist same");
	if (logScale) {
		histo->SetMinimum(.2);
		histo->SetMaximum(histo->GetMaximum()*1000);
	}
	histo->GetXaxis()->SetTitle(xaxisTitle.c_str());
	histo->GetXaxis()->SetLabelSize(0);
	histo->GetXaxis()->SetNdivisions(505);
	histo->GetXaxis()->SetTitleSize(.04);
	histo->GetYaxis()->SetTitle(yaxisTitle.c_str());
	histo->GetYaxis()->SetTitleSize(.04);
	histo->GetYaxis()->SetTitleOffset(1.75);
	//dataHisto->GetYaxis()->SetLabelSize(.04);
	histo->GetYaxis()->SetLabelSize(.04);


	ATLASLabel3(xStart,yStart,"Internal",.85,.04);
	Double_t x=xStart;Double_t y=yStart-.045;
	for (int j=0;j<stuffToWrite.size();j++) {
		latex.DrawLatex(x,  y, stuffToWrite[j].c_str());
		y-=.045;
	}
	pad2->cd();

	TH1F *h1_ratio = (TH1F*)histo->Clone();
	//h1_ratio->Add(h1_ratio,stack1,1,-1);
	h1_ratio->Divide(histo2);
	//h1_ratio->SetMarkerColor(colourAdd);
	//h1_ratio->GetYaxis()->SetRangeUser(0, 2.);
	h1_ratio->GetYaxis()->SetRangeUser(.5, 1.5);
	h1_ratio->GetYaxis()->SetTitle("Pos/Neg #eta");
	//h1_ratio->SetMaximum(.19);
	//h1_ratio->SetMinimum(-.19);
	h1_ratio->GetYaxis()->SetTitleSize(.12);
	h1_ratio->GetYaxis()->SetTitleOffset(.70);
	h1_ratio->GetYaxis()->SetLabelSize(.11);
	h1_ratio->GetXaxis()->SetTitle(xaxisTitle.c_str());
	h1_ratio->GetXaxis()->SetLabelSize(.11);
	h1_ratio->GetXaxis()->SetTitleOffset(1.1);
	/*h1_ratio->GetYaxis()->SetTitleSize(.08);
	h1_ratio->GetYaxis()->SetTitleOffset(.8);
	h1_ratio->GetYaxis()->SetLabelSize(.08);
	h1_ratio->GetXaxis()->SetTitle(xaxisTitle.c_str());
	h1_ratio->GetXaxis()->SetLabelSize(.08);
	h1_ratio->GetXaxis()->SetTitleOffset(.8);*/
	h1_ratio->GetYaxis()->SetNdivisions(10);
	//h1_ratio->GetYaxis()->SetTitleOffset(1.85);
	//h1_ratio->GetYaxis()->SetTitleFont(43);
	//h1_ratio->GetYaxis()->SetLabelSize(0);
	//h1_ratio->GetYaxis()->SetTitleSize(28);
	//h1_ratio->GetXaxis()->SetLabelFont(43);
	//h1_ratio->GetXaxis()->SetTitleFont(43);
	//h1_ratio->GetXaxis()->SetLabelSize(28);
	h1_ratio->GetXaxis()->SetTitleSize(.12);
	//h1_ratio->GetXaxis()->SetRangeUser(xmin, xmax);
	//TGaxis *ratioaxis = new TGaxis(xmin, 0.5, xmin, 1.5, 0.5, 1.5, 503, "");
	//ratioaxis->SetLabelFont(43);
	//ratioaxis->SetLabelSize(28);

	h1_ratio->Draw("P");


	TLine *line=new TLine(h1_ratio->GetXaxis()->GetXmin(),1,h1_ratio->GetXaxis()->GetXmax(),1);
	line->SetLineStyle(7);
	line->Draw();
	//ratioaxis->Draw();
	//cout<<"before save"<<endl;


	std::string temp(histo->GetName());
	if (rebin) {
		temp=temp+"rebin";
	}
	temp=temp+"ratio";
	if (savePNG) {
		c1->Print((outPutDir+temp+".png").c_str(),"png");
	}
	if (savePDF) {
		c1->Print((outPutDir+temp+".pdf").c_str(),"pdf");
	}

}
void saveTwoHistoRatioAsy(TH1* histoOrig,TH1* histoOrig2,TLegend* legend,std::string xaxisTitle,std::string yaxisTitle,vector<std::string> stuffToWrite,std::string outPutDir,bool normalizedArea,Double_t xStart,Double_t yStart,Int_t yAxisStart,Int_t yAxisEnd,bool logScale=false,bool rebin=false);
void saveTwoHistoRatioAsy(TH1* histoOrig,TH1* histoOrig2,TLegend* legend,std::string xaxisTitle,std::string yaxisTitle,vector<std::string> stuffToWrite,std::string outPutDir,bool normalizedArea,Double_t xStart,Double_t yStart,Int_t yAxisStart,Int_t yAxisEnd,bool logScale,bool rebin) {
	TH1D *histo = (TH1D*)histoOrig->Clone();
	TH1D *histo2 = (TH1D*)histoOrig2->Clone();
	TCanvas *c1 = new TCanvas("c1","c1",0,0,600,600);
	TPad *pad1 = new TPad("pad1","pad1",0,0.35,1,1);
	pad1->SetBottomMargin(0.0);
	pad1->SetLeftMargin(0.185);
	pad1->Draw();
	c1->cd();
	TPad *pad2 = new TPad("pad2","pad2",0,0,1,0.35);
	pad2->SetTopMargin(0.0);
	//pad2->SetBottomMargin(0.15);
	pad2->SetBottomMargin(0.3);
	pad2->SetLeftMargin(0.185);
	pad2->Draw();
	//pad2->SetGridy();

	pad1->cd();
	if (logScale) {
		c1->SetLogy();
	}
	TLatex latex;
	latex.SetNDC();
	latex.SetTextColor(1);
	latex.SetTextFont(42);
	latex.SetTextSize(.04);
	Double_t chi2=histo->Chi2Test(histo2,"CHI2");
	Double_t chi2ndf=histo->Chi2Test(histo2,"CHI2/NDF");
	Double_t ksTest=histo->KolmogorovTest(histo2);
	stuffToWrite.push_back("CHI2="+std::to_string(chi2));
	stuffToWrite.push_back("chi2ndf="+std::to_string(chi2ndf));
	stuffToWrite.push_back("ksTest="+std::to_string(ksTest));
	if (normalizedArea) {
		histo->Scale(1/(histo->Integral(1,histo->GetNbinsX())));
		histo2->Scale(1/(histo2->Integral(1,histo2->GetNbinsX())));

	}
	if (rebin) {
		histo->Rebin();
		histo2->Rebin();
	}

	histo->Draw("hist");
	histo2->SetLineColor(2);
	histo2->Draw("hist same");
	legend->Draw();
	if (logScale) {
		histo->SetMinimum(.2);
		histo->SetMaximum(histo->GetMaximum()*1000);
	}
	histo->GetXaxis()->SetTitle(xaxisTitle.c_str());
	histo->GetXaxis()->SetLabelSize(0);
	histo->GetXaxis()->SetNdivisions(505);
	histo->GetXaxis()->SetTitleSize(.04);
	histo->GetYaxis()->SetTitle(yaxisTitle.c_str());
	histo->GetYaxis()->SetTitleSize(.04);
	histo->GetYaxis()->SetTitleOffset(1.75);
	//dataHisto->GetYaxis()->SetLabelSize(.04);
	histo->GetYaxis()->SetLabelSize(.04);


	ATLASLabel3(xStart,yStart,"Internal",.85,.04);
	Double_t x=xStart;Double_t y=yStart-.045;
	for (int j=0;j<stuffToWrite.size();j++) {
		latex.DrawLatex(x,  y, stuffToWrite[j].c_str());
		y-=.045;
	}
	pad2->cd();

	TH1F *h1_ratio = (TH1F*)histo->Clone();
	TH1F *h1_den=(TH1F*)histo->Clone();
	h1_den->Add(histo2);
	//h1_ratio->Add(h1_ratio,stack1,1,-1);
	h1_ratio->Add(histo2,-1);
	h1_ratio->Divide(h1_den);
	//h1_ratio->SetMarkerColor(colourAdd);
	//h1_ratio->GetYaxis()->SetRangeUser(0, 2.);
	h1_ratio->GetYaxis()->SetRangeUser(-.5, .5);
	h1_ratio->GetYaxis()->SetTitle("(Pos-Neg)/(Pos+Neg) #eta");
	//h1_ratio->SetMaximum(.19);
	//h1_ratio->SetMinimum(-.19);
	h1_ratio->GetXaxis()->SetTitle(xaxisTitle.c_str());
	h1_ratio->GetXaxis()->SetTitleSize(.075);
	h1_ratio->GetXaxis()->SetLabelSize(.075);
	//h1_ratio->GetYaxis()->SetTitle(yaxisTitle.c_str());
	h1_ratio->GetYaxis()->SetTitleSize(.075);
	h1_ratio->GetYaxis()->SetTitleOffset(1.0);
	//dataHisto->GetYaxis()->SetLabelSize(.04);
	h1_ratio->GetYaxis()->SetLabelSize(.075);

	/*h1_ratio->GetYaxis()->SetTitleSize(.08);
	h1_ratio->GetYaxis()->SetTitleOffset(.8);
	h1_ratio->GetYaxis()->SetLabelSize(.08);
	h1_ratio->GetXaxis()->SetTitle(xaxisTitle.c_str());
	h1_ratio->GetXaxis()->SetLabelSize(.08);
	h1_ratio->GetXaxis()->SetTitleOffset(.8);*/
	h1_ratio->GetYaxis()->SetNdivisions(5);
	//h1_ratio->GetXaxis()->SetRangeUser(xmin, xmax);
	//TGaxis *ratioaxis = new TGaxis(xmin, 0.5, xmin, 1.5, 0.5, 1.5, 503, "");
	//ratioaxis->SetLabelFont(43);
	//ratioaxis->SetLabelSize(28);

	h1_ratio->Draw("P");


	TLine *line=new TLine(h1_ratio->GetXaxis()->GetXmin(),1,h1_ratio->GetXaxis()->GetXmax(),1);
	line->SetLineStyle(7);
	line->Draw();
	//ratioaxis->Draw();
	//cout<<"before save"<<endl;


	std::string temp(histo->GetName());
	if (rebin) {
		temp=temp+"rebin";
	}
	temp=temp+"Asy";
	if (savePNG) {
		c1->Print((outPutDir+temp+".png").c_str(),"png");
	}
	if (savePDF) {
		c1->Print((outPutDir+temp+".pdf").c_str(),"pdf");
	}

}
Double_t getWidth(TH1D* histo) {
	int mean_bin = histo->FindBin(histo->GetMean());
	if (debug==true) {
		cout<< "mean_bin="<<mean_bin<<" mean="<<histo->GetMean()<<endl;
	}

    int binup = mean_bin;
    int bindown = mean_bin-1;
    Double_t total = histo->Integral();
    Double_t runningup = 0;
    Double_t runningdown = 0;
    if (total<50) {
		return -10.;
	}
    while (runningup < 0.6827/2.0*total) {
        runningup += histo->GetBinContent(binup);
        binup += 1;
	}
    while (runningdown < 0.6827/2.0*total) {
        runningdown += histo->GetBinContent(bindown);
        bindown -= 1;
	}

    Double_t width = (histo->GetBinCenter(binup)-histo->GetBinCenter(bindown))/2.;
    return width/1000.;
}
void JESProfileNoiseNewRandomCone(const std::string& configFile) {
	TEnv *config = new TEnv(configFile.c_str());
	if ( !config ) {
		printf("configFile=%s doesn't appear to exists\n",configFile.c_str());
	}
	std::string outPutDir = config->GetValue("outPutDir","blah");
	std::string inputFileProfile = config->GetValue("inputFileProfile","blah");
	std::string inputFileProfileLC = config->GetValue("inputFileProfileLC","blah");
	std::string inputFileNoise = config->GetValue("inputFileNoise","blah");
	std::string outputFile = config->GetValue("outputFile","blah");
	bool doLC = config->GetValue("doLC",false);
	bool doEM = config->GetValue("doEM",false);
	debug = config->GetValue("debug",false);
	std::string etaBlocksString= config->GetValue("etaBlocks","0,.8");
	std::string PtBins= config->GetValue("PtBins","20:25");
	int minNPV= config->GetValue("minNPV",0);
	int maxNPV= config->GetValue("maxNPV",100);
	std::string temp;
	bool doData = config->GetValue("doData",true);
	bool doMC = config->GetValue("doMC",true);

	int nEtaSteps=-1;
	printf("etaBlocksString=%s\n",etaBlocksString.c_str());
	stringstream ss(etaBlocksString);
	vector<std::string> etaBlocksVector;
	while(getline(ss,temp,',')) {
		etaBlocksVector.push_back(temp);
		nEtaSteps++;
	}

	for (int i=0;i<etaBlocksVector.size();i++) {
		std::cout<<"etaBlocks["<<i<<"]="<<etaBlocksVector[i]<<std::endl;
	}


	int nPtBins=-1;
	printf("PtBins=%s\n",PtBins.c_str());
	stringstream ss3(PtBins);
	vector<std::string> PtBinsVector;
	while(getline(ss3,temp,':')) {
		PtBinsVector.push_back(temp);
		nPtBins++;
	}
	Float_t bins[PtBinsVector.size()];
	for (int i=0;i<PtBinsVector.size();i++) {
		std::cout<<"PtBinsVector["<<i<<"]="<<PtBinsVector[i]<<std::endl;
		bins[i]=std::stoi(PtBinsVector[i]);
		std::cout<<"bins["<<i<<"]="<<bins[i]<<std::endl;
	}
	SetAtlasStyle();
	//int nEtaSteps=7;
	//Double_t etaBlocks[]={0,.8,1.2,2.1,2.8,3.2,3.6,4.5};
	//int nEtaStepsSecondary=7;
	//Double_t etaBlocksSecondary[]={2.1,2.2,2.3,2.4,2.5,2.6,2.7,2.8};
	//int nEtaStepsSecondary=1;
	//Double_t etaBlocksSecondary[]={0,2.1};
	//int startEtaStep=0;
	//int nEtaStepsToTake=7;
	int histNameInt=0;
	//int nBinsForA=1000;
	//int maxMu=35;
	//int maxNPV=30;
	TH1D *R4ConeEM_etaSlicesCalib[nEtaSteps];

	TH1D *R4ConeEM_etaSlicesCalibMC[nEtaSteps];

	TH1D *R4ConeLC_etaSlicesCalib[nEtaSteps];

	TH1D *R4ConeLC_etaSlicesCalibMC[nEtaSteps];

	TH1D* R4ConeEM_NoiseVsEta;
	TH1D* R4ConeEM_NoiseVsEtaMC;

	TH1D* R4ConeLC_NoiseVsEta;
	TH1D* R4ConeLC_NoiseVsEtaMC;

	TFile *infile;
	TFile *infile2;
	infile = TFile::Open(inputFileNoise.c_str());
	if (doData){
		if (doEM) {
			R4ConeEM_NoiseVsEta=(TH1D*)infile->Get("NoiseAsFcnEta4EM");
			R4ConeEM_NoiseVsEta->SetDirectory(0);
		}
		if (doLC) {
			R4ConeLC_NoiseVsEta=(TH1D*)infile->Get("NoiseAsFcnEta4LC");
			R4ConeLC_NoiseVsEta->SetDirectory(0);
		}
	}
	if (doMC) {
		if (doEM) {
			R4ConeEM_NoiseVsEtaMC=(TH1D*)infile->Get("MCNoiseAsFcnEta4EM");
			R4ConeEM_NoiseVsEtaMC->SetDirectory(0);
		}
		if (doLC) {
			R4ConeLC_NoiseVsEtaMC=(TH1D*)infile->Get("MCNoiseAsFcnEta4LC");
			R4ConeLC_NoiseVsEtaMC->SetDirectory(0);
		}
	}

	infile->Close();
	if (doEM) {
		infile = TFile::Open(inputFileProfile.c_str());
	}
	if (doLC) {
		infile2 = TFile::Open(inputFileProfileLC.c_str());
	}
	for (int x=0;x<nEtaSteps;x++) {
		cout<<x<<endl;
		vector<std::string> stuffToWrite3;

		if (doData) {
			stuffToWrite3.push_back(etaBlocksVector[x]+"<|#eta|<"+etaBlocksVector[x+1]);
			if (doEM) {
				R4ConeEM_etaSlicesCalib[x]=(TH1D*)infile->Get(("ratioVsPtTruthAntiKt4TopoEM_absEta_"+etaBlocksVector[x]+"_"+etaBlocksVector[x+1]+"_NPV_"+std::to_string(minNPV)+"_"+std::to_string(maxNPV)).c_str())->Clone();
				R4ConeEM_etaSlicesCalib[x]->SetDirectory(0);
				saveOneHistoMarker(R4ConeEM_etaSlicesCalib[x],"p_{T}^{true}","<p_{T}^{calib}/p_{T}^{pileup sub}>",stuffToWrite3,outPutDir,false,0.20,0.88,0,0,true);
			}
			if (doLC) {
				R4ConeLC_etaSlicesCalib[x]=(TH1D*)infile2->Get(("ratioVsPtTruthAntiKt4LCTopo_absEta_"+etaBlocksVector[x]+"_"+etaBlocksVector[x+1]+"_NPV_"+std::to_string(minNPV)+"_"+std::to_string(maxNPV)).c_str())->Clone();
				R4ConeLC_etaSlicesCalib[x]->SetDirectory(0);
				saveOneHistoMarker(R4ConeLC_etaSlicesCalib[x],"p_{T}^{true}","<p_{T}^{calib}/p_{T}^{pileup sub}>",stuffToWrite3,outPutDir,false,0.20,0.88,0,0,true);
			}
			stuffToWrite3.clear();
		}
		if (doMC) {
			if (!doData) {
				if (doEM) {
					R4ConeEM_etaSlicesCalibMC[x]=(TH1D*)R4ConeEM_etaSlicesCalib[x]->Clone();
				}
				if (doLC) {
					R4ConeLC_etaSlicesCalibMC[x]=(TH1D*)R4ConeLC_etaSlicesCalib[x]->Clone();
				}
			} else {
				if (doEM) {
					std::string tempString(R4ConeEM_etaSlicesCalib[x]->GetName());
					R4ConeEM_etaSlicesCalibMC[x]=(TH1D*)R4ConeEM_etaSlicesCalib[x]->Clone((tempString+"MC").c_str());
				}
				if (doLC) {
					std::string tempString(R4ConeLC_etaSlicesCalib[x]->GetName());
					R4ConeLC_etaSlicesCalibMC[x]=(TH1D*)R4ConeLC_etaSlicesCalib[x]->Clone((tempString+"MC").c_str());
				}
			}
			if (doEM) {
				R4ConeEM_etaSlicesCalibMC[x]->SetDirectory(0);
				Double_t tempError;
				Double_t tempValue;
				Double_t scale=R4ConeEM_NoiseVsEtaMC->GetBinContent(x+1);
				for (int y=1;y<=R4ConeEM_etaSlicesCalibMC[x]->GetNbinsX();y++) {
					if (R4ConeEM_etaSlicesCalibMC[x]->GetBinContent(y)!=0) {
						tempValue=R4ConeEM_etaSlicesCalibMC[x]->GetBinContent(y);
						tempError=R4ConeEM_etaSlicesCalibMC[x]->GetBinError(y);
						R4ConeEM_etaSlicesCalibMC[x]->SetBinContent(y,tempValue*scale);
						R4ConeEM_etaSlicesCalibMC[x]->SetBinError(y,tempValue*scale*TMath::Sqrt(TMath::Power(tempError/tempValue,2)+TMath::Power(R4ConeEM_NoiseVsEtaMC->GetBinError(x+1)/scale,2)));
						cout <<R4ConeEM_NoiseVsEtaMC->GetBinError(x+1)<<" "<<tempError<<endl;
						cout <<R4ConeEM_NoiseVsEtaMC->GetBinError(x+1)/scale<<" "<<tempError/tempValue<<endl;
					}
				}
				//R4ConeEM_etaSlicesCalibMC[x]->Scale(R4ConeEM_NoiseVsEtaMC->GetBinContent(x+1));
			}
			if (doLC) {
				R4ConeLC_etaSlicesCalibMC[x]->SetDirectory(0);
				Double_t tempError;
				Double_t tempValue;
				Double_t scale=R4ConeLC_NoiseVsEtaMC->GetBinContent(x+1);
				for (int y=1;y<=R4ConeLC_etaSlicesCalibMC[x]->GetNbinsX();y++) {
					if (R4ConeLC_etaSlicesCalibMC[x]->GetBinContent(y)!=0) {
						tempValue=R4ConeLC_etaSlicesCalibMC[x]->GetBinContent(y);
						tempError=R4ConeLC_etaSlicesCalibMC[x]->GetBinError(y);
						R4ConeLC_etaSlicesCalibMC[x]->SetBinContent(y,tempValue*scale);
						R4ConeLC_etaSlicesCalibMC[x]->SetBinError(y,tempValue*scale*TMath::Sqrt(TMath::Power(tempError/tempValue,2)+TMath::Power(R4ConeLC_NoiseVsEtaMC->GetBinError(x+1)/scale,2)));
						cout <<R4ConeLC_NoiseVsEtaMC->GetBinError(x+1)<<" "<<tempError<<endl;
						cout <<R4ConeLC_NoiseVsEtaMC->GetBinError(x+1)/scale<<" "<<tempError/tempValue<<endl;
					}
				}
				//R4ConeLC_etaSlicesCalibMC[x]->Scale(R4ConeLC_NoiseVsEtaMC->GetBinContent(x+1));
			}
		}
		if (doData) {
			if (doEM) {
				/*R4ConeEM_etaSlicesCalib[x]->Scale(R4ConeEM_NoiseVsEta->GetBinContent(x+1));
				for (int y=1;y<=R4ConeEM_etaSlicesCalib[x]->GetNbinsX();y++) {
					if (R4ConeEM_etaSlicesCalib[x]->GetBinContent(y)!=0) {
						R4ConeEM_etaSlicesCalib[x]->SetBinError(y,TMath::Sqrt(TMath::Power(R4ConeEM_etaSlicesCalib[x]->GetBinError(y),2)+TMath::Power(R4ConeEM_NoiseVsEtaMC->GetBinError(x+1),2)));
					}
				}*/
				//R4ConeEM_etaSlicesCalib[x]->Scale(R4ConeEM_NoiseVsEta->GetBinContent(x+1));
				Double_t tempError;
				Double_t tempValue;
				Double_t scale=R4ConeEM_NoiseVsEta->GetBinContent(x+1);
				for (int y=1;y<=R4ConeEM_etaSlicesCalib[x]->GetNbinsX();y++) {
					if (R4ConeEM_etaSlicesCalib[x]->GetBinContent(y)!=0) {
						tempValue=R4ConeEM_etaSlicesCalib[x]->GetBinContent(y);
						tempError=R4ConeEM_etaSlicesCalib[x]->GetBinError(y);
						R4ConeEM_etaSlicesCalib[x]->SetBinContent(y,tempValue*scale);
						R4ConeEM_etaSlicesCalib[x]->SetBinError(y,tempValue*scale*TMath::Sqrt(TMath::Power(tempError/tempValue,2)+TMath::Power(R4ConeEM_NoiseVsEta->GetBinError(x+1)/scale,2)));
						cout <<R4ConeEM_NoiseVsEta->GetBinError(x+1)<<" "<<tempError<<endl;
						cout <<R4ConeEM_NoiseVsEta->GetBinError(x+1)/scale<<" "<<tempError/tempValue<<endl;
					}
				}
			}
			if (doLC) {
				//R4ConeLC_etaSlicesCalib[x]->Scale(R4ConeLC_NoiseVsEta->GetBinContent(x+1));
				Double_t tempError;
				Double_t tempValue;
				Double_t scale=R4ConeLC_NoiseVsEta->GetBinContent(x+1);
				for (int y=1;y<=R4ConeLC_etaSlicesCalib[x]->GetNbinsX();y++) {
					if (R4ConeLC_etaSlicesCalib[x]->GetBinContent(y)!=0) {
						tempValue=R4ConeLC_etaSlicesCalib[x]->GetBinContent(y);
						tempError=R4ConeLC_etaSlicesCalib[x]->GetBinError(y);
						R4ConeLC_etaSlicesCalib[x]->SetBinContent(y,tempValue*scale);
						R4ConeLC_etaSlicesCalib[x]->SetBinError(y,tempValue*scale*TMath::Sqrt(TMath::Power(tempError/tempValue,2)+TMath::Power(R4ConeLC_NoiseVsEta->GetBinError(x+1)/scale,2)));
						cout <<R4ConeLC_NoiseVsEta->GetBinError(x+1)<<" "<<tempError<<endl;
						cout <<R4ConeLC_NoiseVsEta->GetBinError(x+1)/scale<<" "<<tempError/tempValue<<endl;
					}
				}
			}
		}

	}
	if (doEM) {
		infile->Close();
	}
	if (doLC) {
		infile2->Close();
	}


	legend2 = new TLegend(.60,.825,1.,.92);
	legend2->SetFillColor(0);
	legend2->SetFillStyle(0);//make it transparent
	legend2->SetBorderSize(0);
	legend2->SetTextSize(0.04);
	legend2->SetTextFont(42);
	legend2->SetMargin(.15);

	legend4 = new TLegend(.60,.73,1.,.92);
	legend4->SetFillColor(0);
	legend4->SetFillStyle(0);//make it transparent
	legend4->SetBorderSize(0);
	legend4->SetTextSize(0.04);
	legend4->SetTextFont(42);
	legend4->SetMargin(.15);

	vector<std::string> stuffToWrite4;


	for (int x=0;x<nEtaSteps;x++) {
		stuffToWrite4.push_back("JES Scale");
		if (doData&&!doMC) {
			//legend2->AddEntry(NoiseAsFcnMu4EMAllSecondary,"R=0.4,EM Scale Data","P");
			//legend2->AddEntry(NoiseAsFcnMu4LCAllSecondary,"R=0.4,LC Scale Data","P");
			//saveTwoHistoMarker(NoiseAsFcnMu4EMAllSecondary,NoiseAsFcnMu4LCAllSecondary,legend2,"p_{T}^{true}","Pile-up Noise[GeV]",stuffToWrite4b,outPutDir,false,0.20,0.88,0,0);
			//legend2->Clear();
		} else if (!doData&&doMC) {
			//legend2->AddEntry(MCNoiseAsFcnMu4EMAllSecondary,"R=0.4,EM Scale MC","P");
			//legend2->AddEntry(MCNoiseAsFcnMu4LCAllSecondary,"R=0.4,LC Scale MC","P");
			//saveTwoHistoMarker(MCNoiseAsFcnMu4EMAllSecondary,MCNoiseAsFcnMu4LCAllSecondary,legend2,"<#mu>","Pile-up Noise[GeV]",stuffToWrite4b,outPutDir,false,0.20,0.88,0,0);
			//legend2->Clear();
		} else if (doData&&doMC) {
			if (doEM&&!doLC) {
				R4ConeEM_etaSlicesCalib[x]->SetMarkerStyle(20);
				R4ConeEM_etaSlicesCalibMC[x]->SetMarkerStyle(20);
				R4ConeEM_etaSlicesCalibMC[x]->SetMarkerColor(2);
				stuffToWrite4.push_back(etaBlocksVector[x]+"<|#eta|<"+etaBlocksVector[x+1]);
				legend2->AddEntry(R4ConeEM_etaSlicesCalib[x],"R=0.4,EM Scale Data","P");
				legend2->AddEntry(R4ConeEM_etaSlicesCalibMC[x],"R=0.4,EM Scale MC","P");
				saveTwoHistoMarker(R4ConeEM_etaSlicesCalib[x],R4ConeEM_etaSlicesCalibMC[x],legend2,"p_{T}^{true}","Pile-up Noise[GeV]",stuffToWrite4,outPutDir,false,0.20,0.88,0,0,true);
				legend2->Clear();
			} else if (!doEM&&doLC) {
				R4ConeLC_etaSlicesCalib[x]->SetMarkerStyle(20);
				R4ConeLC_etaSlicesCalibMC[x]->SetMarkerStyle(20);
				R4ConeLC_etaSlicesCalibMC[x]->SetMarkerColor(2);
				stuffToWrite4.push_back(etaBlocksVector[x]+"<|#eta|<"+etaBlocksVector[x+1]);
				legend2->AddEntry(R4ConeLC_etaSlicesCalib[x],"R=0.4,LC Scale Data","P");
				legend2->AddEntry(R4ConeLC_etaSlicesCalibMC[x],"R=0.4,LC Scale MC","P");
				saveTwoHistoMarker(R4ConeLC_etaSlicesCalib[x],R4ConeLC_etaSlicesCalibMC[x],legend2,"p_{T}^{true}","Pile-up Noise[GeV]",stuffToWrite4,outPutDir,false,0.20,0.88,0,0,true);
				legend2->Clear();
			} else if (doEM&&doLC) {
				R4ConeEM_etaSlicesCalib[x]->SetMarkerStyle(20);
				R4ConeEM_etaSlicesCalibMC[x]->SetMarkerStyle(20);
				R4ConeEM_etaSlicesCalibMC[x]->SetMarkerColor(2);
				R4ConeLC_etaSlicesCalib[x]->SetMarkerStyle(20);
				R4ConeLC_etaSlicesCalib[x]->SetMarkerColor(3);
				R4ConeLC_etaSlicesCalibMC[x]->SetMarkerStyle(20);
				R4ConeLC_etaSlicesCalibMC[x]->SetMarkerColor(4);
				stuffToWrite4.push_back(etaBlocksVector[x]+"<|#eta|<"+etaBlocksVector[x+1]);
				legend4->AddEntry(R4ConeEM_etaSlicesCalib[x],"R=0.4,EM Scale Data","P");
				legend4->AddEntry(R4ConeEM_etaSlicesCalibMC[x],"R=0.4,EM Scale MC","P");
				legend4->AddEntry(R4ConeLC_etaSlicesCalib[x],"R=0.4,LC Scale Data","P");
				legend4->AddEntry(R4ConeLC_etaSlicesCalibMC[x],"R=0.4,LC Scale MC","P");
				saveFourHistoMarker(R4ConeEM_etaSlicesCalib[x],R4ConeEM_etaSlicesCalibMC[x],R4ConeLC_etaSlicesCalib[x],R4ConeLC_etaSlicesCalibMC[x],legend4,"p_{T}^{true}","Pile-up Noise[GeV]",stuffToWrite4,outPutDir,false,0.20,0.88,0,0,true);
				legend4->Clear();
			}
		}
		stuffToWrite4.clear();
	}
	TFile *outfile=new TFile(outputFile.c_str(),"RECREATE");
	for (int x=0;x<nEtaSteps;x++) {
		if (doData) {
			if (doEM) {
				R4ConeEM_etaSlicesCalib[x]->Write();

			}
			if (doLC) {
				R4ConeLC_etaSlicesCalib[x]->Write();

			}
		}
		if (doMC) {
			if (doEM) {
				R4ConeEM_etaSlicesCalibMC[x]->Write();

			}
			if (doLC) {
				R4ConeLC_etaSlicesCalibMC[x]->Write();

			}
		}
	}
	outfile->Close();
}